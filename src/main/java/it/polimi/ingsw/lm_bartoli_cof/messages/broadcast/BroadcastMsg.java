package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import java.io.Serializable;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public interface BroadcastMsg extends Serializable {

	public void showMessage(MessageVisitor visitor);

}
