package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class AcquirePermitTileRequestMsg extends ActionRequestMsg {
	private static final long serialVersionUID = 1L;
	
	private String permitBoard;
	private String permitTile;
	private String politicCards;
	
	public String getPermitBoard() {
		return permitBoard;
	}

	public void setPermitBoard(String permitBoard) {
		this.permitBoard = permitBoard;
	}

	public String getPermitTile() {
		return permitTile;
	}

	public void setPermitTile(String permitTile) {
		this.permitTile = permitTile;
	}

	public String getPoliticCards() {
		return politicCards;
	}

	public void setPoliticCards(String politicCards) {
		this.politicCards = politicCards;
	}
	
	public AcquirePermitTileRequestMsg(Token token) {
		super(token);
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}

	
}
