package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class DisplaySomethingResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private String stringMessage;
	
	
	public DisplaySomethingResponseMsg(String string){
		
		this.stringMessage = string;
	}
	
	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();
		string.append(stringMessage);
		visitor.display(string.toString());
	}

}
