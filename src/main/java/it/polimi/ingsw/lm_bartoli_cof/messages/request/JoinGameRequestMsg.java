package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * is used by a player to join to a waiting room
 * @author stefanobartoli
 *
 */
public class JoinGameRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;
	
	private int gameId;
	
	public int getGameId() {
		return gameId;
	}

	public JoinGameRequestMsg(Token token, String gameId) {
		super(token);
		this.gameId = Integer.parseInt(gameId);
	}

}
