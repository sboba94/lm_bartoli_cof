package it.polimi.ingsw.lm_bartoli_cof.messages;

import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;

/**
 * This class contains both the private response and the public broadcast
 * message.
 * 
 */
public class MessageCouple {

	private ResponseMsg response;
	private BroadcastMsg broadcast;

	public MessageCouple() {
		super();
	}

	public ResponseMsg getResponse() {
		return response;
	}

	public void setResponse(ResponseMsg response) {
		this.response = response;
	}

	public BroadcastMsg getBroadcast() {
		return broadcast;
	}

	public void setBroadcast(BroadcastMsg broadcast) {
		this.broadcast = broadcast;
	}

}
