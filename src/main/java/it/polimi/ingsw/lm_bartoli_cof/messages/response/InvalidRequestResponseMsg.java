package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class InvalidRequestResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private String string;
	
	public InvalidRequestResponseMsg(String string){
		this.string = string;
	}
	
	@Override
	public void showMessage(MessageVisitor visitor) {
		visitor.display(string);
	}

}
