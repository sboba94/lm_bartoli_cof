package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * used to concrete creation of player in the game. Before of this request in game there is only a token to identify the client.
 * Him player does not exist before execute this!!.
 *
 */
public class SetMyPlayerRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;
	
	private String nickname;
	
	public SetMyPlayerRequestMsg(Token token, String nickname) {
		super(token);
		this.nickname = nickname;
	}

	public String getNickname(){
		return nickname;
	}
}
