package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class MarketStateBroadcastMsg implements BroadcastMsg {

	private static final long serialVersionUID = 1L;
	
	private Player player;
	
	public MarketStateBroadcastMsg(Player player){
		this.player = player;
	}
	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();
		string.append("\n*************** BROADCAST ***************\n");
		string.append("\n" + player.getNickname() + "'s market state is started. Wait for the first object for sale\n");
		string.append("\n-----------------------------------------\n");
		visitor.display(string.toString());
	}

}
