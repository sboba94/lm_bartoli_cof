package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * Message to request to build an emporium with a permit tile
 */
public class BuildEmporiumWithPermitTileRequestMsg extends ActionRequestMsg {
	private static final long serialVersionUID = 1L;
	
	String city;
	String tile;
	
	/**
	 * Constructor of BuildEmporiumWithPermitTileRequestMsg
	 * @param token, the token to identify the player
	 */
	public BuildEmporiumWithPermitTileRequestMsg(Token token) {
		super(token);	
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the tile
	 */
	public String getTile() {
		return tile;
	}

	/**
	 * @param tile the tile to set
	 */
	public void setTile(String tile) {
		this.tile = tile;
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}

}
