package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * used to set the Quick Action state and to return the response with all action that this player can do 
 *
 */
public class QuickActionRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;

	public QuickActionRequestMsg(Token token) {
		super(token);
	}

	

}
