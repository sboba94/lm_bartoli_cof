package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class ElectCouncillorWithAssistantRequestMsg extends ActionRequestMsg {
	private static final long serialVersionUID = 1L;
	
	private String color;
	private String board;
	
	public ElectCouncillorWithAssistantRequestMsg(Token token) {
		super(token);
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}
	

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}


}
