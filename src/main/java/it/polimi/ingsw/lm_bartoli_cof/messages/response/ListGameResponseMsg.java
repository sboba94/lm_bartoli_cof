package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

//COPY OF INVALID REQUEST RESPONSE MESSAGE!!
public class ListGameResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private String sentence;

	public ListGameResponseMsg(String string) {
		this.sentence = string;
	}

	@Override
	public void showMessage(MessageVisitor visitor) {
		if (this.sentence == null)
			visitor.display("There aren't avaible game rooms, please type 'new game' to create a new game room: ");
		else
			visitor.display(sentence);
	}

}
