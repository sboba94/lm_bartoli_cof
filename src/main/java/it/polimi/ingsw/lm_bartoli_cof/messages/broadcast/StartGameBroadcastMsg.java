package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class StartGameBroadcastMsg implements BroadcastMsg {
	private static final long serialVersionUID = 1L;

	private int gameId;

	/**
	 * This is the constructor of the class
	 * 
	 * @param gameId,
	 *            the identification number of the game
	 */
	public StartGameBroadcastMsg(int gameId) {
		this.gameId = gameId;

	}

	/**
	 * Show the message to all player of the game
	 */
	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();
		string.append("\nGame " + this.gameId + " is started.\n\n");
		string.append("Please insert your nickname writing 'nickname [YOURNAME]': ");
		visitor.display(string.toString());
	}

}
