package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class BuyRequestMsg extends ActionRequestMsg{
	private static final long serialVersionUID = 1L;
	
	private LinkedList<String> PermitBuyList;
	private LinkedList<String> PoliticBuyList;
	private LinkedList<String> AssistantBuyList;
	// Saranno formattati nel seguente modo:
	// PermitSellList: {[INDICE_CARTA,PREZZO],[INDICE_CARTA,PREZZO]...}
	// PoliticSellList: {[INDICE_CARTA,PREZZO],[INDICE_CARTA,PREZZO]...}
	// AssistentSellList: {[PREZZO][PREZZO]...}
	
	public BuyRequestMsg(Token token) {
		super(token);
		PermitBuyList = new LinkedList<String>();
		PoliticBuyList = new LinkedList<String>();
		AssistantBuyList = new LinkedList<String>();
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}

	/**
	 * @return the permitBuyList
	 */
	public LinkedList<String> getPermitBuyList() {
		return PermitBuyList;
	}

	/**
	 * @param permitBuyList the permitBuyList to set
	 */
	public void setPermitBuyList(LinkedList<String> permitBuyList) {
		PermitBuyList = permitBuyList;
	}

	/**
	 * @return the politicBuyList
	 */
	public LinkedList<String> getPoliticBuyList() {
		return PoliticBuyList;
	}

	/**
	 * @param politicBuyList the politicBuyList to set
	 */
	public void setPoliticBuyList(LinkedList<String> politicBuyList) {
		PoliticBuyList = politicBuyList;
	}

	/**
	 * @return the assistantBuyList
	 */
	public LinkedList<String> getAssistantBuyList() {
		return AssistantBuyList;
	}

	/**
	 * @param assistantBuyList the assistantBuyList to set
	 */
	public void setAssistantBuyList(LinkedList<String> assistantBuyList) {
		AssistantBuyList = assistantBuyList;
	}

	

}
