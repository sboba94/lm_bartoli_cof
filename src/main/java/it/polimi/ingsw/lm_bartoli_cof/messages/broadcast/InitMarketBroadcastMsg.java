package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class InitMarketBroadcastMsg implements BroadcastMsg {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private Player player;

	public InitMarketBroadcastMsg(Player player) {
		this.player = player;
	}

	/**
	 * Show the message to all player of the game
	 */
	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();
		string.append("\nIt's market time! Do you want to sell something? Answer [yes] or [no]");
		visitor.display(string.toString());
	}

}
