package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class DisplayToClient implements BroadcastMsg {
	private static final long serialVersionUID = 1L;
	
	private Token token;
	private String s;
	
	public DisplayToClient(String s, Token token){
		this.s = s;
		this.token = token;
	}
	
	@Override
	public void showMessage(MessageVisitor visitor) {
		visitor.display(s);
	}

	public Object getToken() {
		
		return token;
	}

}
