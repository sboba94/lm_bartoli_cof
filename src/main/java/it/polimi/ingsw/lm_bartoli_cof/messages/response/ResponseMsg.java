package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import java.io.Serializable;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

/**
 * This interface represents a Response message from the server to the client,
 * subsequent to a client request.
 */
public interface ResponseMsg extends Serializable {
	
	/**
     * Create a message and use this method to display the result.
     *
     */
	public void showMessage(MessageVisitor visitor);
}
