package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

/**
 * Broadcast message to display when someone acquires a permit tile
 *
 */
public class AcquirePermitTileBroadcastMsg implements BroadcastMsg {
	private static final long serialVersionUID = 1L;

	private Player player;
	private int coins;
	private BusinessPermitTile tile;
	private RegionBoard region;

	/**
	 * Constructor
	 * 
	 * @param player
	 * @param usedCoins
	 * @param tileAcquired
	 * @param region
	 */
	public AcquirePermitTileBroadcastMsg(Player player, int usedCoins, BusinessPermitTile tileAcquired,
			RegionBoard region) {
		this.coins = usedCoins;
		this.tile = tileAcquired;
		this.player = player;
		this.region = region;
	}

	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();

		string.append("\n*************** BROADCAST ***************\n");
		string.append("\n" + this.player.getNickname());
		string.append(" has acquired this business permit tile");
		string.append(" from " + region.getRegionName().name() + ":\n");
		string.append(this.tile.toString());
		string.append("\nHe has paid " + coins + " coins\n");
		string.append("\n-----------------------------------------\n");

		visitor.display(string.toString());
	}

}
