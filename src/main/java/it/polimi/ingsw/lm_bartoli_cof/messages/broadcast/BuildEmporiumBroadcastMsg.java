package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class BuildEmporiumBroadcastMsg implements BroadcastMsg {
	private static final long serialVersionUID = 1L;

	private Player player;
	private int usedCoins;
	private City newEmporium;
	private City kingsNewCity;

	/**
	 * Constructor for Build Emporium With King
	 * 
	 * @param player
	 * @param usedCoins
	 * @param newEmporium
	 * @param kingsNewCity
	 */
	public BuildEmporiumBroadcastMsg(Player player, int usedCoins, City newEmporium, City kingsNewCity) {
		this.kingsNewCity = kingsNewCity;
		this.newEmporium = newEmporium;
		this.usedCoins = usedCoins;
		this.player = player;
	}

	/**
	 * Constructor for Build Emporium With PermitTile
	 * 
	 * @param player
	 * @param usedCoins
	 * @param newEmporium
	 */
	public BuildEmporiumBroadcastMsg(Player player, int usedCoins, City newEmporium) {
		this.kingsNewCity = null;
		this.newEmporium = newEmporium;
		this.usedCoins = usedCoins;
		this.player = player;
	}

	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();
		if (kingsNewCity != null) {
			string.append("\n*************** BROADCAST ***************\n");
			string.append("\n"+ this.player.getNickname() + " has built a new emporium on " + newEmporium.getName()
					+ " with the help of the King\n");
			string.append("He has used " + usedCoins + " coins and has moved the King to " + kingsNewCity.getName()+"\n");
			string.append("\n-----------------------------------------\n");
		} else {
			string.append("\n*************** BROADCAST ***************\n");
			string.append("\n"+ this.player.getNickname() + " has built a new emporium on " + newEmporium.getName()
					+ " using a business permit tile\n");
			string.append("\n-----------------------------------------\n");
		}
		visitor.display(string.toString());
	}

}
