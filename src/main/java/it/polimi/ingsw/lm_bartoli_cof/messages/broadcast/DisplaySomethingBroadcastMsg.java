package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class DisplaySomethingBroadcastMsg implements BroadcastMsg {
	private static final long serialVersionUID = 1L;
	
	private String msg;
	
	/**
	 * This is the constructor of the class
	 * @param msg, the message for all players
	 */
	public DisplaySomethingBroadcastMsg(String msg){
		this.msg = msg;
	}
	
	/**
	 * Show the message to all player of the game
	 */
	@Override
	public void showMessage(MessageVisitor visitor) {
		visitor.display(msg);
		
	}

}
