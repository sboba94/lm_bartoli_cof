package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * This class need during connection in order to return the token; this token
 * allows to the client to identify himself
 */
public class ConnectionRequest extends RequestMsg {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of ConnectionRequest Message.
	 *
	 * @param token
	 *            the token for the identification of the client
	 */
	public ConnectionRequest(Token token) {
		super(token);
	}
}
