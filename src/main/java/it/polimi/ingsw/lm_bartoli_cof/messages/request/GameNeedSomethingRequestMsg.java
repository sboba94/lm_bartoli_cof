package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class GameNeedSomethingRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;
	
	private String question;
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public RequestMsg getRealRequest() {
		return realRequest;
	}

	public void setRealRequest(RequestMsg realRequest) {
		this.realRequest = realRequest;
	}

	private RequestMsg realRequest;
	
	public GameNeedSomethingRequestMsg(Token token) {
		super(token);
	}

}
