package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

public class AcquirePermitTileResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;

	private int usedCoins;
	private LinkedList<Bonus> bonuses;
	private Player player;

	public AcquirePermitTileResponseMsg(Player player, int coins, LinkedList<Bonus> bonuses) {
		this.bonuses = bonuses;
		this.usedCoins = coins;
		this.player = player;
	}

	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();

		string.append("\nYou have acquired a business permit tile:\n");
		string.append("+ Used coins: " + usedCoins);
		string.append("\n+ Gained bonuses: \n");
		if (!bonuses.isEmpty()) {
			for (Bonus b : bonuses) {
				string.append("* " + b.getType() + " value: " + b.getValue() + ";\n");
			}
			string.append("\n");
		} else {
			string.append("-----> There are no bonuses in this city or in your neighbours emporiums \n");
		}
		string.append(ActionQuestion.askForNextState(player));
		visitor.display(string.toString());
	}

}
