package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class NothingToSellRequestMsg extends ActionRequestMsg {
	private static final long serialVersionUID = 1L;
	
	public NothingToSellRequestMsg(Token token) {
		super(token);
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}

}
