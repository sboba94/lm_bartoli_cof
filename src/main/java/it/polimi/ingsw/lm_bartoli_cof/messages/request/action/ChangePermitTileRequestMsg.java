package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class ChangePermitTileRequestMsg extends ActionRequestMsg {
	private static final long serialVersionUID = 1L;
	
	private String permitBoard;
	
	/**
	 * @return the permitBoard
	 */
	public String getPermitBoard() {
		return permitBoard;
	}

	/**
	 * @param permitBoard the permitBoard to set
	 */
	public void setPermitBoard(String permitBoard) {
		this.permitBoard = permitBoard;
	}

	public ChangePermitTileRequestMsg(Token token) {
		super(token);
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}
}
