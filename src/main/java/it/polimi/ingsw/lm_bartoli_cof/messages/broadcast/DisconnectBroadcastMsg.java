package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class DisconnectBroadcastMsg implements BroadcastMsg {
	private static final long serialVersionUID = 1L;

	private Player player;

	/**
	 * This is the constructor of the class
	 * 
	 * @param player
	 */
	public DisconnectBroadcastMsg(Player player) {
		this.player = player;
	}

	/**
	 * Show the message to all player of the game
	 */
	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();
		string.append("\n*************** BROADCAST ***************\n");
		string.append("\n" + player.getNickname() + " has left the game\n");
		string.append("\n-----------------------------------------\n");
		visitor.display(string.toString());
	}

}
