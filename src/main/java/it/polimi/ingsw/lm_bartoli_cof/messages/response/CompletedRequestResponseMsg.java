package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class CompletedRequestResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private String msg;
	
	public CompletedRequestResponseMsg(String msg){
		this.msg = msg;
	}
	
	@Override
	public void showMessage(MessageVisitor visitor) {
		visitor.display(msg);

	}

}
