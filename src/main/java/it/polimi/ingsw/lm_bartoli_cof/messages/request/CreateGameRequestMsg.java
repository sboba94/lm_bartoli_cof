package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * Request used to inizialize and start a game 
 * @author stefanobartoli
 *
 */
public class CreateGameRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;
	
	private String mapRoot;
	
	public CreateGameRequestMsg(Token token) {
		super(token);
	}

	public String getMapRoot() {
		return mapRoot;
	}

	public void setMapRoot(String mapRoot) {
		this.mapRoot = mapRoot;
	}

}
