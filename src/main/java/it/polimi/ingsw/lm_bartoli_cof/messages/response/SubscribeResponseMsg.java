package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

/**
*
*/
public class SubscribeResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private final boolean success;

	public SubscribeResponseMsg(boolean success) {

		this.success = success;

	}

	public boolean isSuccess() {

		return success;

	}

	@Override
	public void showMessage(MessageVisitor visitor) {
		if(success)
			visitor.display("Subscribed!");
		else
			visitor.display("Not subscribed!");
	}

}
