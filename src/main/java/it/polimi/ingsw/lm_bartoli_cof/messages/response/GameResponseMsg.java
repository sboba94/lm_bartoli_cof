package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class GameResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private String msg;
	
	public GameResponseMsg(String msg){
		this.setMsg(msg);
	}
	
	@Override
	public void showMessage(MessageVisitor visitor) {

	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
