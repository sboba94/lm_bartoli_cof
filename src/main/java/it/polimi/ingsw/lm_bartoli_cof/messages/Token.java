package it.polimi.ingsw.lm_bartoli_cof.messages;

import java.io.Serializable;
import java.util.Objects;
import java.util.Scanner;

public class Token implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final String uuid;
    // Player number is optional
    private int playerNumber;
    
    private Scanner scanner;

    


	/**
     * Create a new Token.
     *
     * @param uuid an unique identifier in String format
     */
    public Token(String uuid, int playerNumber) {
        this.uuid = uuid;
        this.playerNumber =  playerNumber;
    }

    
    /**
     * Get the unique identifier of the client.
     *
     * @return the unique identifier of the client
     */
    public String getUuid() {

        return uuid;

    }

    /**
     * Get the player number in a game.
     *
     * @return the player number in a game
     */
    public int getPlayerNumber() {

        return playerNumber;

    }
    
    public Scanner getScanner() {
		return scanner;
	}


	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

    @Override
    public boolean equals(Object o) {

        // NOTE: Only uuid is used!

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return Objects.equals(uuid, token.uuid);

    }

    @Override
    public int hashCode() {

        // NOTE: Only uuid is used!
        return Objects.hashCode(uuid);

    }
    
}
