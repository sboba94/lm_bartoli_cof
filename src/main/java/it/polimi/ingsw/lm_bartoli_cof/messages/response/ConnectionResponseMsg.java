package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class ConnectionResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private Token token;
	
	public ConnectionResponseMsg(Token token) {
	 this.token = token;
	}

	@Override
	public void showMessage(MessageVisitor visitor) {
		visitor.display("Connected with token: " + token.getUuid());
	}

	public Token getToken() {
		
		return token;
	}

}
