package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * used to print the dashboard
 *
 */
public class ViewGameBoardRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;

	public ViewGameBoardRequestMsg(Token token) {
		super(token);
	}

}
