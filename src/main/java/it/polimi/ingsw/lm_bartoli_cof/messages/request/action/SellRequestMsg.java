package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class SellRequestMsg extends ActionRequestMsg {
	private static final long serialVersionUID = 1L;
	
	private LinkedList<String> permitSellList;
	private LinkedList<String> politicSellList;
	private LinkedList<String> assistantSellList;
	// Saranno formattati nel seguente modo:
	// PermitSellList: {[INDICE_CARTA,PREZZO],[INDICE_CARTA,PREZZO]...}
	// PoliticSellList: {[INDICE_CARTA,PREZZO],[INDICE_CARTA,PREZZO]...}
	// AssistentSellList: {[PREZZO][PREZZO]...}
	
	
	public SellRequestMsg(Token token) {
		super(token);
		this.assistantSellList = new LinkedList<String>();
		this.politicSellList = new LinkedList<String>();
		this.permitSellList = new LinkedList<String>();
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}

	/**
	 * @return the permitSellList
	 */
	public LinkedList<String> getPermitSellList() {
		return permitSellList;
	}

	/**
	 * @param permitSellList the permitSellList to set
	 */
	public void setPermitSellList(LinkedList<String> permitSellList) {
		this.permitSellList = permitSellList;
	}

	/**
	 * @return the politicSellList
	 */
	public LinkedList<String> getPoliticSellList() {
		return politicSellList;
	}

	/**
	 * @param politicSellList the politicSellList to set
	 */
	public void setPoliticSellList(LinkedList<String> politicSellList) {
		this.politicSellList = politicSellList;
	}

	/**
	 * @return the assistantSellList
	 */
	public LinkedList<String> getAssistantSellList() {
		return assistantSellList;
	}

	/**
	 * @param assistantSellList the assistantSellList to set
	 */
	public void setAssistantSellList(LinkedList<String> assistantSellList) {
		this.assistantSellList = assistantSellList;
	}
	
	

}
