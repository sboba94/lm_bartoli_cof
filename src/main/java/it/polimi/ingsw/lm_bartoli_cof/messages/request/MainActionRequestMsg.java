package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * used to set the Main Action state and to return the response with all action that this player can do
 *
 */
public class MainActionRequestMsg extends RequestMsg{
	private static final long serialVersionUID = 1L;

	public MainActionRequestMsg(Token token) {
		super(token);
	}



}
