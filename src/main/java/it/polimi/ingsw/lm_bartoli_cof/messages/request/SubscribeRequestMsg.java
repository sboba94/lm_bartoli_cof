package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class SubscribeRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;

	public SubscribeRequestMsg(Token token) {
		super(token);
	}

}
