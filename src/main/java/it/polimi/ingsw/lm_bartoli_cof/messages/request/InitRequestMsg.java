package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * is used to get the list of waiting rooms
 *
 */
public class InitRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;

	public InitRequestMsg(Token token) {
		super(token);
		
	}

}
