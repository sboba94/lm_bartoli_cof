package it.polimi.ingsw.lm_bartoli_cof.messages.broadcast;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class EndTurnBroadcastMessage implements BroadcastMsg {
	private static final long serialVersionUID = 1L;
	
	private Player player;
	private Token nextPlayer;

	public EndTurnBroadcastMessage(Player player, Token nextPlayer) {
		this.player = player;
		this.nextPlayer = nextPlayer;
	}
	
	public Token getNextPlayer(){
		return nextPlayer;
	}
	
	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();
		string.append("\n*************** BROADCAST ***************\n");
		string.append("\n" + this.player.getNickname() + " ends his turn\n");
		string.append("\n-----------------------------------------\n");
		visitor.display(string.toString());
	}

}
