package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

public class DisconnectRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;

	public DisconnectRequestMsg(Token token) {
		super(token);
	}

}
