package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

public class BuildEmporiumResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;
	
	private City newEmporium;
	private int usedCoins;
	private int usedAssistants;
	private Player player;
	private LinkedList<Bonus> bonuses;

	public BuildEmporiumResponseMsg(City newEmporium, int usedCoins, int usedAssistants, LinkedList<Bonus> bonuses,
			Player player) {
		this.player = player;
		this.usedCoins = usedCoins;
		this.usedAssistants = usedAssistants;
		this.newEmporium = newEmporium;
		this.bonuses = bonuses;
	}

	@Override
	public void showMessage(MessageVisitor visitor) {
		StringBuilder string = new StringBuilder();

		string.append("\nYou have built an emporium on " + newEmporium.getName());
		string.append("\n+ Used coins: " + usedCoins);
		string.append("\n+ Used assistants: " + usedAssistants);
		string.append("\n+ Gained bonuses: \n");
		if(!bonuses.isEmpty()){
			for (Bonus b : bonuses) {
				string.append("- " + b.getType() + " value: " + b.getValue() + ";\n");
			}
		}else {
			string.append("-----> There are no bonuses in this city or in your neighbours emporiums \n");
		}
		
		string.append(ActionQuestion.askForNextState(this.player));
		
		visitor.display(string.toString());
	}

}
