package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;

/**
 * ActionRequest Message superclass that extends RequestMsg
 */
public abstract class ActionRequestMsg extends RequestMsg {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of ActionRequest Message superclass
	 *
	 * @param token
	 *            the token for the identification of the client
	 */
	public ActionRequestMsg(Token token) {
		super(token);
	}

	/**
	 * This method need to create the action, it must be implemented in all type
	 * of ActionRequestMessage because all requestMsg pass himself to the internal function
	 * 
	 * @param visitor
	 *            depends on what type of request message is arrived
	 * @return the action that will be executed
	 */
	public abstract PlayerAction createAction(ActionCreatorFactory visitor);
}