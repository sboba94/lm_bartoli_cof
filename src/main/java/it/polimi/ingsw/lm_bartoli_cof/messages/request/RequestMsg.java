package it.polimi.ingsw.lm_bartoli_cof.messages.request;

import java.io.Serializable;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * This is the superclass of all type of request message
 */
public abstract class RequestMsg implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * All Response Message contains a token in order to allows the client to
	 * identify himself
	 */
	private Token token;

	/**
	 * Constructor of abstract superclass.
	 *
	 * @param token
	 *            the token that need for the identification
	 */
	public RequestMsg(Token token) {
		this.token = token;
		
	}

	/**
	 * Getter of token
	 *
	 * @return the token that is used for the identification
	 */
	public Token getToken() {
		return this.token;
	}

}
