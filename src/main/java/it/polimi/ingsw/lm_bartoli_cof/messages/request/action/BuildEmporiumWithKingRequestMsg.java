package it.polimi.ingsw.lm_bartoli_cof.messages.request.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * Message to request to build an emporium with the king
 */
public class BuildEmporiumWithKingRequestMsg extends ActionRequestMsg {
	private static final long serialVersionUID = 1L;
	
	private String politicCards;
	private String city;
	
	/**
	 * Constructor of BuildEmporiumWithKingRequestMsg
	 * @param token, the token to identify the player
	 */
	public BuildEmporiumWithKingRequestMsg(Token token) {
		super(token);
	}
	
	

	/**
	 * @return the politicCard
	 */
	public String getPoliticCards() {
		return politicCards;
	}



	/**
	 * @param politicCard the politicCard to set
	 */
	public void setPoliticCards(String politicCards) {
		this.politicCards = politicCards;
	}



	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public PlayerAction createAction(ActionCreatorFactory visitor) {
		return visitor.create(this);
	}

}
