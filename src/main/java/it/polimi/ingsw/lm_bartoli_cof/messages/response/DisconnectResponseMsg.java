package it.polimi.ingsw.lm_bartoli_cof.messages.response;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class DisconnectResponseMsg implements ResponseMsg {
	private static final long serialVersionUID = 1L;

	private String stringMessage;
	
	public DisconnectResponseMsg(Player player){
		this.stringMessage = "\nThanks "+player.getNickname()+" for playing!\n";
	}
	
	@Override
	public void showMessage(MessageVisitor visitor) {
		visitor.display(stringMessage);
	}

}
