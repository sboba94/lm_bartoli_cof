package it.polimi.ingsw.lm_bartoli_cof.view.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import it.polimi.ingsw.lm_bartoli_cof.view.common.BrokerInterface;
import it.polimi.ingsw.lm_bartoli_cof.view.common.RequestHandler;
import it.polimi.ingsw.lm_bartoli_cof.view.common.SubscriberInterface;

public class RMIFactory extends PlayerConnectionFactory {

	// private static final Logger LOG =
	// Logger.getLogger(RMIFactory.class.getName());
	//

	// qua va messa la porta del server
	public static final int RMI_PORT = 7777;

	private final BrokerInterface broker;
	private final RequestHandler requestHandler;
	private SubscriberInterface subscriberInterface;

	/**
	 * CREA UN NUOVA CONNESSIONE RMY CON IL FACTORY SULLA CONNESSIONE
	 *
	 * @param host
	 *            the hostname of the server
	 * @param viewUpdater
	 *            the view updater, displays the server updates on the view
	 * @throws RemoteException
	 *             if an error occurred while starting RMI
	 * @throws NotBoundException
	 *             if the remote interface has not been found
	 */
	public RMIFactory(String host, ViewUpdater viewUpdater) throws RemoteException, NotBoundException {
		// SERVE ALLA SUPERCLASSE
		super(viewUpdater);

		// try {

		// RITORNA UN RIFERIMENTO DI UN OGGETTO REMOTO AL REGISTRY DELL'HOST
		// SPECIFICO E ALLA SUA PORTA, SE HOST é NULLO é PERCHé STO USANDO IL
		// LOCALHOST
		Registry registry = LocateRegistry.getRegistry(host, RMI_PORT);

		//SERVE PER OTTENERE IL GESTORE DELLE RICHIESTE
		requestHandler = (RequestHandler) registry.lookup("RequestHandler");

		//SERVE PER OTTENERE IL BROKER
		broker = (BrokerInterface) registry.lookup("Broker");

		// Exports the remote object to make it available to receive
		// incoming
		// calls, using the particular supplied port. By using 0 RMI
		// implementation
		// chooses a port.
		subscriberInterface = (SubscriberInterface) UnicastRemoteObject.exportObject(getSubscriber(), 0);
		
		//IMPOSTA LA CONNESSIONE
		setupConnection();

		// } catch (RemoteException e) {
		// LOG.log(Level.SEVERE, "There was a problem establishing a RMI
		// connection to" + " the game manager.", e);
		// throw e;
		// } catch (NotBoundException | ClassCastException e) {
		// LOG.log(Level.SEVERE, "RMI connection error (problem with the remote
		// " + "interface).", e);
		// throw e;
		// }

	}

	@Override
	public RequestHandler getRequestHandler() {
		return requestHandler;
	}

	@Override
	public BrokerInterface getBrokerInterface() {
		return broker;
	}

	@Override
	public SubscriberInterface getSubscriberInterface() {
		return subscriberInterface;
	}

}
