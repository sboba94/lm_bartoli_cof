package it.polimi.ingsw.lm_bartoli_cof.view.questions;

import java.util.LinkedList;
import java.util.Scanner;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.GameMap;
import it.polimi.ingsw.lm_bartoli_cof.model.map.PermitBoard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;

/**
 * This class is used for ask to a client specific questions about the choices
 * that he can do especially during the attribution of some kind of bonus or at
 * the start of a player turn.
 *
 */
public class ActionQuestion {

	/**
	 * Asks a player to choose the region from which he wants to obtain a
	 * business permit tile without paying the cost after he has gained the
	 * bonus that permit this action.
	 *
	 */
	public static PermitBoard choosePermitBoard(GameMap map) {
		System.out.println(
				"Choose the region from which you want to acquire a business permit tile ['Coast' or 'Hills' or 'Mountains']: ");
		do {
			// for the scanner closing warning
			@SuppressWarnings("resource")
			Scanner keyboard = new Scanner(System.in);
			String input = keyboard.nextLine();
			switch (input) {
			case "Coast":
				return map.getCoast().getPermitBoard();
			case "Hills":
				return map.getHill().getPermitBoard();
			case "Mountains":
				return map.getMountain().getPermitBoard();
			default:
				System.out.println("Please type a valid choice");
			}
		} while (true);
	}

	/**
	 * Asks a player to choose the business permit tile that he wants to acquire
	 * without paying the cost.
	 * 
	 * @param permitBoard
	 *            the {@link PermitBoard} from which the player can choose the
	 *            tile
	 * @return the chosen business permit tile
	 *
	 */
	public static BusinessPermitTile choosePermitTileFromBoard(PermitBoard permitBoard) {
		System.out.println("These are the two aquirable permit card on " + permitBoard.getRegionName().toString()
				+ " permit board:" + "\n");
		System.out.println("1) " + permitBoard.showUsablePermitTile1().toString());
		System.out.println("2) " + permitBoard.showUsablePermitTile2().toString());
		System.out.println("Type your choice number:");
		do {
			// for the scanner closing warning
			@SuppressWarnings("resource")
			Scanner keyboard = new Scanner(System.in);
			String input = keyboard.nextLine();
			switch (input) {
			case "1":
				return permitBoard.getUsablePermitTile1();
			case "2":
				return permitBoard.getUsablePermitTile2();
			default:
				System.out.println("Please type a valid choice");
			}
		} while (true);
	}

	/**
	 * Asks a player to choose the business permit tile from which he wants to
	 * gain the bonuses (also from the face-down ones).
	 *
	 */
	// for the scanner closing warning
	@SuppressWarnings("resource")
	public static BusinessPermitTile choosePermitTileFromHand(Player player) {
		if (player.getUsablePermitTiles().isEmpty() && player.getUsedPermitTiles().isEmpty()) {
			System.out.println("There aren't business permit tiles in your hand so you can't receive any bonus!\n");
			return null;
		} else {
			LinkedList<BusinessPermitTile> allTiles = new LinkedList<BusinessPermitTile>();
			allTiles.addAll(player.getUsablePermitTiles());
			allTiles.addAll(player.getUsedPermitTiles());
			System.out.println("These are all the business permit tiles in your hand, also face down ones: " + "\n");

			// shows all permit tiles from the hand of a player, also face down
			// tiles
			int i = 1;
			for (BusinessPermitTile tile : allTiles) {
				System.out.println(i + ") " + tile.toString() + "; \n");
				i++;
			}
			System.out
					.println("Type the number of the business permit tile from which you want to receive the bonuses:");
			do {
				Scanner keyboard = new Scanner(System.in);
				int input = keyboard.nextInt();
				if (input < i)
					return allTiles.get(input - 1);
				else
					System.out.println("Please type a correct choise");
			} while (true);
		}
	}

	/**
	 * Asks a player to choose a city from which he wants to gain the reward
	 * token (only from the cities on which he has already built an emporium).
	 *
	 */
	@SuppressWarnings("resource")
	public static City chooseTokenFromCity(LinkedList<City> usableCity) {
		if (usableCity.isEmpty()) {
			System.out.println(
					"You have alredy chosen all the cities on which you have built an emporium or you haven't built an emporium yet so you can't receive any token\n");
			return null;
		} else {
			System.out.println(
					"These are all the reward tokens from the cities on which you have built an emporium [this list doesn't contains the cities with a NOBILITY bonus] : "
							+ "\n");
			// shows the reward tokens from the cities on which a player has
			// built an emporium
			int i = 1;
			for (City city : usableCity) {
				System.out.println(i + ") " + city.getToken().toString() + "; \n");
				i++;
			}
			System.out.println("Type the number of the reward token from which you want to receive the bonuses:");
			do {
				Scanner keyboard = new Scanner(System.in);
				int input = keyboard.nextInt();
				if (input < i)
					return usableCity.get(input - 1);
				else
					System.out.println("Please type a correct choise");
			} while (true);
		}
	}

	/**
	 * Asks a player what he wants to do at the start of his turn.
	 * 
	 * @param player
	 *            the player to ask
	 * 
	 * @return the actual question
	 *
	 */
	public static String askForNextState(Player player) {
		StringBuilder string = new StringBuilder();
		string.append("It's your turn!\nThese are the action remaining to perform:\n");
		string.append(" - Main Action: " + player.getNumberOfPrimaryAction() + "\n");
		string.append(" - Quick Action: " + (player.isQuickActionExecuted() == true ? 0 : 1) + "\n");
		if (player.isQuickActionExecuted() == true) {
			if (player.getNumberOfPrimaryAction() > 0)
				string.append("You have to perform a main action, so press 'main'");
			else
				string.append("You can't do any action, press 'exit' to end turn");
		} else {
			if (player.getNumberOfPrimaryAction() > 0)
				string.append(
						"Would you like to perform a main action [type 'main'] or a quick action [type 'quick']?");
			else
				string.append("Would you like to perform a quick action [type 'quick'] or end the turn [type 'exit']?");

		}
		return string.toString();
	}

	/**
	 * Asks a player which main action he wants to perform.
	 * 
	 * @return the actual question
	 *
	 */
	public static String askForMainAction() {
		StringBuilder string = new StringBuilder();
		string.append(
				"\nWhich main action you want to perform?\nType 'main' followed by the number [example: 'main2']: ");
		string.append(
				"\n 1) Elect a Councillor\n 2) Acquire a Business Permit Tile\n 3) Build an Emporium using a Permit Tile\n 4) Build an Emporium with the Help of the King");

		return string.toString();
	}

	/**
	 * Asks a player which quick action he wants to perform.
	 * 
	 * @return the actual question
	 *
	 */
	public static String askForQuickAction() {
		StringBuilder string = new StringBuilder();

		string.append(
				"\nWhich quick action you want to perform?\nType 'quick' followed by the number [example: 'quick2']: ");
		string.append(
				"\n 1) Engage an Assistant\n 2) Change Business Permit Tile\n 3) Send an Assistant to Elect a Councillor\n 4) Perform an Additional Main Action");

		return string.toString();
	}

}
