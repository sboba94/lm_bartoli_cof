package it.polimi.ingsw.lm_bartoli_cof.view.client;

import java.rmi.RemoteException;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.view.common.BrokerInterface;
import it.polimi.ingsw.lm_bartoli_cof.view.common.RequestHandler;
import it.polimi.ingsw.lm_bartoli_cof.view.common.SubscriberInterface;

//questa è la classe astratta della factory dalla quale RMIFACTORY e
// SOCKETFACTORY prelevano le informazioni e i metodi

public abstract class PlayerConnectionFactory {

	// questo attributo contiene l'oggetto che modifica la view del gioco
	private final ViewUpdater viewUpdater;

	private final Subscriber subscriber;

	// il token serve a identificare un determinato client
	private Token token;

	/**
	 * SUPERCONSTRUTTORE NECESSITA DEL VIEWUPDATER PER POTER INFORMARE IL BROKER
	 * TRAMITE UN ISCRITTO
	 *
	 * @param viewUpdater
	 *            SERVE A MOSTRARE GLI UPDATE AL SERVER
	 */
	public PlayerConnectionFactory(ViewUpdater viewUpdater) {

		if (viewUpdater == null) {
			throw new IllegalArgumentException("ViewUpdater must not be null.");
		}

		this.viewUpdater = viewUpdater;
		subscriber = new Subscriber(viewUpdater);

		token = null;

	}

	/**
	 * IMPOSTA UNA NUOVA CONNESSIONE AL SERVER TRAMITE UN CLIENT.
	 */
	public void setupConnection() throws RemoteException {

		token = getRequestHandler().connect();
		
		//questo viewUpdater ha lo stesso numero del token!!!
		if (viewUpdater.getPlayerNum() == -1) {
			viewUpdater.setPlayerNum(token.getPlayerNumber());
		}

		getBrokerInterface().subscribe(getSubscriberInterface(), token);

	}

	/**
	 * RESTITUISCE IL GESTORE DELLE RICHIESTE
	 *
	 * @return the remote request handler
	 */
	public abstract RequestHandler getRequestHandler();

	/**
	 * RITORNA L?INTERFACCIA DEL BROKER REMOTO
	 *
	 * @return the remote broker interface
	 */
	public abstract BrokerInterface getBrokerInterface();

	/**
	 * Set an initial token for the connection (this is required for a
	 * reconnection) to be reconnected as a known client. [NOT YET IMPLEMENTED]
	 *
	 * @param token
	 *            an old token to reconnect as a known client
	 */
	protected void setToken(Token token) {

		this.token = token;

	}

	/**
	 * SERVE PER IDENTIFICARE IL CLIENT
	 *
	 * @return the token that identifies the client
	 */
	public Token getToken() {

		return token;

	}

	/**
	 * RESTITUISCE L'OGGETTO CHE PERMETTE L?AGGIORNAMENTO DELLA VIEW
	 *
	 * @return the view updater
	 */
	public ViewUpdater getViewUpdater() {

		return viewUpdater;

	}

	/**
	 * Get the Subscriber.
	 *
	 * @return the subscriber
	 */
	public Subscriber getSubscriber() {

		return subscriber;

	}

	/**
	 * Get the subscriber interface.
	 *
	 * @return the subscriber interface
	 */
	public SubscriberInterface getSubscriberInterface() {

		return subscriber;

	}

}
