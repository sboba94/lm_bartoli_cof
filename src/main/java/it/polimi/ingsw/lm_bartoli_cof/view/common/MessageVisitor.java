package it.polimi.ingsw.lm_bartoli_cof.view.common;

public interface MessageVisitor {

	
	// qui dentro ci vanno tutti i metodi che fanno vedere i messaggi
	////////////////////////////////////// BROADCAST
	////////////////////////////////////// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	
	/**
	 * Display the content of a message
	 *
	 * @param msg
	 *            the received message
	 */
	void display(String msg);
	
	/**
     * Set the player number of this client.
     *
     * @param playerNum the player number of this client
     */
    void setPlayerNum(int playerNum);

    /**
     * Get the player number of this client.
     *
     * @return the player number of this client
     */
    int getPlayerNum();
	

}
