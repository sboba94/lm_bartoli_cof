package it.polimi.ingsw.lm_bartoli_cof.view.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.SubscribeRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.SubscribeResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.view.common.BrokerInterface;
import it.polimi.ingsw.lm_bartoli_cof.view.common.SubscriberInterface;

/**
 * Handle the subscriber in the publisher-subscriber component. It receives
 * subscription requests and after a successful subscription all the messages
 * received from the dispatchMessage method are processed from the queue and
 * sent to the subscriber.
 */
public class SubscriberHandler extends SocketHandler implements SubscriberInterface {

	private static final int TIMEOUT = 3000;

	private final BrokerInterface brokerInterface;

	private final Queue<BroadcastMsg> broadcastBuffer;
	private ObjectOutputStream outputStream;

	boolean isSubscribed;

	/**
	 * Create a new subscriberhandler.
	 *
	 * @param socket
	 *            the socket connection
	 * @param brokerInterface
	 *            the broker interface used to setup new subscriptions
	 */
	public SubscriberHandler(Socket socket, BrokerInterface brokerInterface) {

		super(socket);
		this.brokerInterface = brokerInterface;

		broadcastBuffer = new ConcurrentLinkedQueue<>();

		isSubscribed = false;

	}

	/**
	 * Manages a subscription request by dispatching it to the broker interface.
	 *
	 * @param subscribeRequest
	 *            the subscription request
	 * @throws IOException
	 *             if a problem occurred during subscription
	 */
	private void manageSubscription(SubscribeRequestMsg subscribeRequest) throws IOException {

		try {

			brokerInterface.subscribe(this, subscribeRequest.getToken());
			isSubscribed = true;

		} catch (RemoteException e) {
			e.printStackTrace();
		}

		ResponseMsg responseSub = new SubscribeResponseMsg(isSubscribed);
		sendMsg(responseSub);

	}

	/**
	 * Listens for a subscription request.
	 *
	 * @return the subscription request message
	 */
	private SubscribeRequestMsg receiveSubscriptionRequest() {

		// Return null if there is a problem
		SubscribeRequestMsg subscribeMessage = null;

		try {

			getSocket().setSoTimeout(TIMEOUT);
			ObjectInputStream inputStream = new ObjectInputStream(getSocket().getInputStream());
			Object object = inputStream.readObject();
			if (!(object instanceof SubscribeRequestMsg))
				throw new ClassCastException();
			subscribeMessage = (SubscribeRequestMsg) object;

			// Close the input half od the socket
			try {
				getSocket().shutdownInput();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return subscribeMessage;

	}

	/**
	 * Manages the unsubscription of this client from the broker.
	 */
	private void unsubscribe() {

		if (isSubscribed) {
			try {
				brokerInterface.unsubscribe(this);
				isSubscribed = false;
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Sends a message to the subscriber.
	 *
	 * @param message
	 *            the message to send
	 * @throws IOException
	 *             if there was a problem during the transfer of the message
	 */
	private void sendMsg(Object message) throws IOException {

		try {
			if (outputStream == null) {
				outputStream = new ObjectOutputStream(getSocket().getOutputStream());
			}

			outputStream.writeObject(message);
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
			unsubscribe();
		}

	}

	/**
	 * Send to the subscriber the content of the broadcastBuffer.
	 *
	 * @throws IOException
	 *             if there was a problem during the transfer of the message
	 * @throws InterruptedException
	 *             if there was an interrupted exception
	 */
	private void broadcastBuffer() throws IOException, InterruptedException {

		synchronized (broadcastBuffer) {

			while (broadcastBuffer.isEmpty()) {

				try {
					broadcastBuffer.wait();
				} catch (InterruptedException e) {
					throw e;
				}

			}

			BroadcastMsg broadcastMessage;
			do {
				broadcastMessage = broadcastBuffer.poll();
				if (broadcastMessage != null)
					sendMsg(broadcastMessage);
			} while (broadcastMessage != null);

		}

	}

	@Override
	public void run() {

		try {

			SubscribeRequestMsg subscribeRequest;
			subscribeRequest = receiveSubscriptionRequest();
			manageSubscription(subscribeRequest);

			while (true) {
				broadcastBuffer();
			}

		} catch (IOException e) {

			unsubscribe();

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				getSocket().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void dispatchMessage(BroadcastMsg message) throws RemoteException {

		broadcastBuffer.add(message);
		synchronized (broadcastBuffer) {
			broadcastBuffer.notify();
		}

	}

	@Override
	public void dispatchMessage(ResponseMsg message) {

		

	}

}
