package it.polimi.ingsw.lm_bartoli_cof.view.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;

/**
 * QUESTA INTERFACCIA SERVE A GESTIRE I COMPONENTI PUBBLICI E GLI ISCRITTI.
 * PERMETTERA QUINDI AD UN CLIENT DI ISCRIVERSI O DISISCRIVERSI DAL BROKER
 */
public interface BrokerInterface extends Remote {

	/**
	 * ISCRIVE UN CLIENT AL BROKER IN BASE AL TOKEN IDENTIFICATIVO
	 *
	 * @param subscriber
	 *            the subscription handler
	 * @param token
	 *            the token of the client
	 * @throws RemoteException
	 *             if there was a problem during the connection
	 */
	void subscribe(SubscriberInterface subscriber, Token token) throws RemoteException;

	/**
	 * DISISCRIVE UN CLIENT AL BROKER
	 *
	 * @param subscriber
	 * @throws RemoteException
	 *             if there was a problem during the connection
	 * @throws IllegalArgumentException
	 *             if the subscriber did not exist
	 */
	void unsubscribe(SubscriberInterface subscriber) throws RemoteException, IllegalArgumentException;

}
