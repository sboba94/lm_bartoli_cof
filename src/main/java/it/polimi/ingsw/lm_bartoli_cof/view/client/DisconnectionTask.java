package it.polimi.ingsw.lm_bartoli_cof.view.client;

import java.rmi.RemoteException;
import java.util.TimerTask;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.DisconnectRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.view.common.RequestHandler;

public class DisconnectionTask extends TimerTask {

	private RequestHandler requestHandler;
	private ViewUpdater view;
	private Token token;

	public DisconnectionTask(RequestHandler requestHandler, ViewUpdater view, Token token) {
		this.requestHandler = requestHandler;
		this.view = view;
		this.token = token;
	}

	@Override
	public void run() {
		DisconnectRequestMsg request = new DisconnectRequestMsg(token);
		ResponseMsg response = null;
		try {
			response = requestHandler.processRequest(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		view.update(response);

	}

}
