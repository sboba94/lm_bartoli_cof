package it.polimi.ingsw.lm_bartoli_cof.view.cli;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.CreateGameRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.JoinGameRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.MainActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.QuickActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.SetMyPlayerRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.ViewGameBoardRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.ViewPlayersInfoRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.AcquirePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithKingRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithPermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuyRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ChangePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DisconnectActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorWithAssistantRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.EngageAssistantRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.NothingToSellRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PassRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PerformAddictionallyMainActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.SellRequestMsg;

public class CliInterpreter {

	private CliInterpreter() {
		throw new AssertionError();
	}

	/**
	 * Interpret command line commands and converts them into request messages.
	 *
	 * @param token
	 *            the client token to generate the request
	 * @param cmd
	 *            the command string
	 * @return the corresponding action, null if the command was not well formed
	 */
	public static RequestMsg parseString(Token token, String cmd) {

		if (cmd.matches("new game"))
			// NEW GAME
			return new CreateGameRequestMsg(token);
		else if (cmd.matches("game ([0-9]|[0-9][0-9])")) {
			// GAME JOIN
			String gameIdString = cmd.substring(5);
			return new JoinGameRequestMsg(token, gameIdString);
		} else if (cmd.contains("nickname")) {
			// INSERT NICKNAME
			String nickname = cmd.substring(9);
			return new SetMyPlayerRequestMsg(token, nickname);
		} else if (cmd.matches("quick")) {
			// QUICK ACTION
			return new QuickActionRequestMsg(token);
		} else if (cmd.matches("main")) {
			// MAIN ACTION
			return new MainActionRequestMsg(token);
		} else if (cmd.matches("game info")) {
			// GAME INFO
			return new ViewGameBoardRequestMsg(token);
		} else if (cmd.matches("players info")) {
			// PLAYERS INFO
			return new ViewPlayersInfoRequestMsg(token);
		} else if (cmd.matches("exit")) {
			// EXIT
			return new PassRequestMsg(token);
		} else if (cmd.matches("yes")) {
			// MARKET POSITIVE ANSWER
			return new SellRequestMsg(token);
		} else if (cmd.matches("no")) {
			// MARKET NEGATIVE ANSWER
			return new NothingToSellRequestMsg(token);
		} else if (cmd.matches("buy")) {
			// MARKET BUY ANSWER
			return new BuyRequestMsg(token);
		} else if (cmd.matches("disconnect")) {
			return new DisconnectActionRequestMsg(token);
		} else if (cmd.contains("quick")) {
			// QUICK ACTION CHOISE
			String quickChoise = cmd;
			switch (quickChoise) {
			case "quick1":
				return new EngageAssistantRequestMsg(token);
			case "quick2":
				return new ChangePermitTileRequestMsg(token);
			case "quick3":
				return new ElectCouncillorWithAssistantRequestMsg(token);
			case "quick4":
				return new PerformAddictionallyMainActionRequestMsg(token);
			}
		} else if (cmd.contains("main")) {
			// MAIN ACTION CHOISE
			String mainChoise = cmd;
			switch (mainChoise) {
			case "main1":
				return new ElectCouncillorRequestMsg(token);
			case "main2":
				return new AcquirePermitTileRequestMsg(token);
			case "main3":
				return new BuildEmporiumWithPermitTileRequestMsg(token);
			case "main4":
				return new BuildEmporiumWithKingRequestMsg(token);
			}
		}
		return null;

	}
}
