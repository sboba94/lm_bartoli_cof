package it.polimi.ingsw.lm_bartoli_cof.view.server;

import it.polimi.ingsw.lm_bartoli_cof.controller.GamesController;

/**
 * Entry point to start a GameManager.
 */
public class GameManager {

	public static void main(String[] args) {

		ServerConnectionFactory serverInitializer = new ServerConnectionFactory();
		PublisherInterface publisherInterface = serverInitializer.getPublisherInterface();

		GamesController mainController = new GamesController(publisherInterface);

		serverInitializer.setRequestHandler(mainController);
		serverInitializer.startServers();

		System.out.println("SERVER UP");

	}
}
