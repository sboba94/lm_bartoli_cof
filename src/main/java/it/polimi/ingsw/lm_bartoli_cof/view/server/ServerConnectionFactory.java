package it.polimi.ingsw.lm_bartoli_cof.view.server;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.Executors;

import it.polimi.ingsw.lm_bartoli_cof.view.common.BrokerInterface;
import it.polimi.ingsw.lm_bartoli_cof.view.common.RequestHandler;

/**
 * Creates both RMI and Socket connections for request-response Server and
 * publisher-subscriber Broker.
 */
public class ServerConnectionFactory {

	// SONO I PARAMETRI DI CONNESSIONE DELLE PORTE
	private static final int RMI_PORT = 7777;
	private static final int SOCKET_SERVER_PORT = 1337;
	private static final int SOCKET_PUBLISHER_PORT = 1338;
	private static final int MAX_SUB_THREADS = 24;

	private RequestHandler requestHandler;
	private final Broker broker;
	private Registry registry;
	private SocketServer server, publisher;

	/**
	 * Create a new ServerConnectionFactory, a new Broker is automatically
	 * initialized.
	 */
	public ServerConnectionFactory() {

		broker = new Broker();

	}

	/**
	 * Set the request handler for the server to process requests.
	 *
	 * @param requestHandler
	 *            the request handler for the server
	 */
	public void setRequestHandler(RequestHandler requestHandler) {
		
		this.requestHandler = requestHandler;

	}

	/**
	 * Get the Publisher Interface created at instantiation (the Broker).
	 *
	 * @return get the publisher interface
	 */
	public PublisherInterface getPublisherInterface() {

		return broker;

	}

	/**
	 * FA PARTIRE ENTRAMBE LE CONNESSIONI.
	 */
	public void startServers() {
		if (requestHandler == null) {
			throw new IllegalStateException("RequestHandler is not set.");
		}
		initRMI();
		initSocket();
	}

	/**
	 * Stop both RMI and SOCKET connections.
	 */
	public void stopServers() {
		releaseRMI();
		releaseSocket();
	}

	/**
	 * Initialize RMI communication.
	 */
	private void initRMI() {

		try {

			// CREO IL REGISTRY
			registry = LocateRegistry.createRegistry(RMI_PORT);
			
			// CREA LO STUB
			
			BrokerInterface brokerStub = (BrokerInterface) UnicastRemoteObject.exportObject(broker, 0);
			
			// FA IL BIND DI BROKER
			registry.rebind("Broker", brokerStub);
			
			RequestHandler reqHandlerStub = (RequestHandler) UnicastRemoteObject.exportObject(requestHandler, 0);
			
			// FA IL BIND DI REQUESTHANDLER
			registry.rebind("RequestHandler", reqHandlerStub);
			
		} catch (RemoteException e) {
			System.out.println("Can't initialize RMI Registry.");
		}

	}

	/**
	 * Release RMI resources.
	 */
	private void releaseRMI() {

		try {

			// RIMUOVE IL BIND
			registry.unbind("RequestHandler");
			UnicastRemoteObject.unexportObject(requestHandler, true);

			// RIMUOVE IL BIND
			registry.unbind("Broker");
			UnicastRemoteObject.unexportObject(broker, true);

		} catch (RemoteException | NotBoundException e) {
			System.out.println("Exception thrown while releasing RMI.");
		}

	}

	 /**
     * Initialize Socket communication.
     */
    private void initSocket() {

        try {

            server = new SocketServer(SOCKET_SERVER_PORT, Executors.newCachedThreadPool
                    (), requestHandler);
            publisher = new SocketPubServer(SOCKET_PUBLISHER_PORT, Executors
                    .newFixedThreadPool(MAX_SUB_THREADS), broker);

            server.start();
            publisher.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Release Socket resources.
     */
    private void releaseSocket() {

        publisher.stopServer();
        server.stopServer();

    }

}