package it.polimi.ingsw.lm_bartoli_cof.view.client;

import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;

/**
 * This interface allows to update the view of the game depends on the type of message received
 */
public interface ViewUpdater {

	//GUARDARE CLASSI MSG DI SBOBA
    /**
     * allows to update the view with a received broadcast message.
     *
     * @param msg the received broadcast message
     */
    void update(BroadcastMsg msg);

    //GUARDARE CLASSI MSG DI SBOBA
    /**
     * allows to update the view with a received response message
     *
     * @param msg the received response message
     */
    void update(ResponseMsg msg);

    /**
     * Set the player number of this client in the game, it is used to filter out some
     * broadcast messages.
     *
     * @param playerNum the player number of this client in the game
     */
    void setPlayerNum(int playerNum);

    /**
     * Get the player number of this client in the game.
     *
     * @return the player number of this client in the game
     */
    int getPlayerNum();

}
