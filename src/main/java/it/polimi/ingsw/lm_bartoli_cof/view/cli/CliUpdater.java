package it.polimi.ingsw.lm_bartoli_cof.view.cli;


import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.view.client.ViewUpdater;
import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

public class CliUpdater implements ViewUpdater {

    MessageVisitor visitor;

    public CliUpdater() {

        visitor = new CliMessageVisitor();

    }

    @Override
    public void update(BroadcastMsg msg) {

        msg.showMessage(visitor);

    }

    @Override
    public void update(ResponseMsg msg) {

        msg.showMessage(visitor);

    }

    @Override
    public void setPlayerNum(int playerNum) {

        visitor.setPlayerNum(playerNum);

    }

    @Override
    public int getPlayerNum() {

        return visitor.getPlayerNum();

    }

}
