package it.polimi.ingsw.lm_bartoli_cof.view.client;

import java.rmi.RemoteException;

import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.view.common.SubscriberInterface;

/**
 * The SubscriberInterface implementation that will be exposed to the broker to publish
 * message to the client.
 */
public class Subscriber implements SubscriberInterface{

    protected ViewUpdater viewUpdater;

    //CREA UN SUBSCRIBER IMPOSTANDOGLI IL VIEWUPDATER IM MODO DA MOSTRARE I MESSAGGI
    /**
     * Create a new subscriber "handler".
     *
     * @param viewUpdater the view updater to display received broadcast messages
     */
    Subscriber(ViewUpdater viewUpdater) {

        this.viewUpdater = viewUpdater;

    }
  
    
    //SERVE A DISTRIBUIRE IL MESSAGGIO
    public void dispatchMessage(BroadcastMsg message) throws RemoteException {

        viewUpdater.update(message);

    }


	@Override
	public void dispatchMessage(ResponseMsg message) throws RemoteException {
		
		viewUpdater.update(message);
		
	}

}
