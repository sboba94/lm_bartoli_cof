package it.polimi.ingsw.lm_bartoli_cof.view.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;

/**
 * QUESTA INTERFACCIA GESTICE I COMPONENTI SUL SERVER IN BASE AI MESSAGGI DI
 * RICHIESTA DA UN CLIENT E RITORNA APPROPIATO MESSAGGIO DI RISPOSTA This remote
 */
public interface RequestHandler extends Remote {

	/**
	 * CHIEDE AL SERVER UNA NUOVA CONNESSIONE INVIANDO UN CONNECTREQUESTMESSAGE
	 * CON UN TOKEN VUOTO VERRA RESTITUITO UN NUOVO TOKEN CHE SERVIRà AL CLIET
	 * PER IDENTIFICARE SE STESSO
	 *
	 * @return a new Token to identify the client
	 *
	 * @throws RemoteException
	 *             if a problem occurs during the connection or if it times out
	 */
	Token connect() throws RemoteException;

	/**
	 * MANDA UNA RICHIESTA CHE DEVE ESSERE ELABORATA DAL SERVER E SI METTE IN
	 * ATTESA DI UNA RISPOSTA
	 *
	 * @param request
	 *            the request to be processed by the server
	 * @return the response obtained from the server
	 *
	 * @throws RemoteException
	 *             if a problem occurs during the connection or if it times out
	 */
	ResponseMsg processRequest(RequestMsg request) throws RemoteException;
	
	String giveMeSomething(RequestMsg request, String question) throws RemoteException;

}
