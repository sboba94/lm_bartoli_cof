package it.polimi.ingsw.lm_bartoli_cof.view.cli;

import it.polimi.ingsw.lm_bartoli_cof.view.common.MessageVisitor;

//GUARDA I MESSAGGI IN ARRIVO E LI PUBBLICA SULLA CLI
public class CliMessageVisitor implements MessageVisitor{
	
	private int playerNum;

    public CliMessageVisitor() {

        playerNum = -1;

    }

    public int getPlayerNum() {

        return playerNum;

    }

    public void setPlayerNum(int playerNum) {

        this.playerNum = playerNum;

    }

    private void cli(String string) {
    	
        System.out.println(string);

    }

	@Override
	public void display(String msg) {
		this.cli(msg);
	}

}
