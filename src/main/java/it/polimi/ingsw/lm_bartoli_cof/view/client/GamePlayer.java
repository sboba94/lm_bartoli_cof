package it.polimi.ingsw.lm_bartoli_cof.view.client;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import it.polimi.ingsw.lm_bartoli_cof.messages.request.CreateGameRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.InitRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.MainActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.AcquirePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithKingRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithPermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuyRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ChangePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorWithAssistantRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.SellRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.view.cli.CliInterpreter;
import it.polimi.ingsw.lm_bartoli_cof.view.cli.CliUpdater;
import it.polimi.ingsw.lm_bartoli_cof.view.common.RequestHandler;

public class GamePlayer {
	// serve a fare partire la partita del client, questa classe fa sciegliere
	// all'utente se usare connessione socket o RMI che gestisce con una
	// factory, dopo di che si getisce tutte le request/response

	// private static final Logger LOG =
	// Logger.getLogger(GamePlayer.class.getName());
	//
	public static void main(String[] args) throws RemoteException, NotBoundException {

		boolean firstTime = true;

		Scanner scanner = new Scanner(System.in);

		String connectionType;
		do {
			System.out.println("Enter 'rmi' or 'socket' to choose the connection type:");
			connectionType = scanner.nextLine();
		} while (!connectionType.matches("^(rmi|socket)$"));

		// CREO LA CONNESSIONE RMI

		ViewUpdater view = new CliUpdater();

		try {
			PlayerConnectionFactory playerConnectionFactory;

			if (connectionType.matches("^(rmi)$")) {
				playerConnectionFactory = new RMIFactory("localhost", view);
			} else {
				playerConnectionFactory = new SocketFactory("localhost", view);
			}

			// RICHIEDO TRAMITE CONNESSIONE IL GESTORE DELLE RICHIESTE
			RequestHandler requestHandler = playerConnectionFactory.getRequestHandler();

			RequestMsg request;
			ResponseMsg response;

			while (true) {

				if (firstTime) {
					request = new InitRequestMsg(playerConnectionFactory.getToken());
					firstTime = false;
				} else {
					String cmd = scanner.nextLine();
					request = CliInterpreter.parseString(playerConnectionFactory.getToken(), cmd);
				}

				if (request == null) {
					System.out.println("INVALID COMMAND\nPlease type a valid choise\n");
					continue;

				} else if (request instanceof CreateGameRequestMsg) {
					String cmd;
					printOnScreen("Choose a map typing a number between 1 and 8: ");
					// int i = 0;
					// do {
					// if (i > 0)
					// printOnScreen("Please type a valid choise");
					cmd = scanner.nextLine();
					// } while (cmd.matches("^(1|2|3|4|5|6|7|8) + \n"));
					((CreateGameRequestMsg) request).setMapRoot("maps/Map" + cmd + ".xml");
				}

				//// Elect a Councillor ////

				// DONE
				else if (request instanceof ElectCouncillorRequestMsg) {
					
					String cmd;
					printOnScreen(requestHandler.giveMeSomething(request, "AllCouncilBalcony"));
					printOnScreen(
							"Choose the board on which you want to elect the Councillor ['Coast' or 'Hills' or 'Mountain' or 'King']: ");
					int i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
					} while (cmd.matches("^(\\bCoast\\b|\\bHills\\b|\\bMountain\\b|\\bKing\\b) + \n"));
					((ElectCouncillorRequestMsg) request).setBoard(cmd);

					printOnScreen(requestHandler.giveMeSomething(request, "AvailableColors"));
					printOnScreen("Type the color of the Councillor that you want to elect [example: Black]: ");
					i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
					} while (cmd.matches("^(\\w)+$ + \n"));
					((ElectCouncillorRequestMsg) request).setColor(cmd);

				}

				//// Acquire a Business Permit Tile ////

				// DONE
				else if (request instanceof AcquirePermitTileRequestMsg) {
					
					String cmd;

					// Shows the politic card of a player
					printOnScreen(requestHandler.giveMeSomething(request, "PoliticInMyHand"));

					// Shows the council balconies and the permit boards of the
					// regions
					printOnScreen(requestHandler.giveMeSomething(request, "RegionCouncilBalconies"));

					// Ask to choose the council balcony on which perform the
					// action
					printOnScreen("Choose the council that you want to satisfy ['Coast' or 'Hills' or 'Mountain']: ");
					int i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
					} while (cmd.matches("^(\\bCoast\\b|\\bHills\\b|\\bMountain\\b) + \n"));
					((AcquirePermitTileRequestMsg) request).setPermitBoard(cmd);

					// Ask to choose the politic cards that the player wants to
					// use for satisfy the council
					StringBuilder string = new StringBuilder();
					printOnScreen(
							"Choose the politic cards that you want to use for satisfy the council [for example: 'Black,Pink,White']: ");
					i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
						string.append(cmd);
					} while (cmd.matches("(?:|\\s+,\\s+)((\\w)(?:,)*)* + \n"));
					((AcquirePermitTileRequestMsg) request).setPoliticCards(string.toString());

					printOnScreen(requestHandler.giveMeSomething(request, "TilesOnBoard"));
					printOnScreen("Choose the business permit tile that you want  to acquire typing is number: ");
					// Ask to choose the permit tile that the player wants to
					// take
					i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
						string.append(cmd);
					} while (cmd.matches("(1|2) + \n"));
					((AcquirePermitTileRequestMsg) request).setPermitTile(cmd);

				}

				//// Build an Emporium Using a Permit Tile ////

				// DONE
				else if (request instanceof BuildEmporiumWithPermitTileRequestMsg) {
					
					String cmd;
					if (requestHandler.giveMeSomething(request, "UsablePermitTile").isEmpty()) {
						printOnScreen(requestHandler.giveMeSomething(request, "UsablePermitTiles"));
						printOnScreen("Choose the business permit tile that you want to use: ");
						cmd = scanner.nextLine();
						((BuildEmporiumWithPermitTileRequestMsg) request).setTile(cmd);

						printOnScreen(
								"Choose the city on which you want to build an emporium from the following typing his name: ");
						int i = 0;
						do {
							if (i > 0)
								printOnScreen("Please type a valid choise");
							printOnScreen(requestHandler.giveMeSomething(request, "CityOnPermitTile"));
							cmd = scanner.nextLine();
						} while (cmd.matches("^(\\w)+$ + \n"));
						((BuildEmporiumWithPermitTileRequestMsg) request).setCity(cmd);
					} else {
						printOnScreen("\n------> You don't have usable permit tiles in your hand\n");
						request = new MainActionRequestMsg(request.getToken());
					}
				}

				//// Build an Emporium with the help of the King ////

				// DONE
				else if (request instanceof BuildEmporiumWithKingRequestMsg) {
					
					String cmd;
					StringBuilder string = new StringBuilder();
					printOnScreen(requestHandler.giveMeSomething(request, "PoliticInMyHand"));
					printOnScreen(requestHandler.giveMeSomething(request, "KingCouncilBalcony"));
					printOnScreen(
							"Choose the politic cards that you want to use for satisfy the council [for example: 'Black,Pink,White']: ");
					int i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
						string.append(cmd);
					} while (cmd.matches("(?:|\\s+,\\s+)((\\w)(?:,)*)* + \n"));
					((BuildEmporiumWithKingRequestMsg) request).setPoliticCards(string.toString());

					printOnScreen("Choose the city that you want to reach with the King: ");
					i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						printOnScreen(requestHandler.giveMeSomething(request, "City"));
						cmd = scanner.nextLine();
					} while (cmd.matches("^(\\w)+$ + \n"));
					((BuildEmporiumWithKingRequestMsg) request).setCity(cmd);
				}

				//// Change Business Permit Tiles ////

				// DONE
				else if (request instanceof ChangePermitTileRequestMsg) {
					
					String cmd;
					printOnScreen(requestHandler.giveMeSomething(request, "RegionPermitBoards"));
					printOnScreen(
							"Choose the board where you want to change tile ['Coast' or 'Hills' or 'Mountain']: ");
					int i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
					} while (cmd.matches("^(\\bCoast\\b|\\bHills\\b|\\bMountain\\b) + \n"));
					((ChangePermitTileRequestMsg) request).setPermitBoard(cmd);

				}

				//// Send an Assistant to Elect a Councillor ////

				else if (request instanceof ElectCouncillorWithAssistantRequestMsg) {
					
					String cmd;
					printOnScreen(requestHandler.giveMeSomething(request, "AllCouncilBalcony"));
					printOnScreen(
							"Choose the board on which you want to elect the councillor ['Coast' or 'Hills' or 'Mountain' or 'King']: ");
					int i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
					} while (cmd.matches("^(\\bCoast\\b|\\bHills\\b|\\bMountain\\b|\\bKing\\b) + \n"));
					((ElectCouncillorWithAssistantRequestMsg) request).setBoard(cmd);

					printOnScreen(requestHandler.giveMeSomething(request, "AvailableColors"));
					printOnScreen("Type the color of the Councillor that you want to elect [example: Black]: ");
					i = 0;
					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
					} while (cmd.matches("^(\\w)+$ + \n"));
					((ElectCouncillorWithAssistantRequestMsg) request).setColor(cmd);

				}

				//// Sell Action ////

				else if (request instanceof SellRequestMsg) {
					
					String cmd;

					// Choose the object to sell
					printOnScreen("Insert ID,PRICE in ordert to put a permit tile for sale, finally type quit");

					printOnScreen(requestHandler.giveMeSomething(request, "UsablePermitTiles"));

					printOnScreen("\n******** Business Permit Tiles ********\n");
					printOnScreen(requestHandler.giveMeSomething(request, "UsablePermitTile"));
					printOnScreen(
							"Type the number of the tile that you want to sell followed by the selling price [example: 10,100]: ");
					int i = 0;

					do {
						if (i > 0)
							printOnScreen("Please type a valid choise");
						cmd = scanner.nextLine();
						if (!cmd.equals("quit"))
							((SellRequestMsg) request).getPermitSellList().add(cmd);
					} while (!cmd.equals("quit"));

					printOnScreen("Insert ID,PRICE in ordert to put a politic card for sale, finally type quit");
					printOnScreen(requestHandler.giveMeSomething(request, "NumeratedPoliticInMyHand"));
					do {
						cmd = scanner.nextLine();
						if (!cmd.equals("quit"))
							((SellRequestMsg) request).getPoliticSellList().add(cmd);
					} while (!cmd.equals("quit"));
					printOnScreen("You have " + requestHandler.giveMeSomething(request, "NumberOfAssistant"));
					do {
						printOnScreen("Do you want to sell an assistant? if yes type price, else type quit");
						cmd = scanner.nextLine();
						if (!cmd.equals("quit"))
							((SellRequestMsg) request).getAssistantSellList().add(cmd);
					} while (!cmd.equals("quit"));

				}

				//// Buy Action ////

				else if (request instanceof BuyRequestMsg) {
					
					String cmd;
					String s = requestHandler.giveMeSomething(request, "BuyList");

					if (s == null) {
						printOnScreen("There is nothing to buy");
					} else {
						printOnScreen(s);
						printOnScreen("Insert ID in ordert to buy a permit tile for sale, finally type quit");
						do {
							cmd = scanner.nextLine();
							if (!cmd.equals("quit"))
								((BuyRequestMsg) request).getPermitBuyList().add(cmd);
						} while (!cmd.equals("quit"));
						printOnScreen("Insert ID in ordert to buy a politic card for sale, finally type quit");
						do {
							cmd = scanner.nextLine();
							if (!cmd.equals("quit"))
								((BuyRequestMsg) request).getPoliticBuyList().add(cmd);
						} while (!cmd.equals("quit"));
						do {
							printOnScreen("Do you want to buy an assistant? if yes type ID, else type quit");
							cmd = scanner.nextLine();
							if (!cmd.equals("quit"))
								((BuyRequestMsg) request).getAssistantBuyList().add(cmd);
						} while (!cmd.equals("quit"));
					}
				}

				response = requestHandler.processRequest(request);
				view.update(response);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void printOnScreen(String string) {
		System.out.println(string);
	}
}