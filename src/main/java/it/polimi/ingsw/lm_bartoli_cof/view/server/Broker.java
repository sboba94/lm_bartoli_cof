package it.polimi.ingsw.lm_bartoli_cof.view.server;

import java.rmi.RemoteException;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplayToClient;
import it.polimi.ingsw.lm_bartoli_cof.view.common.BrokerInterface;
import it.polimi.ingsw.lm_bartoli_cof.view.common.SubscriberInterface;

public class Broker implements BrokerInterface, PublisherInterface {

	// insieme di stringhe che corrispondono ai nomi degli oggetti remoti
	private final List<String> topics;

	// per ogni topic c'è la lista degli utenti iscritti
	private final Map<String, LinkedList<Token>> subscriptions;

	// ad un tipo di token viene associato uno ed un solo subscriber interface
	// ?!?!?
	private final Map<Token, SubscriberInterface> subscribers;

	/**
	 * Create a new Broker.
	 */
	public Broker() {
		subscriptions = new HashMap<>();
		subscribers = new HashMap<>();
		topics = new LinkedList<String>();
	}

	@Override
	public synchronized void subscribe(SubscriberInterface subscriber, Token token) {

		if (subscriber == null) {
			throw new IllegalArgumentException("SubscriberInterface must not be null.");
		} else if (token == null) {
			throw new IllegalArgumentException("Token must not be null.");
		}

		if (subscribers.values().contains(subscriber)) {
			return; // Already in subscribers
		} else if (subscribers.keySet().contains(token)) {
			throw new IllegalArgumentException(
					"Subscriber is already connected to the " + "broker with another interface.");
		}

		subscribers.put(token, subscriber);

	}

	@Override
	public synchronized void unsubscribe(SubscriberInterface subscriber) {

		// If it was already removed nothing happens
		boolean unsubscribed = subscribers.values().remove(subscriber);

		if (unsubscribed) {
			// gestire ?
		}

	}

	// pubblica un messaggio a tutti gli utenti legati a quel topic.. da
	// rivedere!!
	@Override
	public synchronized void publish(BroadcastMsg message, String topic) {

		if (message == null) {
			throw new IllegalArgumentException("Message must not be null.");
		} else if (!topic.contains(topic)) {
			throw new IllegalArgumentException("Invalid topic.");
		}

		// Find the clients subscribed to a topic
		Collection<Token> subscribedToTopic = subscriptions.get(topic);

		for (Token token : subscribedToTopic) {
			SubscriberInterface subscriber = subscribers.get(token);
			if (!(message instanceof DisplayToClient)) {
				// Get the subscriber interface to communicate with the client
				// (if
				// any)
				

				if (subscriber == null) {
					System.out.println("Subscriver for token: " + token.getPlayerNumber() + "not found");
					// GESTIRE ECCEZIONE
				}

				try {
					// Try to dispatch the message
					subscriber.dispatchMessage(message);
				} catch (RemoteException e) {
					System.out.println("Remote exception" + e);
					e.printStackTrace();
				} catch (NullPointerException e) {
					System.out.println("NullPointer exception" + e);
					e.printStackTrace();
				}
			} else if (message instanceof DisplayToClient && token.equals(((DisplayToClient) message).getToken())) {
				
				// Get the subscriber interface to communicate with the client
				// (if any)
				try {
					// Try to dispatch the message
					subscriber.dispatchMessage(message);
				} catch (RemoteException e) {
					System.out.println("Remote exception" + e);
					e.printStackTrace();
				} catch (NullPointerException e) {
					System.out.println("NullPointer exception" + e);
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public synchronized void addTopic(String topic) {

		if (topics.contains(topic)) {
			throw new IllegalArgumentException("Topic already present.");
		}

		topics.add(topic);

	}

	@Override
	public void addTopic(String topic, LinkedList<Token> clients) {

		addTopic(topic);
		subscribeClientsToTopic(topic, clients);

	}

	@Override
	public void subscribeClientToTopic(String topic, Token client) {

		if (!topics.contains(topic)) {
			throw new IllegalArgumentException("Invalid topic.");
		} else if (client == null) {
			throw new IllegalArgumentException("Client must not be null");
		}
		if (!subscriptions.containsKey((topic)))
			subscriptions.put(topic, new LinkedList<Token>());
		subscriptions.get(topic).add(client);
	}

	@Override
	public void subscribeClientsToTopic(String topic, LinkedList<Token> clients) {

		if (!topics.contains(topic)) {
			throw new IllegalArgumentException("Invalid topic(does not exist).");
		} else if (clients == null) {
			throw new IllegalArgumentException("Clients must not be null");
		}
		if (!subscriptions.containsKey((topic)))
			subscriptions.put(topic, new LinkedList<Token>());
		subscriptions.get(topic).addAll(clients);

	}

	@Override
	public void unsubscribeClientFromTopic(String topic, Token client) {

		if (!topics.contains(topic)) {
			throw new IllegalArgumentException("Invalid topic (does not exist).");
		} else if (client == null) {
			throw new IllegalArgumentException("Client must not be null");
		}

		subscriptions.remove(topic, client);

	}

	@Override
	public void removeClient(Token client) {

		if (client == null) {
			throw new IllegalArgumentException("Client must not be null");
		}

		subscriptions.values().remove(client);

	}

	@Override
	public void removeClients(Set<Token> clients) {

		if (clients == null) {
			throw new IllegalArgumentException("Client must not be null");
		}

		subscriptions.values().removeAll(clients);

	}

	@Override
	public void removeTopic(String topic) {

		if (!topics.contains(topic)) {
			throw new IllegalArgumentException("Invalid topic.");
		}

		subscriptions.remove(topic);
		topics.remove(topic);

	}

}