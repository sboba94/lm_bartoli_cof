package it.polimi.ingsw.lm_bartoli_cof.view.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;

/**
 * LITERFACCIA é USATA DA COMPONENTI PUBBLICHE PER COMUNICARE CON GLI "ABBONATI".
 */
public interface SubscriberInterface extends Remote {

    /**
     *  SERVE A MANDARE I MESSAGGI AI VAREI SUBSCRIBER
     *
     * @param message the message to dispatch
     * @throws RemoteException if there was a problem during data transfer
     */
    void dispatchMessage(BroadcastMsg message) throws RemoteException;
    
    /**
     *  SERVE A MANDARE I MESSAGGI AI VAREI SUBSCRIBER
     *
     * @param message the message to dispatch
     * @throws RemoteException if there was a problem during data transfer
     */
    void dispatchMessage(ResponseMsg message) throws RemoteException;

}

