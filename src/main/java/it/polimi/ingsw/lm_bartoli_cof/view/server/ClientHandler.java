package it.polimi.ingsw.lm_bartoli_cof.view.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.GameResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.view.common.RequestHandler;

/**
 * Handles a single request-response connection from a client. After receiving a request
 * for a new socket we send an appropriate response and we immediately close the
 * connection.
 */
public class ClientHandler extends SocketHandler {

    
    private static final int TIMEOUT = 3000;
    private final RequestHandler requestHandler;

    /**
     * Creates a new client handler.
     *
     * @param client the socket used by the client
     * @param requestHandler the interface to handle client requests
     */
    public ClientHandler(Socket client, RequestHandler requestHandler) {

        super(client);
        this.requestHandler = requestHandler;

    }

    /**
     * Receive a request message object from a client.
     *
     * @return the object received from the client
     *
     * @throws ClassNotFoundException problems while reading from the input stream
     * @throws IOException            in case of error while receiving message from
     *                                client
     */
    protected Object receiveObject() throws ClassNotFoundException, IOException {

        Object receivedObject;

        try {

            getSocket().setSoTimeout(TIMEOUT);

            ObjectInputStream inputStream = new ObjectInputStream(getSocket()
                    .getInputStream());

            receivedObject = inputStream.readObject();

        } catch (SocketException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } catch (ClassNotFoundException e) {
            throw e;
        }

        return receivedObject;

    }

    /**
     * Send a response to the client after receiving a request, this also closes the
     * connection.
     *
     * @param response the response to send to the client
     */
    protected void sendResponse(Object response) {

        try {

            ObjectOutputStream outputStream = new ObjectOutputStream(getSocket()
                    .getOutputStream());
            if (response != null) {
                outputStream.writeObject(response);
            } else {
                outputStream.writeObject(new InvalidRequestResponseMsg("There was a problem " +
                        "handling the request."));
            }
            outputStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                getSocket().close();
                // Response sent (or failed, hopefully not), try to close the connection
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void run() {

        ResponseMsg response;

        try {

            Object request = receiveObject();

            if (request instanceof RequestMsg) {
                response = requestHandler.processRequest((RequestMsg) request);
            } else {
                response = new InvalidRequestResponseMsg("Unknown request.");
            }
            
            if(response instanceof GameResponseMsg)
            	sendResponse(((GameResponseMsg) response).getMsg());
            else 
            	sendResponse(response);


        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            
        }

    }

}
