package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class NoMorePoliticCardsException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public NoMorePoliticCardsException(String message) {
		super(message);
	}

}
