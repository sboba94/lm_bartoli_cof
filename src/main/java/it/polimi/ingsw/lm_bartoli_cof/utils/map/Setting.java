package it.polimi.ingsw.lm_bartoli_cof.utils.map;

import java.io.File;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BonusRewardTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.KingRewardTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.CityColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionName;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CitiesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.EdgesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NobilityTrackNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PoliticDeckNotConfiguredException;

public class Setting {

	private int idGame;
	private Document doc;

	public Setting(String root) {
		try {
			File inputFile = new File(root);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Document getConfigFile() {
		return doc;
	}

	public int getIdGame() {
		return idGame;
	}

	public void setIdGame(int idGame) {
		this.idGame = idGame;
	}

	private NodeList getListOf(String attribute) {
		NodeList items = this.doc.getElementsByTagName(attribute);
		return items;
	}

	/**
	 * This method scans the configuration file for the generation of the
	 * Councillor Pool.
	 * 
	 * @return toAddPool - a {@link LinkedList} of Councillor
	 * @throws CouncillorPoolNotConfiguredException
	 */
	public LinkedList<Councillor> generateCouncillorPool() throws CouncillorPoolNotConfiguredException {

		Element councillorPool = (Element) getListOf("councillorPool").item(0);

		NodeList councillorList = councillorPool.getElementsByTagName("color");
		LinkedList<Councillor> toAddPool = new LinkedList<Councillor>();

		if (councillorList.getLength() == 0) {
			throw new CouncillorPoolNotConfiguredException("Councillor Pool missed in the configuration file");
		} else {

			for (int i = 0; i < councillorList.getLength(); i++) {
				switch (councillorList.item(i).getTextContent()) {
				case "BLACK":
					toAddPool.add(new Councillor(CouncillorColors.BLACK));
					break;
				case "BLUE":
					toAddPool.add(new Councillor(CouncillorColors.BLUE));
					break;
				case "ORANGE":
					toAddPool.add(new Councillor(CouncillorColors.ORANGE));
					break;
				case "PINK":
					toAddPool.add(new Councillor(CouncillorColors.PINK));
					break;
				case "PURPLE":
					toAddPool.add(new Councillor(CouncillorColors.PURPLE));
					break;
				case "WHITE":
					toAddPool.add(new Councillor(CouncillorColors.WHITE));
					break;
				}
			}
			return toAddPool;
		}

	}

	/**
	 * This method scans the configuration file for the generation of the
	 * Politic Cards Deck.
	 * 
	 * @return politicCards - a {@link LinkedList} of Politic Cards
	 * @throws PoliticDeckNotConfiguredException
	 */
	public LinkedList<PoliticCard> generatePoliticDeck() throws PoliticDeckNotConfiguredException {

		Element politicDeck = (Element) this.getListOf("politicDeck").item(0);

		NodeList cardList = politicDeck.getElementsByTagName("color");
		LinkedList<PoliticCard> politicCards = new LinkedList<PoliticCard>();

		if (cardList.getLength() == 0) {
			throw new PoliticDeckNotConfiguredException("Politic Card Deck missed in the configuration file");
		} else {

			for (int i = 0; i < cardList.getLength(); i++) {
				switch (cardList.item(i).getTextContent()) {
				case "BLACK":
					politicCards.add(new PoliticCard(CouncillorColors.BLACK));
					break;
				case "BLUE":
					politicCards.add(new PoliticCard(CouncillorColors.BLUE));
					break;
				case "MULTICOLOR":
					politicCards.add(new PoliticCard(CouncillorColors.MULTICOLOR));
					break;
				case "ORANGE":
					politicCards.add(new PoliticCard(CouncillorColors.ORANGE));
					break;
				case "PINK":
					politicCards.add(new PoliticCard(CouncillorColors.PINK));
					break;
				case "PURPLE":
					politicCards.add(new PoliticCard(CouncillorColors.PURPLE));
					break;
				case "WHITE":
					politicCards.add(new PoliticCard(CouncillorColors.WHITE));
					break;
				}

			}
			return politicCards;
		}

	}

	/**
	 * This method scans the configuration file for the generation of a Business
	 * Permit Deck.
	 * 
	 * @return toAddPermitTiles - a {@link LinkedList} of Business Permit Tiles
	 * @throws PermitDeckNotConfiguredException
	 */
	public LinkedList<BusinessPermitTile> generatePermitDeck(RegionName region, LinkedList<City> citiesInRegion)
			throws PermitDeckNotConfiguredException {

		Element permitDeck = (Element) this.getListOf("permitDeck").item(0);

		NodeList permitTiles = permitDeck.getElementsByTagName("permitTile");
		LinkedList<BusinessPermitTile> toAddPermitTiles = new LinkedList<BusinessPermitTile>();

		if (permitTiles.getLength() == 0) {
			throw new PermitDeckNotConfiguredException("Business Permit Deck missed in the configuration file");
		} else {

			for (int i = 0; i < permitTiles.getLength(); i++) {

				Element permitTile = (Element) permitTiles.item(i);

				Element permitCitiesList = (Element) permitTile.getElementsByTagName("cities").item(0);
				NodeList city = permitCitiesList.getElementsByTagName("city");
				LinkedList<City> tempCities = new LinkedList<City>();
				LinkedList<Bonus> tempBonuses = new LinkedList<Bonus>();

				for (int j = 0; j < city.getLength(); j++) {

					String cityName = city.item(j).getTextContent();

					// checks if the text content of a specific tag <city> is
					// contained in the citiesInRegione list
					for (int n = 0; n < citiesInRegion.size(); n++) {

						if (citiesInRegion.get(n).getName().equals(cityName)) {
							City toAddCity = new City(cityName);
							tempCities.add(toAddCity);

						}

					}

				}

				// if the list of cities of the same region is empty
				// stops the iteration
				if (!tempCities.isEmpty()) {

					Element bonusesList = (Element) permitTile.getElementsByTagName("bonuses").item(0);
					NodeList bonus = bonusesList.getElementsByTagName("*");

					for (int n = 0; n < bonus.getLength(); n++) {

						// we admit Business Permit Tiles without any bonus
						// so we did not throw a NotValidPermitTileException in
						// this particular case
						if (Integer.parseInt(bonus.item(n).getTextContent()) != 0) {
							Bonus auxBonus = new Bonus();
							String name = bonus.item(n).getNodeName();
							auxBonus.setType(BonusType.valueOf(name.toUpperCase()));
							auxBonus.setValue(Integer.parseInt(bonus.item(n).getTextContent()));
							tempBonuses.add(auxBonus);
						}

					}

				} else {
					continue;
				}

				BusinessPermitTile newTile = new BusinessPermitTile(tempCities, tempBonuses);
				toAddPermitTiles.add(newTile);

			}

		}

		return toAddPermitTiles;
	}

	/**
	 * This method scans the configuration file for the generation of a King
	 * Rewards Deck.
	 * 
	 * @return kingRewardTiles - a {@link LinkedList} of King Rewards Tiles
	 * @throws KingDeckNotConfiguredException
	 */
	public LinkedList<KingRewardTile> generateKingRewardsDeck() throws KingDeckNotConfiguredException {

		Element kingTiles = (Element) this.getListOf("kingTiles").item(0);

		NodeList kingTilesList = kingTiles.getElementsByTagName("kingTile");
		LinkedList<KingRewardTile> kingRewardTiles = new LinkedList<KingRewardTile>();

		if (kingTilesList.getLength() == 0) {
			throw new KingDeckNotConfiguredException("King Deck missed in the configuration file");
		} else {

			for (int i = 0; i < kingTilesList.getLength(); i++) {

				Element kingTile = (Element) kingTilesList.item(i);
				Bonus toAddBonus = new Bonus();

				// King Reward Tiles has only victory points bonus
				Element victoryBonus = (Element) kingTile.getElementsByTagName("victory").item(0);

				toAddBonus.setType(BonusType.VICTORY);
				toAddBonus.setValue(Integer.parseInt(victoryBonus.getTextContent()));

				KingRewardTile newTile = new KingRewardTile(toAddBonus, i + 1);
				kingRewardTiles.add(newTile);
			}
		}

		return kingRewardTiles;
	}

	/**
	 * This method scans the configuration file for the generation of the Bonus
	 * Tiles.
	 * 
	 * @return kingRewardTiles - a {@link LinkedList} of Bonus Tiles
	 * @throws BonusTilesNotConfiguredException
	 */
	public LinkedList<BonusRewardTile> generateBonusTiles() throws BonusTilesNotConfiguredException {

		Element bonusTiles = (Element) this.getListOf("bonusTiles").item(0);

		NodeList bonusTilesList = bonusTiles.getElementsByTagName("bonusTile");
		LinkedList<BonusRewardTile> toAddBonusTiles = new LinkedList<BonusRewardTile>();

		if (bonusTilesList.getLength() == 0) {
			throw new BonusTilesNotConfiguredException("Bonus Tiles missed in the configuration file");
		} else {

			for (int i = 0; i < bonusTilesList.getLength(); i++) {

				Element tile = (Element) bonusTilesList.item(i);

				Element position = (Element) tile.getElementsByTagName("position").item(0);
				String positionAttribute = position.getTextContent();

				Element bonus = (Element) tile.getElementsByTagName("victory").item(0);

				Bonus auxBonus = new Bonus();
				auxBonus.setType(BonusType.VICTORY);
				auxBonus.setValue(Integer.parseInt(bonus.getTextContent()));

				BonusRewardTile bonusTile = new BonusRewardTile(auxBonus, positionAttribute);
				toAddBonusTiles.add(bonusTile);

			}

		}

		return toAddBonusTiles;

	}

	/**
	 * This method scans the configuration file for the generation of all the
	 * cities of a GameMap.
	 * 
	 * @throws CitiesNotConfiguredException
	 */
	public LinkedList<City> fillGraphWithCities() throws CitiesNotConfiguredException {

		Element citiesList = (Element) this.getListOf("citiesList").item(0);

		NodeList cities = citiesList.getElementsByTagName("city");
		LinkedList<City> citiesForGraph = new LinkedList<City>();

		if (cities.getLength() == 0) {
			throw new CitiesNotConfiguredException("Cities missed in the configuration file");
		} else {

			for (int i = 0; i < cities.getLength(); i++) {

				Element city = (Element) cities.item(i);

				// creation of temporary variable in which setting the
				// attributes of the city from the configuration file
				City tempCity = new City(city.getElementsByTagName("name").item(0).getTextContent());

				// region setting
				switch (city.getElementsByTagName("region").item(0).getTextContent()) {
				case "COAST":
					tempCity.setRegionByEnum(RegionName.COAST);
					break;
				case "HILLS":
					tempCity.setRegionByEnum(RegionName.HILLS);
					break;
				case "MOUNTAIN":
					tempCity.setRegionByEnum(RegionName.MOUNTAIN);
					break;
				}

				// color setting
				switch (city.getElementsByTagName("color").item(0).getTextContent()) {
				case "PURPLE":
					tempCity.setColor(CityColors.PURPLE);
					break;
				case "GOLD":
					tempCity.setColor(CityColors.GOLD);
					break;
				case "IRON":
					tempCity.setColor(CityColors.IRON);
					break;
				case "BRONZE":
					tempCity.setColor(CityColors.BRONZE);
					break;
				case "SILVER":
					tempCity.setColor(CityColors.SILVER);
					break;
				}

				// bonus setting
				Element bonuses = (Element) city.getElementsByTagName("bonuses").item(0);

				NodeList bonusList = bonuses.getElementsByTagName("*");
				LinkedList<Bonus> toAddRewardBonuses = new LinkedList<Bonus>();

				for (int j = 0; j < bonusList.getLength(); j++) {

					if (Integer.parseInt(bonusList.item(j).getTextContent()) != 0) {
						Bonus auxBonus = new Bonus();
						String name = bonusList.item(j).getNodeName();
						auxBonus.setType(BonusType.valueOf(name.toUpperCase()));
						auxBonus.setValue(Integer.parseInt(bonusList.item(j).getTextContent()));
						toAddRewardBonuses.add(auxBonus);
					}

				}

				tempCity.setToken(toAddRewardBonuses);

				// everything is fine -> add the city
				citiesForGraph.add(tempCity);
			}
		}

		return citiesForGraph;
	}

	/**
	 * This method scans the configuration file for the generation of all the
	 * edges of the graph in the GameMap.
	 * 
	 * @throws EdgesNotConfiguredException
	 * 
	 */
	public LinkedHashMap<String, LinkedList<String>> fillGraphWithEdges() throws EdgesNotConfiguredException {

		Element edges = (Element) this.getListOf("edges").item(0);

		NodeList edgesList = edges.getElementsByTagName("edge");
		LinkedHashMap<String, LinkedList<String>> graphEdges = new LinkedHashMap<String, LinkedList<String>>();

		if (edgesList.getLength() == 0) {
			throw new EdgesNotConfiguredException("Edges missed in the configuration file");
		} else {

			for (int i = 0; i < edgesList.getLength(); i++) {

				Element edge = (Element) edgesList.item(i);

				NodeList cities = edge.getElementsByTagName("city");
				LinkedList<String> toAddCities = new LinkedList<String>();

				Element vertexCity = (Element) cities.item(0);
				String keyCity = vertexCity.getTextContent();
				// fills the list of cities that has an edge with the vertex
				for (int j = 1; j < cities.getLength(); j++) {
					toAddCities.add(new String(cities.item(j).getTextContent()));
				}
				graphEdges.put(keyCity, toAddCities);

			}

		}

		return graphEdges;
	}

	/**
	 * This method scans the configuration file for the generation of the
	 * Nobility Track.
	 * 
	 * @throws NobilityTrackNotConfiguredException
	 * 
	 */
	public LinkedHashMap<Integer, LinkedList<Bonus>> generateNobilityTrack()
			throws NobilityTrackNotConfiguredException {

		// better implementation if configuration file does not contain bonuses
		// in places where the position of the Nobility Track is empty?
		Element nobilityTrack = (Element) this.getListOf("nobilityTrack").item(0);

		LinkedHashMap<Integer, LinkedList<Bonus>> generatedNobilityTrack = new LinkedHashMap<Integer, LinkedList<Bonus>>();
		NodeList positions = nobilityTrack.getElementsByTagName("position");

		if (positions.getLength() == 0) {
			throw new NobilityTrackNotConfiguredException("Nobility Track missed in the configuration file");
		} else {

			for (int i = 0; i < positions.getLength(); i++) {

				Element position = (Element) positions.item(i);
				NodeList bonuses = position.getElementsByTagName("*");

				LinkedList<Bonus> toAddBonuses = new LinkedList<Bonus>();

				for (int j = 0; j < bonuses.getLength(); j++) {
					Element bonus = (Element) bonuses.item(j);
					if (Integer.parseInt(bonus.getTextContent()) != 0) {

						Bonus auxBonus = new Bonus();
						String name = bonuses.item(j).getNodeName();
						auxBonus.setType(BonusType.valueOf(name.toUpperCase()));
						auxBonus.setValue(Integer.parseInt(bonuses.item(j).getTextContent()));
						toAddBonuses.add(auxBonus);

					}

				}

				if (!toAddBonuses.isEmpty())
					generatedNobilityTrack.put(i, toAddBonuses);
				else
					// creates an empty list
					generatedNobilityTrack.put(i, new LinkedList<Bonus>());

			}

		}

		return generatedNobilityTrack;
	}

}
