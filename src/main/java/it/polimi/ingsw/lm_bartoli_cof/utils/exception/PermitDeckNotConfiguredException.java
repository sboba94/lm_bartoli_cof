package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class PermitDeckNotConfiguredException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public PermitDeckNotConfiguredException(String message) {
		super(message);
	}

}
