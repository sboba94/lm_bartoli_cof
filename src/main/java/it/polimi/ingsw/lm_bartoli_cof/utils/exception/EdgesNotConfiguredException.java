package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class EdgesNotConfiguredException extends Exception {

	private static final long serialVersionUID = 1L;

	public EdgesNotConfiguredException(String message) {
		super(message);
	}

}
