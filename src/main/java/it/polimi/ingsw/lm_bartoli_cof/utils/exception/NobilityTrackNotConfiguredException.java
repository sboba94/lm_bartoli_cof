package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class NobilityTrackNotConfiguredException extends Exception {
	private static final long serialVersionUID = 1L;

	public NobilityTrackNotConfiguredException(String message) {
		super(message);
	}

}
