package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class CitiesNotConfiguredException extends Exception {

	private static final long serialVersionUID = 1L;

	public CitiesNotConfiguredException(String message) {
		super(message);
	}

}
