package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class KingDeckNotConfiguredException extends Exception {

	private static final long serialVersionUID = 1L;

	public KingDeckNotConfiguredException(String message) {
		super(message);
	}

}
