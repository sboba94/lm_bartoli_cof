package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class PoliticDeckNotConfiguredException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public PoliticDeckNotConfiguredException(String message) {
		super(message);
	}

}
