package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class CouncillorPoolNotConfiguredException extends Exception {

	private static final long serialVersionUID = 1L;

	public CouncillorPoolNotConfiguredException(String message) {
		super(message);
	}

}
