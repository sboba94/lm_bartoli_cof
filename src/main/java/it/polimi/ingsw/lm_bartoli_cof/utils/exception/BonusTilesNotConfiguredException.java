package it.polimi.ingsw.lm_bartoli_cof.utils.exception;

public class BonusTilesNotConfiguredException extends Exception {
	private static final long serialVersionUID = 1L;

	public BonusTilesNotConfiguredException(String message) {
		super(message);
	}

}
