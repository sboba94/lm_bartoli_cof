package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.map.Board;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows elect councillor with an assistant, the player need to
 * choose the color of the new councillor and the council on where elect the
 * council
 */
public class ElectCouncillorWithAssistant extends PlayerAction {

	private GameController game;

	/**
	 * these values were choose by player
	 */
	private Board board;
	private Councillor newCouncillor;

	private String invalidMsg;

	/**
	 * this is the constructor of the action
	 * 
	 * @param player
	 * @param game
	 * @param choosedBoard
	 * @param chooseCouncillor
	 */
	public ElectCouncillorWithAssistant(Player player, GameController game, Board choosedBoard,
			Councillor chooseCouncillor) {
		super(player);
		this.game = game;
		this.board = choosedBoard;
		this.newCouncillor = chooseCouncillor;
	}

	/**
	 * this method ask if it's possible to perform the action
	 */
	public boolean isValid() {
		if (game.getGame().getCurrentPlayer().isQuickActionExecuted() == true) {
			invalidMsg = "-----> Quick action not available!\n\n";
			return false;
		}
		if (super.getPlayer().getAssistantNumber() < 1) {
			invalidMsg = "-----> You don't have enough assistant to perform this action\n-----> Please repeat the action\n\n";
			return false;
		}
		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		if (isValid()) {
			super.getPlayer().setQuickActionExecuted(true);
			game.getGame().getGameMap().getCouncillorPool().remove(newCouncillor);
			game.getGame().getGameMap().getCouncillorPool().add(board.getCouncil().electCouncillor(newCouncillor));
			super.getPlayer().removeAssistants(1);
			String boardName = (board instanceof RegionBoard) ? ((RegionBoard) board).getRegionName().name() : "King";
			String msg = this.getPlayer().getNickname() + " elects " + this.newCouncillor.getColor() + " councillor in "
					+ boardName + " board";
			game.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(msg));
			game.getMessageCouple().setResponse(new CompletedRequestResponseMsg(
					"Councillor elected" + ActionQuestion.askForNextState(getPlayer())));
		} else {
			game.getMessageCouple().setBroadcast(null);
			game.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();

	}

}