package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.AcquirePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithKingRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithPermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuyRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ChangePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DisconnectActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DrawPoliticCardRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorWithAssistantRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.EngageAssistantRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.NothingToSellRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PassRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PerformAddictionallyMainActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.SellRequestMsg;

public interface ActionCreatorFactory {
	
	/**
     * Create the corresponding action depends on requestMsg.
     *
     * @param requestMsg, the request message
     * @return the corresponding action
     */
    PlayerAction create(BuildEmporiumWithPermitTileRequestMsg requestMsg);

	PlayerAction create(BuildEmporiumWithKingRequestMsg requestMsg);

    PlayerAction create(AcquirePermitTileRequestMsg requestMsg);
	
	PlayerAction create(ChangePermitTileRequestMsg requestMsg);
	
	PlayerAction create(DrawPoliticCardRequestMsg requestMsg);
	
	PlayerAction create(ElectCouncillorRequestMsg requestMsg);
	
	PlayerAction create(ElectCouncillorWithAssistantRequestMsg requestMsg);
	
	PlayerAction create(EngageAssistantRequestMsg requestMsg);
	
	PlayerAction create(PerformAddictionallyMainActionRequestMsg requestMsg);
	
	PlayerAction create(PassRequestMsg requestMsg);
	
	PlayerAction create(SellRequestMsg requestMsg);
	
	PlayerAction create(BuyRequestMsg requestMsg);

	PlayerAction create(NothingToSellRequestMsg nothingToSellRequestMsg);

	PlayerAction create(DisconnectActionRequestMsg disconnectActionRequestMsg);
	
}
