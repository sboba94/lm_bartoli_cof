package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.BuyAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.EndTurnAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

public class BuyState extends TurnState {

	public final static BuyState state = new BuyState();
	
	public static TurnState getState() {
		return state;
	}

	
	@Override
	protected boolean isActionValid(PlayerAction action) {
		return (action instanceof BuyAction || action instanceof EndTurnAction);
	}

	

}
