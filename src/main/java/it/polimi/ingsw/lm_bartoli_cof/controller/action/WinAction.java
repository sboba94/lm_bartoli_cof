package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.QuickActionState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;

public class WinAction {

	private GameController game;

	public WinAction(GameController game) {
		this.game = game;
	}

	public TurnState execute() {
		StringBuilder string = new StringBuilder();
		string.append("The game is over\n This is the final ranking of the match:\n");
		LinkedList<Player> finalRanking = new LinkedList<Player>();
		// victory points depends on bonus
		for (Player p : game.getGame().getPlayers()) {
			for (Bonus b : p.getBonus()) {
				game.useBonusByType(b, p);
			}
		}
		// victory points depends on nobility track position
		if (game.getGame().firstOnNobiltyTrack().size() > 1) {
			for (Player p : game.getGame().firstOnNobiltyTrack()) {
				p.addVictoryPos(5);
			}
		} else {
			game.getGame().firstOnNobiltyTrack().getFirst().addVictoryPos(5);
			for (Player p : game.getGame().secondOnNobiltyTrack()) {
				p.addVictoryPos(2);
			}
		}
		// victory points depends on permit cards
		game.getGame().ownerWithTheGreaterNumberOfPermitCards().addVictoryPos(3);
		// to order the ranking position
		finalRanking.addAll(game.getGame().getPlayers());
		for (int i = 0; i < finalRanking.size(); i++) {
			for (int j = 0; j < finalRanking.size() - 1; j++) {
				if (finalRanking.get(j).getVictoryPos() < finalRanking.get(j + 1).getVictoryPos()) {
					Player aux = finalRanking.get(j);
					finalRanking.set(j, finalRanking.get(j + 1));
					finalRanking.set(j + 1, aux);
				}
			}
		}
		for (Player p : finalRanking) {
			string.append("Player " + p.getNickname() + " has " + p.getVictoryPos() + " point on Victory Track\n");
		}
		string.append("\n**************************************\n\nTHE WINNER IS "
				+ finalRanking.get(0).getNickname().toUpperCase());
		game.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(string.toString()));
		return QuickActionState.getState();
	}

}
