package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.BuyState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.SellState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.market.ForSale;

public class SellAction extends PlayerAction {

	private GameController gameController;
	/**
	 * This is the list of object that the player would like to sell
	 */
	private LinkedList<ForSale> toSell;

	/**
	 * This is the constructor of the class.
	 * 
	 * @param player,
	 *            the player that perform the action;
	 * @param gameController,
	 *            to manage the game where player is;
	 * @param toSell,
	 *            this is the list of object to sell.
	 */
	public SellAction(Player player, GameController gameController, LinkedList<ForSale> toSell) {
		super(player);
		this.gameController = gameController;
		this.toSell = toSell;
	}

	public boolean isValid() {
		return (gameController.getTurnMachine().getState() instanceof SellState) ? true : false;
	}

	public TurnState execute() {
		if (isValid()) {
			// this action put the object in the list
			super.getPlayer().setSellExecute(true);
			for (ForSale x : toSell) {
				this.gameController.getGame().getMarket().getSaleList().add(x);
			}
			if (gameController.getGame().everyoneHasExecuteSellAction()) {
				gameController.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg("Ok, everyone has put something to sell. \nNow wait your turn to buy! \n"));
				gameController.getMessageCouple().setResponse(new CompletedRequestResponseMsg(""));
				gameController.getTurnMachine().setState(BuyState.getState());
				gameController.createBuyState();
				gameController.getGame().resetSellExecute();
				return BuyState.getState();
			} else {
				gameController.getMessageCouple().setBroadcast(null);
				String msg = "Request completed, your object are putting on sell. \nNow wait that everyone puts something to sell.";
				gameController.getMessageCouple().setResponse(new CompletedRequestResponseMsg(msg));
			}
		} else {
			gameController.getMessageCouple().setBroadcast(null);
			String msg = "Request not allow, your request will not be perform";
			gameController.getMessageCouple().setResponse(new CompletedRequestResponseMsg(msg));
		}

		return SellState.getState();
	}

}