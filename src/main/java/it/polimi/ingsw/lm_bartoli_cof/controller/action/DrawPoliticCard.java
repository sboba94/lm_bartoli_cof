package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.DrawState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.DisplaySomethingResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * 
 */
public class DrawPoliticCard extends PlayerAction {

	private GameController game;

	public DrawPoliticCard(Player player, GameController game) {
		super(player);
		this.game = game;
	}

	public boolean isValid() {
		return true;
	}

	@Override
	public TurnState execute() {
		PoliticCard drawCard = new PoliticCard();
		super.getPlayer().setQuickActionExecuted(false);
		try {
			drawCard = game.getGame().getPoliticDeck().drawCard();
			super.getPlayer().addPoliticCard(drawCard);
		} catch (NoMorePoliticCardsException e) {
			e.getMessage();
		}
		super.getPlayer().resetNumberOfPrimaryAction();

		game.getMessageCouple().setBroadcast(null);
		game.getMessageCouple().setResponse(new DisplaySomethingResponseMsg("\nHere is your politic card: "
				+ drawCard.toString() + "\n\n" + ActionQuestion.askForNextState(getPlayer())));

		return DrawState.getState();
	}

}