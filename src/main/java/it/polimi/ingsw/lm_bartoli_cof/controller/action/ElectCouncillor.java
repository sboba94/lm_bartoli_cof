package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.map.Board;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows to use elect a councillor, player need to choose the color
 * of the councillor to elect and the board of the council
 */
public class ElectCouncillor extends PlayerAction {

	private GameController game;

	/**
	 * These values were choose by player
	 */
	private Board board;
	private Councillor newCouncillor;

	private String invalidMsg;

	/**
	 * this is the constructor of the action
	 * 
	 * @param player
	 * @param game
	 * @param choosedBoard
	 * @param choosedCouncillor
	 */
	public ElectCouncillor(Player player, GameController game, Board choosedBoard, Councillor choosedCouncillor) {
		super(player);
		this.game = game;
		this.board = choosedBoard;
		this.newCouncillor = choosedCouncillor;
	}

	/**
	 * this method ask if it's possible to perform the action
	 */
	public boolean isValid() {
		if (game.getGame().getCurrentPlayer().getNumberOfPrimaryAction() == 0) {
			invalidMsg = "-----> Main action not available!\n\n";
			return false;
		}
		if (!game.getGame().getGameMap().getCouncillorPool().contains(newCouncillor)) {
			invalidMsg = "-----> No councillor avaiable\n-----> Please repeat the action\n\n";
			return false;
		}
		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		if (isValid()) {
			super.getPlayer().addNumberOfPrimaryAction(-1);
			game.getGame().getGameMap().getCouncillorPool().remove(newCouncillor);
			game.getGame().getGameMap().getCouncillorPool().add(board.getCouncil().electCouncillor(newCouncillor));
			super.getPlayer().addCoins(4);
			String boardName = board instanceof RegionBoard ? ((RegionBoard) board).getRegionName().name() : "King";
			String msg = this.getPlayer().getNickname() + " elects a " + this.newCouncillor.getColor()
					+ " councillor in " + boardName + " board";
			game.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(msg));
			game.getMessageCouple().setResponse(new CompletedRequestResponseMsg(
					"completed, councillor elected" + ActionQuestion.askForNextState(getPlayer())));
		} else {
			game.getMessageCouple().setBroadcast(null);
			game.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();
	}

}