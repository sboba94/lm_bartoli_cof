package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.BuyState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.DrawState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.SellState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PassRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;

public class NoSellAction extends PlayerAction {

	private GameController gameController;

	public NoSellAction(Player player, GameController game) {
		super(player);
		this.gameController = game;
	}

	@Override
	public boolean isValid() {
		if(gameController.getTurnMachine().getState().equals(SellState.getState()) && !getPlayer().isSellExecute())
			return true;
		return false;
	}

	@Override
	public TurnState execute() {
		if(isValid()){
			this.getPlayer().setSellExecute(true);
			if (gameController.getGame().everyoneHasExecuteSellAction()) {
				gameController.getPublisherInterface().publish(new DisplaySomethingBroadcastMsg("Ok, everyone has put something to sell. \nNow wait your turn to buy! \n"),gameController.makeTopic());
				if(gameController.getGame().getMarket().getSaleList().isEmpty()){
					gameController.getGame().resetSellExecute();
					gameController.getTurnMachine().setState(SellState.getState());
					gameController.getPublisherInterface().publish(new DisplaySomethingBroadcastMsg("No object on sell, the buy state it's been jumped"), gameController.makeTopic());
					gameController.handleRequest(new PassRequestMsg(gameController.getTokenByPlayer(gameController.getGame().getCurrentPlayer())));
					return DrawState.getState();
				}
				gameController.getTurnMachine().setState(BuyState.getState());
				gameController.createBuyState();
				gameController.getGame().resetSellExecute();
				gameController.getMessageCouple().setBroadcast(null);
				gameController.getMessageCouple().setResponse(new CompletedRequestResponseMsg(""));
				return BuyState.getState(); 
			} else {
				gameController.getMessageCouple().setBroadcast(null);
				String msg = "Request completed, your object are putting on sell. \nNow wait that everyone puts something to sell.";
				gameController.getMessageCouple().setResponse(new CompletedRequestResponseMsg(msg));
			}
		} else {
			gameController.getMessageCouple().setBroadcast(null);
			String msg = "Request not allow, your request will not be perform";
			gameController.getMessageCouple().setResponse(new CompletedRequestResponseMsg(msg));
		}

		return SellState.getState();

	}

}
