package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BuildEmporiumBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.BuildEmporiumResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Game;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncilBalcony;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.CityColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.GameMap;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows to use build an emporium with the help of king from one
 * region board, player need to choose the politic cards that want to use
 */
public class BuildEmporiumWithKing extends PlayerAction {
	private int totalCoinsToPay;
	private GameController gameController;
	private Game game;
	private GameMap gameMap;
	private CouncilBalcony council;

	/**
	 * These information are chosen by the player
	 */
	private City buildEmporiumIn;
	private LinkedList<PoliticCard> politicCards;

	private String invalidMsg;

	/**
	 * this is the constructor of the action
	 * 
	 * @param player
	 * @param gameController
	 * @param politicCard
	 * @param chooseCity
	 */
	public BuildEmporiumWithKing(Player player, GameController gameController, LinkedList<PoliticCard> politicCard,
			City chooseCity) {
		super(player);
		this.gameController = gameController;
		this.game = gameController.getGame();
		this.gameMap = gameController.getGame().getGameMap();
		this.council = game.getGameMap().getKingBoard().getCouncil();
		this.politicCards = politicCard;
		this.buildEmporiumIn = chooseCity;
	}

	/**
	 * this method ask if it's possible to perform the action
	 */
	public boolean isValid() {
		if (gameController.getGame().getCurrentPlayer().getNumberOfPrimaryAction() == 0) {
			invalidMsg = "-----> Main action not available!\n\n";
			return false;
		}
		this.council = gameMap.getKingBoard().getCouncil();
		boolean cardsOk = true;
		LinkedList<CouncillorColors> councillorColorsList = new LinkedList<CouncillorColors>();

		totalCoinsToPay = (this.politicCards.size() == 4) ? 0 : 10 - 3 * (this.politicCards.size() - 1);

		for (PoliticCard card : politicCards)
			councillorColorsList.add(card.getPoliticCardColor());

		cardsOk = council.containColors(councillorColorsList);

		if (!cardsOk) {
			invalidMsg = "-----> None of the chosen cards can satisfy this council balcony\n-----> Please repeat the action\n\n";
			return false;
		}

		totalCoinsToPay = (this.politicCards.size() == 4) ? 0 : 10 - 3 * (this.politicCards.size() - 1);
		for (PoliticCard card : politicCards)
			if (card.getPoliticCardColor().compareTo(CouncillorColors.MULTICOLOR) == 0)
				totalCoinsToPay += 1;

		int jumps = gameMap.getCities().findMinimumJumps(gameMap.getKing().getCity(), buildEmporiumIn);
		totalCoinsToPay += (jumps * 2);
		if (totalCoinsToPay > super.getPlayer().getCoinsNumber()) {
			invalidMsg = "-----> You don't have enough coins to perform this action\n-----> Please repeat the action\n\n";
			return false;
		}

		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		if (isValid()) {
			super.getPlayer().addNumberOfPrimaryAction(-1);
			super.getPlayer().removeCoins(this.totalCoinsToPay);
			super.getPlayer().getPoliticCards().removeAll(politicCards);
			// MainActionQuestion.chooseCity(cities) is waiting for a LinkedList
			LinkedList<City> cities = new LinkedList<City>();
			cities.addAll(gameMap.getCities().vertexSet());
			// remove cities where player has built an emporium yet;
			for (int i = 0; i < cities.size(); i++) {
				if (super.getPlayer().getEmporiums().contains(cities.get(i))) {
					cities.remove(i);
				}
			}
			LinkedList<Bonus> totalBonuses = new LinkedList<Bonus>();
			super.getPlayer().buildEmporium(buildEmporiumIn);
			LinkedList<City> myNeighbourhood = gameMap.getCities()
					.findNeighboursWithPlayerEmporiums(this.getPlayer().getEmporiums(), buildEmporiumIn);
			totalBonuses.addAll(buildEmporiumIn.getToken().getBonuses());
			for (City c : myNeighbourhood) {
				totalBonuses.addAll(c.getToken().getBonuses());
			}
			for (Bonus b : totalBonuses) {
				gameController.useBonusByType(b, super.getPlayer());
			}
			gameMap.getKing().setCity(buildEmporiumIn);
			// controllo se ha tutte le città di quel colore
			if (this.getPlayer().hasAllCitiesByColor(gameMap.getCities(), buildEmporiumIn.getColor())
					&& buildEmporiumIn.getColor().compareTo(CityColors.PURPLE) != 0) {
				this.getPlayer().addBonus(gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()).getBonus());
				gameController.useBonusByType(gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()).getBonus(),
						super.getPlayer());
				totalBonuses.add(gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()).getBonus());
				this.gameMap.getBonusTiles().remove(gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()));
			}
			// Check if it has all city in a region depends on RegionBoard
			RegionBoard realRegion = gameMap.getRealRegion(buildEmporiumIn.getRegionName());
			if (this.getPlayer().hasAllCitiesByRegion(realRegion)) {
				this.getPlayer().addBonus(gameMap.getBonusTileByPosition(realRegion.getRegionName()).getBonus());
				gameController.useBonusByType(gameMap.getBonusTileByPosition(realRegion.getRegionName()).getBonus(),
						super.getPlayer());
				totalBonuses.add(gameMap.getBonusTileByPosition(realRegion.getRegionName()).getBonus());
				this.gameMap.getBonusTiles().remove(gameMap.getBonusTileByPosition(realRegion.getRegionName()));
			}
			// COUPLE OF MESSAGE
			gameController.getMessageCouple().setBroadcast(new BuildEmporiumBroadcastMsg(this.getPlayer(),
					this.totalCoinsToPay, buildEmporiumIn, buildEmporiumIn));
			gameController.getMessageCouple().setResponse(
					new BuildEmporiumResponseMsg(buildEmporiumIn, this.totalCoinsToPay, 0, totalBonuses, getPlayer()));
		} else {
			gameController.getMessageCouple().setBroadcast(null);
			gameController.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();
	}
}