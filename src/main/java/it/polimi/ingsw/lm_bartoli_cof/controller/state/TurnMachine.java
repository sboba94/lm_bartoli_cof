package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

/**
 * This class manage game. It allows to change State depends on the Action
 * executed
 */
public class TurnMachine {
	// When game starts the TurnMachine work only on that game
	private final GameController g;

	// This variable allows to understand in which state the game is working on.
	private TurnState state;

	/**
	 * the constructor instance the game in turnMachine and start to working
	 * on the first state
	 * 
	 * @param game controller
	 */
	public TurnMachine(GameController g) {
		this.g = g;
		state= new StartConfigState();
	}
	
	public void executeAction(PlayerAction action){
		
		if(state.isActionValid(action)){
			
			
			//it return the next state of the turn machine!
			TurnState nextState = state.executeAction(action);
			
			//the FSA go forward
			state = nextState;
			
		}
		else
			System.out.println("Stato non valido per questa azione");
	}
	
	
	/**
	 * @return the g
	 */
	public GameController getG() {
		return g;
	}

	/**
	 * @return the state
	 */
	public TurnState getState() {
		return state;
	}

	/**
	 * @param state, the state to set
	 */
	public void setState(TurnState state) {
		this.state = state;
	}
}