package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows to engage an assistant with the use of a number of coins
 * depends on assistant_price
 */
public class EngageAssistant extends PlayerAction {
	private GameController game;
	private String invalidMsg;
	public static int ASSISTANT_PRICE = 3;

	/**
	 * this is the constructor of the action
	 * 
	 * @param player
	 * @param game
	 */
	public EngageAssistant(Player player, GameController game) {
		super(player);
		this.game = game;
	}

	/**
	 * this method ask if it's possible to perform the action
	 */
	public boolean isValid() {
		if (game.getGame().getCurrentPlayer().isQuickActionExecuted() == true) {
			invalidMsg = "-----> Quick action not available!\n\n";
			return false;
		}
		if (this.getPlayer().getCoinsNumber() < ASSISTANT_PRICE) {
			invalidMsg = "-----> You don't have enough coins to perform this action\n-----> Please repeat the action\n\n";
			return false;
		}
		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		if (isValid()) {
			super.getPlayer().removeCoins(ASSISTANT_PRICE);
			super.getPlayer().addAssistant();
			super.getPlayer().setQuickActionExecuted(true);
			String msg = "Player " + this.getPlayer().getNickname() + " engage a new assistant";
			game.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(msg));
			game.getMessageCouple().setResponse(new CompletedRequestResponseMsg("Action Completed!\n"+ ActionQuestion.askForNextState(getPlayer())));
		} else {
			game.getMessageCouple().setBroadcast(null);
			game.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();
	}

}