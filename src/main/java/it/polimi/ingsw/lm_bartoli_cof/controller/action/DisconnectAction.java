package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisconnectBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PassRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.DisconnectResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;

public class DisconnectAction extends PlayerAction {

	private GameController gameController;

	public DisconnectAction(Player player, GameController gameController) {
		super(player);
		this.gameController = gameController;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public TurnState execute() {
		super.getPlayer().disconnect();
		super.getPlayer().setNumberOfPrimaryAction(0);
		
		this.gameController.getTurnMachine().setState(new FinalControlState());
		gameController.handleRequest(new PassRequestMsg(gameController.getTokenByPlayer(super.getPlayer())));

		gameController.getMessageCouple().setResponse(new DisconnectResponseMsg(super.getPlayer()));
		gameController.getMessageCouple().setBroadcast(new DisconnectBroadcastMsg(super.getPlayer()));
		
		return FinalControlState.getState();
	}

}
