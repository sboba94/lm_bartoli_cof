package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.security.InvalidParameterException;
import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.AcquirePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithKingRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithPermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuyRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ChangePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DisconnectActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DrawPoliticCardRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ElectCouncillorWithAssistantRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.EngageAssistantRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.NothingToSellRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PassRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PerformAddictionallyMainActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.SellRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.Board;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.model.market.ForSale;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;

public class ActionCreatorFactoryImpl implements ActionCreatorFactory {

	private GameController game;

	/**
	 * Constructor of the class
	 * 
	 * @param game,
	 *            GameController
	 * @param players,
	 *            list of token
	 * @param turnM,
	 *            the turn machine
	 */
	public ActionCreatorFactoryImpl(GameController game) {
		this.game = game;
	}

	/**
	 * This method extract the token from request message; with the token i can
	 * obtain the player
	 * 
	 * @param requestMsg,
	 *            the message of request
	 * @return the player of request message
	 */
	public Player getPlayerByMessage(RequestMsg requestMsg) {
		for (Player p : this.game.getGame().getPlayers())
			if (p.getId() == requestMsg.getToken().getPlayerNumber())
				return p;
		return null;
	}

	/**
	 * This method is used to set up and create BuildEmporiumWithPermitTile
	 * action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(BuildEmporiumWithPermitTileRequestMsg requestMsg) {
		BusinessPermitTile tile = this.convertPermitTile(requestMsg);
		City city = this.convertCity(requestMsg);
		return new BuildEmporiumWithPermitTile(this.getPlayerByMessage(requestMsg), game, tile, city);
	}

	/**
	 * This method is used to set up and create BuildEmporiumWithKingRequestMsg
	 * action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(BuildEmporiumWithKingRequestMsg requestMsg) {
		String[] parts = requestMsg.getPoliticCards().split(",");
		LinkedList<PoliticCard> cardsList = game.getGame().getCurrentPlayer().getPoliticCardsByColors(parts);
		City city = this.convertCity(requestMsg);
		return new BuildEmporiumWithKing(this.getPlayerByMessage(requestMsg), game, cardsList, city);
	}

	/**
	 * This method is used to set up and create AcquirePermitTileRequestMsg
	 * action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(AcquirePermitTileRequestMsg requestMsg) {
		RegionBoard region = (RegionBoard) this.convertRegionBoard(requestMsg);
		BusinessPermitTile tile = this.convertPermitTile(requestMsg);
		String[] parts = requestMsg.getPoliticCards().split(",");
		LinkedList<PoliticCard> cardsList = game.getGame().getCurrentPlayer().getPoliticCardsByColors(parts);
		return new AcquirePermitTile(this.getPlayerByMessage(requestMsg), cardsList, game, region, tile);
	}

	/**
	 * This method is used to set up and create ChangePermitTileRequestMsg
	 * action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(ChangePermitTileRequestMsg requestMsg) {
		RegionBoard region = (RegionBoard) this.convertRegionBoard(requestMsg);
		return new ChangePermitTile(this.getPlayerByMessage(requestMsg), game, region);
	}

	/**
	 * This method is used to set up and create DrawPoliticCardRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(DrawPoliticCardRequestMsg requestMsg) {
		return new DrawPoliticCard(this.getPlayerByMessage(requestMsg), game);
	}

	/**
	 * This method is used to set up and create ElectCouncillorRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(ElectCouncillorRequestMsg requestMsg) {
		Board board = this.convertRegionBoard(requestMsg);
		Councillor councillor = game.getGame().getGameMap().getCouncillorByColor(this.convertColor(requestMsg));
		return new ElectCouncillor(this.getPlayerByMessage(requestMsg), game, board, councillor);
	}

	/**
	 * This method is used to set up and create
	 * ElectCouncillorWithAssistantRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(ElectCouncillorWithAssistantRequestMsg requestMsg) {
		Board board = this.convertRegionBoard(requestMsg);
		Councillor councillor = game.getGame().getGameMap().getCouncillorByColor(this.convertColor(requestMsg));
		return new ElectCouncillorWithAssistant(this.getPlayerByMessage(requestMsg), game, board, councillor);
	}

	/**
	 * This method is used to set up and create EngageAssistantRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(EngageAssistantRequestMsg requestMsg) {
		return new EngageAssistant(this.getPlayerByMessage(requestMsg), this.game);
	}

	/**
	 * This method is used to set up and create
	 * PerformAddictionallyMainActionRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(PerformAddictionallyMainActionRequestMsg requestMsg) {
		return new PerformAddictionallyMainAction(this.getPlayerByMessage(requestMsg), this.game);
	}

	/**
	 * This method is used to set up and create PassRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(PassRequestMsg requestMsg) {
		return new EndTurnAction(this.getPlayerByMessage(requestMsg), this.game);
	}

	/**
	 * This method is used to set up and create SellRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(SellRequestMsg requestMsg) {
		LinkedList<ForSale> toSell = this.convertObjectToSell(requestMsg);
		return new SellAction(this.getPlayerByMessage(requestMsg), this.game, toSell);
	}

	/**
	 * This method is used to set up and create BuyRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(BuyRequestMsg requestMsg) {
		LinkedList<ForSale> toBuy = this.convertObjectToBuy(requestMsg);
		return new BuyAction(this.getPlayerByMessage(requestMsg), this.game, toBuy);
	}

	/**
	 * This method is used to set up and create NothingToSellRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(NothingToSellRequestMsg requestMsg) {
		return new NoSellAction(this.getPlayerByMessage(requestMsg), this.game);
	}

	/**
	 * This method is used to set up and create NothingToSellRequestMsg action
	 * 
	 * @param requestMsg,
	 *            it is used to specify what action to create
	 * @return the action created
	 */
	@Override
	public PlayerAction create(DisconnectActionRequestMsg requestMsg) {
		return new DisconnectAction(this.getPlayerByMessage(requestMsg), this.game);
	}

	////////////////////////// Choose methods /////////////////////////

	private LinkedList<ForSale> convertObjectToBuy(BuyRequestMsg requestMsg) {
		LinkedList<ForSale> toBuy = new LinkedList<ForSale>();
		for (String s : requestMsg.getPermitBuyList()) {
			ForSale tempObject = game.getGame().getMarket().getSaleList().get(Integer.parseInt(s));
			toBuy.add(tempObject);
		}
		for (String s : requestMsg.getPoliticBuyList()) {
			
			ForSale tempObject = game.getGame().getMarket().getSaleList().get(Integer.parseInt(s));
			toBuy.add(tempObject);
		}
		for (String s : requestMsg.getAssistantBuyList()) {
			
			ForSale tempObject = game.getGame().getMarket().getSaleList().get(Integer.parseInt(s));
			toBuy.add(tempObject);
		}
		return toBuy;
	}

	private LinkedList<ForSale> convertObjectToSell(SellRequestMsg requestMsg) {
		LinkedList<ForSale> toSell = new LinkedList<ForSale>();
		Player tempPlayer = this.getPlayerByMessage(requestMsg);
		for (String s : requestMsg.getPermitSellList()) {
			String[] parts = s.split(",");
			BusinessPermitTile tempObject = tempPlayer.getUsablePermitTiles().get(Integer.parseInt(parts[0])-1);
			int tempPrice = Integer.parseInt(parts[1]);
			ForSale temp = new ForSale(tempPlayer, tempObject, tempPrice);
			toSell.add(temp);
		}
		for (String s : requestMsg.getPoliticSellList()) {
			String[] parts = s.split(",");
			PoliticCard tempObject = tempPlayer.getPoliticCards().get(Integer.parseInt(parts[0])-1);
			int tempPrice = Integer.parseInt(parts[1]);
			ForSale temp = new ForSale(tempPlayer, tempObject, tempPrice);
			toSell.add(temp);
		}
		for (String s : requestMsg.getAssistantSellList()) {
			int tempPrice = Integer.parseInt(s);
			ForSale temp = new ForSale(tempPlayer, BonusType.ASSISTANTS, tempPrice);
			toSell.add(temp);
		}
		return toSell;
	}

	/**
	 * this method allows to choose the region where perform the action
	 * 
	 * @param requestMsg
	 * @return the Region Board that was choose
	 */
	private Board convertRegionBoard(AcquirePermitTileRequestMsg requestMsg) {
		Board region = null;
		switch (requestMsg.getPermitBoard()) {
		case "Coast":
			region = game.getGame().getGameMap().getCoast();
			break;
		case "Hills":
			region = game.getGame().getGameMap().getHill();
			break;
		case "Mountain":
			region = game.getGame().getGameMap().getMountain();
			break;
		default:
			throw new InvalidParameterException("Please enter a valid choise");

		}
		return region;
	}

	/**
	 * this method allows to choose the region where perform the action
	 * 
	 * @param requestMsg
	 * @return the Region Board that was choose
	 */
	private Board convertRegionBoard(ElectCouncillorRequestMsg requestMsg) {
		Board region = null;
		switch (requestMsg.getBoard()) {
		case "Coast":
			region = game.getGame().getGameMap().getCoast();
			break;
		case "Hills":
			region = game.getGame().getGameMap().getHill();
			break;
		case "Mountain":
			region = game.getGame().getGameMap().getMountain();
			break;
		case "King":
			region = game.getGame().getGameMap().getKingBoard();
			break;
		default:
			throw new InvalidParameterException("Please enter a valid choise");

		}
		return region;
	}

	/**
	 * this method allows to choose the region where perform the action
	 * 
	 * @param requestMsg
	 * @return the Region Board that was choose
	 */
	private Board convertRegionBoard(ElectCouncillorWithAssistantRequestMsg requestMsg) {
		Board region = null;
		switch (requestMsg.getBoard()) {
		case "Coast":
			region = game.getGame().getGameMap().getCoast();
			break;
		case "Hills":
			region = game.getGame().getGameMap().getHill();
			break;
		case "Mountain":
			region = game.getGame().getGameMap().getMountain();
			break;
		case "King":
			region = game.getGame().getGameMap().getKingBoard();
			break;
		default:
			throw new InvalidParameterException("Please enter a valid choise");

		}
		return region;
	}

	/**
	 * These methods allows to choose a permit tile
	 * 
	 * @param requestMsg
	 * @return the permit tile that was choose
	 */
	private BusinessPermitTile convertPermitTile(AcquirePermitTileRequestMsg requestMsg) {
		BusinessPermitTile tile = null;
		switch (requestMsg.getPermitBoard()) {
		case "Coast":
			if (requestMsg.getPermitTile().equals("1"))
				tile = game.getGame().getGameMap().getCoast().getPermitBoard().getUsablePermitTile1();
			else
				tile = game.getGame().getGameMap().getCoast().getPermitBoard().getUsablePermitTile2();
			break;
		case "Hills":
			if (requestMsg.getPermitTile().equals("1"))
				tile = game.getGame().getGameMap().getHill().getPermitBoard().getUsablePermitTile1();
			else
				tile = game.getGame().getGameMap().getHill().getPermitBoard().getUsablePermitTile2();
			break;
		case "Mountain":
			if (requestMsg.getPermitTile().equals("1"))
				tile = game.getGame().getGameMap().getMountain().getPermitBoard().getUsablePermitTile1();
			else
				tile = game.getGame().getGameMap().getMountain().getPermitBoard().getUsablePermitTile2();
			break;
		default:
			throw new InvalidParameterException("Please enter a valid choise");
		}
		return tile;
	}

	private BusinessPermitTile convertPermitTile(BuildEmporiumWithPermitTileRequestMsg requestMsg) {
		Player player = this.getPlayerByMessage(requestMsg);
		return player.getUsablePermitTiles().get(Integer.parseInt(requestMsg.getTile()) - 1);
	}

	/**
	 * These methods allows to choose the city
	 * 
	 * @param requestMsg
	 * @return the city that was choose
	 * @throws InvalidParameterException
	 *             if request not valid
	 */
	private City convertCity(BuildEmporiumWithKingRequestMsg requestMsg) {
		for (City c : game.getGame().getGameMap().getCities().vertexSet()) {
			if (c.getName().toUpperCase().equals(requestMsg.getCity().toUpperCase()))
				return c;
		}
		throw new InvalidParameterException("Not valid Request Message");
	}

	private City convertCity(BuildEmporiumWithPermitTileRequestMsg requestMsg) {
		for (City c : game.getGame().getGameMap().getCities().vertexSet()) {
			if (c.getName().toUpperCase().equals(requestMsg.getCity().toUpperCase()))
				return c;
		}
		throw new InvalidParameterException("Not valid Request Message");
	}

	private CouncillorColors convertColor(ElectCouncillorRequestMsg request) {
		for (CouncillorColors c : CouncillorColors.values()) {
			if (c.toString().equals(request.getColor()))
				return c;
		}
		throw new InvalidParameterException("Not valid Request Message");
	}

	private CouncillorColors convertColor(ElectCouncillorWithAssistantRequestMsg request) {
		for (CouncillorColors c : CouncillorColors.values())
			if (c.toString().equals(request.getColor()))
				return c;
		throw new InvalidParameterException("Not valid Request Message");
	}

	/**
	 * this method allows to choose the region where perform the action
	 * 
	 * @param requestMsg
	 * @return the Region Board that was choose
	 */
	private Board convertRegionBoard(ChangePermitTileRequestMsg requestMsg) {
		Board region = null;
		switch (requestMsg.getPermitBoard()) {
		case "Coast":
			region = game.getGame().getGameMap().getCoast();
			break;
		case "Hills":
			region = game.getGame().getGameMap().getHill();
			break;
		case "Mountain":
			region = game.getGame().getGameMap().getMountain();
			break;
		}
		return region;
	}

}
