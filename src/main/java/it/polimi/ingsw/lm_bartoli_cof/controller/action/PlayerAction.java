package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;

/**
 * 
 */
public abstract class PlayerAction {

	private Player player;

	public PlayerAction(Player player) {
		super();
		this.player = player;
	}

	public abstract boolean isValid();

	public abstract TurnState execute();

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return this.player;
	}
}