package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.AcquirePermitTileBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.AcquirePermitTileResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncilBalcony;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows to use acquire a permit tile from one region board, player
 * need to choose the politic cards that want to use
 */
public class AcquirePermitTile extends PlayerAction {

	/**
	 * cards that a player choose to use to acquire a permit tile from a region
	 * board
	 */
	private LinkedList<PoliticCard> politicCard;
	private RegionBoard choosenBoard;
	private BusinessPermitTile tileToUse;

	private int totalCoinsToPay;
	private String invalidMsg;
	private CouncilBalcony council;
	private GameController game;

	/**
	 * Constructor of acquire permit tile action
	 * 
	 * @param player
	 * @param politicCard
	 * @param game
	 * @param choosenBoard
	 * @param choosenTile
	 */
	public AcquirePermitTile(Player player, LinkedList<PoliticCard> politicCard, GameController game,
			RegionBoard choosenBoard, BusinessPermitTile choosenTile) {
		super(player);
		this.politicCard = politicCard;
		this.choosenBoard = choosenBoard;
		this.tileToUse = choosenTile;
		this.game = game;
	}

	/**
	 * this method check if it's possible to perform the action
	 */
	public boolean isValid() {
		if (game.getGame().getCurrentPlayer().getNumberOfPrimaryAction() == 0) {
			invalidMsg = "-----> Main action not available!\n\n";
			return false;
		}

		if (politicCard.isEmpty()) {
			invalidMsg = "-----> Wrong chosen cards\n-----> Please repeat the action\n\n";
			return false;
		}

		this.council = choosenBoard.getCouncil();
		totalCoinsToPay = 0;
		boolean cardsOk = false;
		LinkedList<CouncillorColors> councillorColorsList = new LinkedList<CouncillorColors>();

		for (PoliticCard card : politicCard)
			councillorColorsList.add(card.getPoliticCardColor());

		cardsOk = council.containColors(councillorColorsList);

		if (!cardsOk) {
			invalidMsg = "-----> The chosen cards can't satisfy this council balcony\n-----> Please repeat the action\n\n";
			return false;
		}

		totalCoinsToPay = (this.politicCard.size() == 4) ? 0 : 10 - 3 * (this.politicCard.size() - 1);
		for (PoliticCard card : politicCard)
			if (card.getPoliticCardColor().compareTo(CouncillorColors.MULTICOLOR) == 0)
				totalCoinsToPay += 1;

		if (totalCoinsToPay > super.getPlayer().getCoinsNumber()) {
			invalidMsg = "-----> You don't have enough coins to perform this action\n-----> Please repeat the action\n\n";
			return false;
		}

		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		if (isValid()) {
			super.getPlayer().addNumberOfPrimaryAction(-1);
			super.getPlayer().removeCoins(this.totalCoinsToPay);
			super.getPlayer().addPermitTile(tileToUse);
			super.getPlayer().getPoliticCards().removeAll(politicCard);
			LinkedList<Bonus> bonusToAssign = tileToUse.getBonuses();
			for (Bonus b : bonusToAssign) {
				game.useBonusByType(b, super.getPlayer());
			}
			game.getMessageCouple().setBroadcast(
					new AcquirePermitTileBroadcastMsg(this.getPlayer(), this.totalCoinsToPay, tileToUse, choosenBoard));
			game.getMessageCouple().setResponse(
					new AcquirePermitTileResponseMsg(this.getPlayer(), this.totalCoinsToPay, bonusToAssign));

		} else {
			game.getMessageCouple().setBroadcast(null);
			game.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();
	}

}