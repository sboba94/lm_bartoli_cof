package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.EndTurnAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.NoSellAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.SellAction;

/**
 * 
 */
public class SellState extends TurnState {
	
	public final static SellState state = new SellState();
	
	public static TurnState getState() {
		return state;
	}

	
	@Override
	protected boolean isActionValid(PlayerAction action) {
		return (action instanceof SellAction || action instanceof NoSellAction || action instanceof EndTurnAction);
	}

	

    



}