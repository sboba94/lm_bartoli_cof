package it.polimi.ingsw.lm_bartoli_cof.controller;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.StartConfigAction;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.ConnectionRequest;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.CreateGameRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.DisconnectRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.GameNeedSomethingRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.InitRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.JoinGameRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.SetMyPlayerRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.ViewGameBoardRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.ViewPlayersInfoRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DrawPoliticCardRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ConnectionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.GameResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ListGameResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.view.common.RequestHandler;
import it.polimi.ingsw.lm_bartoli_cof.view.server.PublisherInterface;

//FINIRE METODI IMPORTATI DA REQUEST HANDLER !!!!!

public class GamesController implements RequestHandler {

	public static LinkedList<GameController> waitingRooms; // giochiDafarPartire

	private final int COUNTDOWN = 20000;
	private static LinkedList<GameController> gameRooms; // giochiPartiti
	protected LinkedList<Token> waitingPlayers;
	private Map<Token, GameController> clients; // players in a waitingRoom or
												// players in a game room
	private PublisherInterface publishInterface;
	private Timer timer;
	private TimerTask timerTask;
	public static int numberOfGames;

	public GamesController(PublisherInterface publish) {
		waitingPlayers = new LinkedList<Token>();
		gameRooms = new LinkedList<GameController>();
		clients = new HashMap<>();
		waitingRooms = new LinkedList<GameController>();
		numberOfGames = GamesController.gameRooms.size();
		this.publishInterface = publish;
	}

	/**
	 * this function add a new player to the waiting players list
	 * 
	 * @param player:
	 *            to add
	 */
	public void addWaitingPlayer(Token player) {
		waitingPlayers.add(player);
	}

	/**
	 * This function allows a new player to create a new waiting room
	 * 
	 * @param player
	 */
	public void newWaitingRoom(GameController game) {
		GamesController.waitingRooms.add(game);
	}

	/**
	 * Turn a waiting room into a game room
	 * 
	 * @param game
	 */
	public void newGameRoom(GameController game) {
		if (GamesController.waitingRooms.contains(game)) {
			GamesController.gameRooms.add(game);
			GamesController.waitingRooms.remove(game);
		}
		GamesController.numberOfGames = GamesController.gameRooms.size();
	}

	/**
	 * This method add a Player to a waiting room than checks if this room has
	 * almost 2 players and start game.
	 */
	public void newPlayerToWaitingRoom(Token token, GameController game) {
		clients.put(token, game);
		game.addPlayer(token);

		if (game.getTokenList().size() == 2) {

			timerTask = new TimerTask() {
				@Override
				public void run() {
					try {
						game.initGame();

					} catch (Exception e) {

					}
				}
			};

			timer = new Timer();
			game.getPublisherInterface().publish(
					new DisplaySomethingBroadcastMsg(
							"New player join the game! \nThe game will start in a few seconds... \n"),
					game.makeTopic());
			timer.schedule(timerTask, (long) COUNTDOWN);

		} else if(game.getTokenList().size() > 2) {
			game.getPublisherInterface().publish(
					new DisplaySomethingBroadcastMsg(
							"New player join the game! \nThe game will start in a few seconds... \n"),
					game.makeTopic());
			timerTask.cancel();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					try {
						game.initGame();

					} catch (Exception e) {

					}
				}
			};
			timer.schedule(timerTask, COUNTDOWN);
		}

	}

	/**
	 * This method returns all waiting rooms in a string format.
	 * 
	 * @return
	 */
	public String getListStringOfWaitingRooms() {
		if (GamesController.waitingRooms.isEmpty())
			return null;
		StringBuilder string = new StringBuilder();
		string.append("Here's the list of the joinable game rooms: \n\n");
		for (GameController g : GamesController.waitingRooms) {
			string.append("- Game room number " + g.getGameID() + "\n");
		}
		string.append(
				"\n + Please enter 'game' followed by the number of the game room that you want to join [example: game 10]\n");
		string.append(" + Please enter 'new game' if you want to create a new game room");
		return string.toString();
	}

	@Override
	public Token connect() throws RemoteException {

		// UUID inmutable universally unique identifier. Una volta istanziato
		// questo valore è unico ed immodificabile.
		// stiamo assegnando al giocatore un token con questo identificatore
		// unico
		Token token = new Token(UUID.randomUUID().toString(), this.waitingPlayers.size());
		addWaitingPlayer(token);
		return token;
	}

	@Override
	public ResponseMsg processRequest(RequestMsg request) throws RemoteException {

		if (request instanceof ConnectionRequest) {

			ConnectionRequest connectionRequest = (ConnectionRequest) request;
			if (connectionRequest.getToken() == null) {
				return new ConnectionResponseMsg(connect());
			} else {
				return new InvalidRequestResponseMsg("OPS! Reconnection is not supported yet!");
			}

		} else if (request instanceof InitRequestMsg) {
			return new ListGameResponseMsg(this.getListStringOfWaitingRooms());

		} else if (request instanceof CreateGameRequestMsg) {
			GameController newGame = new GameController(this.publishInterface,
					((CreateGameRequestMsg) request).getMapRoot());
			GamesController.numberOfGames++;
			this.newPlayerToWaitingRoom(request.getToken(), newGame);
			this.newWaitingRoom(newGame);
			this.clients.put(request.getToken(), newGame);

			return new CompletedRequestResponseMsg(
					"\n******************************************************************************************\nNew game room created with ID: "
							+ newGame.getGameID() + "\nWaiting for new player to join this game...");

		} else if (request instanceof ViewGameBoardRequestMsg) {
			return new CompletedRequestResponseMsg(clients.get(request.getToken()).printGameBoard(request.getToken()));

		} else if (request instanceof ViewPlayersInfoRequestMsg) {
			return new CompletedRequestResponseMsg(
					clients.get(request.getToken()).printPlayersInfo(request.getToken()));

		} else if (request instanceof JoinGameRequestMsg) {

			GameController toJoin = null;
			for (GameController g : GamesController.waitingRooms)
				if (g.getGameID() == ((JoinGameRequestMsg) request).getGameId())
					toJoin = g;

			this.newPlayerToWaitingRoom(request.getToken(), toJoin);
			return new CompletedRequestResponseMsg("\nJoined the game with ID: " + toJoin.getGameID());

		} else if (request instanceof GameNeedSomethingRequestMsg) {
			return new GameResponseMsg(this.clients.get(request.getToken()).gameRequestHandler(
					((GameNeedSomethingRequestMsg) request).getRealRequest(),
					((GameNeedSomethingRequestMsg) request).getQuestion()));

		} else if (request instanceof SetMyPlayerRequestMsg) {
			// for each token there is a player in game in the same index!
			// i'm setting the nickname of the player
			clients.get(request.getToken()).getGame().getPlayers()
					.get(clients.get(request.getToken()).getTokenList().indexOf(request.getToken()))
					.setNickname(((SetMyPlayerRequestMsg) request).getNickname());

			if (clients.get(request.getToken()).getGame().getCurrentPlayer() != null)

				// se sei il primo giocatore
				if (request.getToken().getPlayerNumber() == clients.get(request.getToken()).getGame().getCurrentPlayer()
						.getId()) {
				clients.get(request.getToken()).getTurnMachine().executeAction(new StartConfigAction(clients.get(request.getToken()).getGame()));
				return processRequest(new DrawPoliticCardRequestMsg(request.getToken()));
				} else
				return new CompletedRequestResponseMsg("\n*******************************************************************************************\n" + "Game session started, now wait for your turn...\nType 'game info' if you want to display informations about the map configuration\nType 'players info' if you want to display informations about other players\n");

			else
				return new CompletedRequestResponseMsg("Current player not set!");
		
		} else if (request instanceof DisconnectRequestMsg) {
			GameController game = clients.get(request.getToken());
			return game.handleRequest(request);			
		}
		

		else {

			Token requestToken = request.getToken();

			if (!clients.containsKey(requestToken)) {
				return new InvalidRequestResponseMsg("OPS! Invalid Token!");
			} else {

				GameController game = clients.get(requestToken);
				return game.handleRequest(request);

			}

		}
	}

	public String giveMeSomething(RequestMsg request, String question) throws RemoteException {
		return clients.get(request.getToken()).gameRequestHandler(request, question);
	}

}