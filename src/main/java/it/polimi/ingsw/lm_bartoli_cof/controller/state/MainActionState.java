package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.AcquirePermitTile;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.BuildEmporiumWithKing;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.BuildEmporiumWithPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.DisconnectAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.ElectCouncillor;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

/**
 * 
 */
public class MainActionState extends TurnState {

	public final static MainActionState state = new MainActionState();

	public static TurnState getState() {
		return state;
	}

	@Override
	protected boolean isActionValid(PlayerAction action) {
		return (action instanceof BuildEmporiumWithKing || action instanceof BuildEmporiumWithPermitTile
				|| action instanceof ElectCouncillor || action instanceof AcquirePermitTile
				|| action instanceof DisconnectAction);
	}

}