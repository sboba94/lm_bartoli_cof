package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.BuyState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.DrawState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.PassRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.DisplaySomethingResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.market.ForSale;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;

/**
 * This class need to buy an object on sale. It allows to move an object from a
 * player to another. At the buying player will be subtracted the coins
 * corresponding on the price of the object. These coins will be added to the
 * seller.
 */
public class BuyAction extends PlayerAction {

	private GameController gameController;
	private int totalCoinsToPay;
	private String invalidMsg;
	
	/**
	 * This is the list of object that the player would like to buy
	 */
	private LinkedList<ForSale> toBuy;

	/**
	 * This is the constructor of the class.
	 * 
	 * @param player,
	 *            the player that perform the action;
	 * @param gameController,
	 *            to manage the game where player is;
	 * @param toSell,
	 *            this is the list of object to buy.
	 */
	public BuyAction(Player player, GameController gameController, LinkedList<ForSale> toBuy) {
		super(player);
		this.gameController = gameController;
		this.toBuy = toBuy;
		this.totalCoinsToPay=0;
		this.invalidMsg = "";
	}

	/**
	 * This method control if it's possible to perform the action
	 */
	public boolean isValid() {
		for (ForSale x : toBuy) {
			this.totalCoinsToPay += x.getPrice();
		}
		if (super.getPlayer().getCoinsNumber() < totalCoinsToPay) {
			invalidMsg = "You don't have enough coins to perform this action";
			return false;
		} 
		return true;
	}

	/**
	 * This method allows to execute the action;
	 */
	public TurnState execute() {
		if (isValid()) {
			
			for (ForSale x : toBuy) {
				System.out.println(x.toString());
				super.getPlayer().removeCoins(x.getPrice());
				x.getOwner().addCoins(x.getPrice());
				this.transfert(x.getOwner(), x.getObject());
				this.gameController.getGame().getMarket().getSaleList().remove(x);
			}
			String msg = "Player " + super.getPlayer().getNickname() + " has bought some object";
			gameController.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(msg));
			msg = "Request completed, now objects are yours";
			gameController.getMessageCouple().setResponse(new DisplaySomethingResponseMsg(msg));
			this.gameController.getBuyTokenList().removeFirst();
			
			if(!this.gameController.getBuyTokenList().isEmpty()){
				this.gameController.changePlayerInBuy();
				msg = "end his buy turn";
			}
			else{
				gameController.getPublisherInterface().publish(new DisplaySomethingBroadcastMsg("Buy fase is termidated\n"), gameController.makeTopic());
				this.gameController.handleRequest(new PassRequestMsg(this.gameController.getTokenList().getLast()));		
				return DrawState.getState();
			}
		} else {
			gameController.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(" "));
			gameController.getMessageCouple().setResponse(new CompletedRequestResponseMsg(invalidMsg  + "\n\n"));
		}
		return BuyState.getState();
	}

	private void transfert(Player owner, Object o) {
		if (o instanceof BusinessPermitTile) {
			super.getPlayer().getUsablePermitTiles().add((BusinessPermitTile) o);
			owner.getUsablePermitTiles().remove(o);
		} else if (o instanceof PoliticCard) {
			super.getPlayer().getPoliticCards().add((PoliticCard) o);
			owner.getPoliticCards().remove(o);
		} else {
			super.getPlayer().addAssistant();
			owner.removeAssistants(1);
		}

	}

}