package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows to change the permit tile on a board, the player need to
 * choose the board
 */
public class ChangePermitTile extends PlayerAction {

	private GameController game;
	/**
	 * these values were choose by player
	 */
	private RegionBoard board;
	private String invalidMsg;

	/**
	 * This is the constructor of the class
	 * 
	 * @param player
	 * @param game
	 * @param choosedBoard
	 */
	public ChangePermitTile(Player player, GameController game, RegionBoard choosedBoard) {
		super(player);
		this.game = game;
		this.board = choosedBoard;
	}

	/**
	 * this method ask if it's possible to perform the action
	 */
	@Override
	public boolean isValid() {
		if (game.getGame().getCurrentPlayer().isQuickActionExecuted() == true) {
			invalidMsg = "-----> Quick action not available!\n\n";
			return false;
		}
		if (super.getPlayer().getAssistantNumber() < 1) {
			invalidMsg = "-----> You don't have enough assistant to perform this action\n-----> Please repeat the action\n\n";
			return false;
		}
		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	@Override
	public TurnState execute() {
		if (isValid()) {
			super.getPlayer().removeAssistants(1);
			super.getPlayer().setQuickActionExecuted(true);
			this.board.getPermitBoard().changeUsableTiles();
			String msg = this.getPlayer().getNickname() + " has changed tile on " + board.getRegionName() + " board";
			game.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(msg));
			game.getMessageCouple().setResponse(
					new CompletedRequestResponseMsg("Action performed!" + ActionQuestion.askForNextState(getPlayer())));
		} else {
			game.getMessageCouple().setBroadcast(null);
			game.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();
	}

}
