package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.DisconnectAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.EndTurnAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

/**
 * 
 */
public class FinalControlState extends TurnState {

	public final static FinalControlState state = new FinalControlState();

	public static TurnState getState() {
		return state;
	}
	
	@Override
	protected boolean isActionValid(PlayerAction action) {
		
		return (action instanceof EndTurnAction || action instanceof DisconnectAction);
	}
	
	

}