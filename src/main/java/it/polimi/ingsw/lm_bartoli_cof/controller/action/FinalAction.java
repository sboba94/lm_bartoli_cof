package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.model.Game;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;

public class FinalAction extends PlayerAction {

	public final int FIRST_NOBILTYTRACK_AWARD = 5;
	public final int SECOND_NOBILTYTRACK_AWARD = 2;
	public final int PERMITCARD_AWARD = 3;

	private Game game;

	public FinalAction(Player player, Game game) {
		super(player);
		this.game = game;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public TurnState execute() {
		// ogni giocatore guadagna i punti vittoria segnati sulle proprie
		// tessere bonus
		for (Player p : game.getPlayers()) {
			for (Bonus b : p.getBonus()) {
				p.addVictoryPos(b.getValue());
			}
		}
		// il giocatore più avanti sul percorso della nobilta guadagna 5 punti
		// vittoria, il secondo 2(se primi a parimerito guadagnano tutti 5 e il
		// secondo nulla, se secondi a parimerito guadagnano tutti 2)
		LinkedList<Player> playersFirst = game.firstOnNobiltyTrack();
		if (playersFirst.size() > 1) {
			for (Player p : playersFirst) {
				p.addVictoryPos(this.FIRST_NOBILTYTRACK_AWARD);
			}
		} else if (playersFirst.size() == 1) {
			game.firstOnNobiltyTrack().getFirst().addVictoryPos(this.FIRST_NOBILTYTRACK_AWARD);
			LinkedList<Player> playersSecond = game.firstOnNobiltyTrack();
			if (playersSecond.size() > 1) {
				for (Player p : playersSecond) {
					p.addVictoryPos(this.SECOND_NOBILTYTRACK_AWARD);
				}
			}
		}
		// il giocatore con più tessere permesso guadagna 3 punti vittoria in
		// caso di parimerito si guarda il giocatore che possiede piu
		// aiutanti+cartePolitica.. Fa già tutto la funzione
		game.ownerWithTheGreaterNumberOfPermitCards().addVictoryPos(PERMITCARD_AWARD);

		// devo in qualche modo passare il vincitore??
		// che stato devo passare??
		System.out.println("Fine gioco");
		return null;
	}

}
