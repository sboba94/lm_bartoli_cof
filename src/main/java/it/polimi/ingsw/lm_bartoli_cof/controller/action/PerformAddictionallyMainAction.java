package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.CompletedRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows to perform an additionally main action
 */
public class PerformAddictionallyMainAction extends PlayerAction {

	private GameController game;
	private String invalidMsg;

	/**
	 * This is the constructor of the action
	 * 
	 * @param player
	 * @param game
	 */
	public PerformAddictionallyMainAction(Player player, GameController game) {
		super(player);
		this.game = game;
	}

	/**
	 * this method ask if it's possible to perform the action
	 */
	public boolean isValid() {
		if (game.getGame().getCurrentPlayer().isQuickActionExecuted() == true) {
			invalidMsg = "-----> Quick action not available!\n\n";
			return false;
		}
		if (super.getPlayer().getAssistantNumber() < 3) {
			invalidMsg = "-----> You don't have enough assistant to perform this action\n-----> Please repeat the action\n\n";
			return false;
		}
		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		if (isValid()) {
			super.getPlayer().setQuickActionExecuted(true);
			super.getPlayer().removeAssistants(3);
			super.getPlayer().addNumberOfPrimaryAction(1);
			String msg = this.getPlayer().getNickname() + " has chosen to perfom an addictional main action";
			game.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(msg));
			game.getMessageCouple()
					.setResponse(new CompletedRequestResponseMsg(ActionQuestion.askForNextState(getPlayer())));
		} else {
			game.getMessageCouple().setBroadcast(null);
			game.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();
	}

}