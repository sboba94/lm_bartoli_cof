package it.polimi.ingsw.lm_bartoli_cof.controller;

import java.rmi.RemoteException;
import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactory;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.ActionCreatorFactoryImpl;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.BuyState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.MainActionState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.QuickActionState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.SellState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnMachine;
import it.polimi.ingsw.lm_bartoli_cof.messages.MessageCouple;
import it.polimi.ingsw.lm_bartoli_cof.messages.Token;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplayToClient;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.StartGameBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.DisconnectRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.MainActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.QuickActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.RequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.AcquirePermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.ActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuildEmporiumWithPermitTileRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.BuyRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DisconnectActionRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.SellRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.DisconnectResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.DisplaySomethingResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidRequestResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.ResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Game;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.CityColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionName;
import it.polimi.ingsw.lm_bartoli_cof.model.market.ForSale;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.RewardToken;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;
import it.polimi.ingsw.lm_bartoli_cof.view.server.PublisherInterface;

public class GameController {

	private Setting config;
	private int gameID; // the gameID will be equals to the number of Games in
						// that moment
	private Game game;

	// those two attributes are used in case of someone's victory
	private boolean lastRound; // used to do last round
	private Player lastPlayer; // used to identify the last player that can do
								// an action

	private MessageCouple messages = new MessageCouple();
	private final TurnMachine turnMachine;
	private LinkedList<Token> playersToken;
	private ActionCreatorFactory actionFactory;
	private PublisherInterface publisherInterface;
	private LinkedList<Token> buyTokenList;

	/**
	 * Default constructor
	 */
	public GameController(PublisherInterface publisher, String mapRoot) {

		this.gameID = GamesController.numberOfGames;
		this.config = new Setting(mapRoot);

		// There will be only a turn machine for each game controller
		this.turnMachine = new TurnMachine(this);
		this.lastRound = false;
		this.playersToken = new LinkedList<Token>();
		this.publisherInterface = publisher;

		publisherInterface.addTopic(makeTopic());
	}

	/**
	 * getter of turn machine
	 * 
	 * @return the turnMachine
	 */
	public TurnMachine getTurnMachine() {
		return turnMachine;
	}

	public LinkedList<Token> getTokenList() {
		return this.playersToken;
	}

	public Setting getSettings() {

		return this.config;
	}

	public LinkedList<Token> getBuyTokenList() {
		return buyTokenList;
	}

	public PublisherInterface getPublisherInterface() {

		return this.publisherInterface;
	}

	public MessageCouple getMessageCouple() {
		return this.messages;
	}

	public Game getGame() {
		return this.game;
	}

	public int getGameID() {
		return gameID;
	}

	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	public boolean isLastRound() {
		return lastRound;
	}

	public void setLastRound(boolean lastRound) {
		this.lastRound = lastRound;
	}

	public Player getLastPlayer() {
		return lastPlayer;
	}

	public void setLastPlayer(Player lastPlayer) {
		this.lastPlayer = lastPlayer;
	}

	private boolean isPlayerTurn(Token token) {

		return game.getCurrentPlayer().getId() == token.getPlayerNumber();

	}

	/**
	 * Create the action and execute it!
	 * 
	 * @param request
	 * @return a MessageCouple composed by Broadcast msg and Response msg
	 */
	private MessageCouple executeAction(ActionRequestMsg request) {
		PlayerAction action;

		// Create the appropriate action using the Visitor Pattern
		action = request.createAction(actionFactory);

		turnMachine.executeAction(action);

		// each action must be reset and set the global message couple!!
		return this.messages;

	}

	/**
	 * Handle a request passed by GamesController. Tipically handle all request
	 * of actions.
	 * 
	 * @param request
	 * @return a response message to print on client's cli
	 */
	public ResponseMsg handleRequest(RequestMsg request) {

		// if game doesn't started yet this is an invalid request!
		if (game == null) {
			return new InvalidRequestResponseMsg("Please wait for the game to start");
		}

		// if isn't your turn this is an invalid request!
		if (!isPlayerTurn(request.getToken()) && !this.turnMachine.getState().equals(SellState.getState())
				&& !this.turnMachine.getState().equals(BuyState.getState())) {
			return new InvalidRequestResponseMsg("Please wait your turn to play");
		}
		if (request instanceof MainActionRequestMsg) {
			// non puo farlo se non ha azioni
			this.turnMachine.setState(MainActionState.getState());
			return new DisplaySomethingResponseMsg(ActionQuestion.askForMainAction());
		} else if (request instanceof QuickActionRequestMsg) {
			// non puo farlo se non ha azioni
			this.turnMachine.setState(QuickActionState.getState());
			return new DisplaySomethingResponseMsg(ActionQuestion.askForQuickAction());
		}

		if (request instanceof ActionRequestMsg) {

			MessageCouple result = executeAction((ActionRequestMsg) request);

			if (result.getBroadcast() != null) {
				// Publish a public message
				publisherInterface.publish(result.getBroadcast(), makeTopic());

			}

			// teminate game
			// if (isGameFinished()) {
			// finishGame();
			// }

			return result.getResponse();

		}
		if (request instanceof DisconnectRequestMsg) {
			executeAction(new DisconnectActionRequestMsg(request.getToken()));
			return new DisconnectResponseMsg(this.getPlayerByToken(request.getToken()));
		}

		// never reached, just to eliminate the error
		return null;
	}

	/**
	 * The topic is the name of game and is composed by 'game' plus idGame.
	 * 
	 * @return the topic string
	 */
	public String makeTopic() {
		return "game " + this.gameID;
	}

	/**
	 * Add a player to the game (only if the game did not already started).
	 *
	 * @param token
	 *            the token of the player to add
	 */
	public void addPlayer(Token token) {

		if (game == null) {
			playersToken.add(token);
			// Subscribe the client to the topic of this game
			publisherInterface.subscribeClientToTopic(makeTopic(), token);
		}

	}

	/**
	 * Start a game. Create a game's object, an action factory and publish a
	 * start game message in broadcast mode
	 */
	public void initGame() {
		try {
			this.game = new Game(this.gameID, config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		GamesController.waitingRooms.remove(this);

		actionFactory = new ActionCreatorFactoryImpl(this);

		int i = 0;
		for (Token t : this.getTokenList()) {
			this.game.addNewPlayer(new Player(t.getPlayerNumber(), 1 + i, 10 + i, "Player " + t.getPlayerNumber()));
			i++;
		}
		this.setInitPlayer();

		// il broker pubblica il broadcast msg
		publisherInterface.publish(new StartGameBroadcastMsg(getGameID()), makeTopic());

	}

	/**
	 * Sets initial player. Init player is the first Token in Token's list in
	 * GameController
	 */
	public void setInitPlayer() {
		this.getGame().setCurrentPlayer(this.playersToken.getFirst().getPlayerNumber());
	}

	public void changePlayer(Player player) {
		Player newPlayer;
		int index = 0;
		if (getGame().getPlayers().indexOf(this.getGame().getCurrentPlayer()) == getGame().getPlayers().size() - 1) {
			newPlayer = this.getGame().getPlayers().get(index);
		} else {
			index = this.getGame().getPlayers().indexOf(this.getGame().getCurrentPlayer()) + 1;
			newPlayer = this.getGame().getPlayers().get(index);
		}
		if (!newPlayer.isConnect()) {
			newPlayer = this.getGame().getPlayers().get(index + 1);
		}
		this.getGame().setCurrentPlayer(newPlayer.getId());

	}

	public Token getTokenByPlayer(Player player) {
		for (Token t : this.getTokenList())
			if (t.getPlayerNumber() == player.getId())
				return t;
		return null;
	}

	public void useBonusByType(Bonus bonus, Player player) {
		switch (bonus.getType()) {
		case ASSISTANTS:
			player.addAssistants(bonus.getValue());
			break;
		case MONEY:
			player.addCoins(bonus.getValue());
			break;
		case POLITIC:
			try {
				for (int i = 0; i < bonus.getValue(); i++)
					player.addPoliticCard(game.getPoliticDeck().drawCard());
			} catch (NoMorePoliticCardsException e) {
				e.getMessage();
			}
			break;
		case ACTION:
			player.addNumberOfPrimaryAction(1);
			break;
		case VICTORY:
			player.addVictoryPos(bonus.getValue());
			break;
		case NOBILITY:
			this.addNobilityPos(bonus.getValue(), player);
			break;
		case ACQUIREPERMITCARD:
			BusinessPermitTile acquiredTile = ActionQuestion
					.choosePermitTileFromBoard(ActionQuestion.choosePermitBoard(game.getGameMap()));
			for (Bonus permitBonus : acquiredTile.getBonuses())
				this.useBonusByType(permitBonus, player);
			player.addPermitTile(acquiredTile);
			break;
		case GETBONUSFROMPERMITCARD:
			BusinessPermitTile chosenTile = ActionQuestion.choosePermitTileFromHand(player);
			for (Bonus chosenBonus : chosenTile.getBonuses())
				this.useBonusByType(chosenBonus, player);
			break;
		case GETBONUSFROMTOKEN:
			LinkedList<City> usableCities = new LinkedList<City>();
			for (City thisCity : player.getEmporiums())
				usableCities.add(game.getGameMap().getCities().getVertexFromName(thisCity.getName()));

			// removes from the list of choosable cities the ones that has a
			// NOBILITY bonus
			for (City usableCity : usableCities) {
				for (Bonus singleBonus : usableCity.getToken().getBonuses())
					if (singleBonus.getType() == BonusType.NOBILITY)
						usableCities.remove(usableCity);
			}

			for (int i = 0; i < bonus.getValue(); i++) {
				City chosenCity = ActionQuestion.chooseTokenFromCity(usableCities);
				if (chosenCity == null)
					break;
				usableCities.remove(chosenCity);
				RewardToken chosenToken = chosenCity.getToken();
				for (Bonus chosenTokenBonus : chosenToken.getBonuses())
					this.useBonusByType(chosenTokenBonus, player);
			}
			break;
		default:

			throw new InvalidParameterException();
		}
	}

	private void addNobilityPos(int addPos, Player player) {
		int nobilityPos = player.getNobilityPos() + addPos;
		player.setNobilityPos(nobilityPos);
		LinkedList<Bonus> bonusToAssign = game.getGameMap().getKingBoard().getTrack().getAllBonusAtKey(nobilityPos);
		for (Bonus b : bonusToAssign) {
			this.useBonusByType(b, player);
		}
	}

	public void createBuyState() {
		buyTokenList = new LinkedList<Token>();
		for (Token p : this.playersToken)
			if (this.getPlayerByToken(p).isConnect())
				buyTokenList.add(p);
		Collections.shuffle(buyTokenList);
		changePlayerInBuy();
	}

	public void changePlayerInBuy() {
		Token t = buyTokenList.getFirst();
		publisherInterface.publish(new DisplaySomethingBroadcastMsg(
				"Player " + this.getPlayerByToken(t).getNickname() + " starts to buy\n"), makeTopic());
		publisherInterface.publish(new DisplayToClient("Press buy to init Buy fase\n", t), makeTopic());
	}

	/**
	 * This method is used to return some parameter, some attributes o some
	 * object to the client in a String format. It's necessary because we can't
	 * pass to client an object unserializable so we pass a String that is
	 * serializable!
	 * 
	 */
	public String gameRequestHandler(RequestMsg request, String question) throws RemoteException {

		StringBuilder string = new StringBuilder();

		if (question.equals("TilesOnBoard")) {
			string.append("These are the face up permit tiles on this board: \n");
			switch (((AcquirePermitTileRequestMsg) request).getPermitBoard()) {
			case "Coast":
				string.append("1) "
						+ this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile1().toString()
						+ "\n");
				string.append("2) "
						+ this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile2().toString()
						+ "\n");
				break;
			case "Hills":
				string.append("1) "
						+ this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile1().toString()
						+ "\n");
				string.append("2) "
						+ this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile2().toString()
						+ "\n");
				break;
			case "Mountain":
				string.append("1) "
						+ this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile1().toString()
						+ "\n");
				string.append("2) "
						+ this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile2().toString()
						+ "\n");
				break;
			}

		}

		if (question.equals("PoliticInMyHand")) {
			string.append("\nThese are the politic cards in your hand: \n");
			for (PoliticCard c : this.getGame().getCurrentPlayer().getPoliticCards())
				string.append(c.getPoliticCardColor().toString() + "\n");
		}

		if (question.equals("NumeratedPoliticInMyHand")) {
			string.append("\nThese are the politic cards in your hand: \n");
			int i = 0;
			for (PoliticCard c : this.getGame().getCurrentPlayer().getPoliticCards()) {
				string.append(i + ") " + c.getPoliticCardColor().toString() + "\n");
				i++;
			}
		}

		if (question.equals("UsablePermitTiles")) {
			Player player = this.getGame().getPlayers().get(this.getTokenList().indexOf(request.getToken()));
			if (!player.getUsablePermitTiles().isEmpty()) {
				string.append("\nThese are the business permit tiles in your hand: \n");
				int i = 1;
				for (BusinessPermitTile p : player.getUsablePermitTiles()) {
					string.append(i + ") " + p.toString() + "\n");
					i++;
				}
			}
		}

		if (question.equals("RegionPermitBoards")) {

			// COAST
			string.append("COAST: \n" + "\n");
			string.append("These are the face up permit tiles on the Coast's permit board: \n");
			string.append(
					this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile1().toString() + " \n");
			string.append(this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile2().toString()
					+ " \n" + "\n");

			// HILLS
			string.append("HILLS: \n" + "\n");
			string.append("These are the face up permit tiles on the Hill's permit board: \n");
			string.append(
					this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile1().toString() + " \n");
			string.append(this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile2().toString()
					+ " \n" + "\n");

			// MOUNTAIN
			string.append("MOUNTAIN: \n" + "\n");
			string.append("These are the face up permit tiles on the Mountain's permit board: \n");
			string.append(this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile1().toString()
					+ " \n");
			string.append(this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile2().toString()
					+ " \n" + "\n");

		}

		if (question.equals("RegionCouncilBalconies")) {

			// COAST
			string.append("************** COAST **************\n\n");
			string.append("These are the councillor on the Coast's council balcony: \n");
			for (Councillor councillor : this.getGame().getGameMap().getCoast().getCouncil().getCouncil()) {
				string.append(councillor.getColor().toString() + " \n");
			}
			string.append("\n");
			string.append("These are the face up permit tiles on the Coast's permit board: \n");
			string.append(
					this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile1().toString() + " \n");
			string.append(this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile2().toString()
					+ " \n" + "\n");

			// HILLS
			string.append("************** HILLS **************\n\n");
			string.append("These are the councillor on the Hill's council balcony: \n");
			for (Councillor councillor : this.getGame().getGameMap().getHill().getCouncil().getCouncil()) {
				string.append(councillor.getColor().toString() + " \n");
			}
			string.append(" \n");
			string.append("These are the face up permit tiles on the Hill's permit board: \n");
			string.append(
					this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile1().toString() + " \n");
			string.append(this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile2().toString()
					+ " \n" + "\n");

			// MOUNTAIN
			string.append("************** MOUNTAIN **************\n\n");
			string.append("These are the councillor on the Mountain's council balcony: \n");
			for (Councillor councillor : this.getGame().getGameMap().getMountain().getCouncil().getCouncil()) {
				string.append(councillor.getColor().toString() + " \n");
			}
			string.append(" \n");
			string.append("These are the face up permit tiles on the Mountain's permit board: \n");
			string.append(this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile1().toString()
					+ " \n");
			string.append(this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile2().toString()
					+ " \n" + "\n");

		}

		if (question.equals("AllCouncilBalcony")) {

			// COAST
			string.append("\nCOAST \n");
			string.append("These are the councillor on the Coast's council balcony: \n");
			for (Councillor councillor : this.getGame().getGameMap().getCoast().getCouncil().getCouncil()) {
				string.append(councillor.getColor().toString() + " \n");
			}

			// HILLS
			string.append("\nHILLS \n");
			string.append("These are the councillor on the Hill's council balcony: \n");
			for (Councillor councillor : this.getGame().getGameMap().getHill().getCouncil().getCouncil()) {
				string.append(councillor.getColor().toString() + " \n");
			}

			// MOUNTAIN
			string.append("\nMOUNTAIN \n");
			string.append("These are the councillor on the Mountain's council balcony: \n");
			for (Councillor councillor : this.getGame().getGameMap().getMountain().getCouncil().getCouncil()) {
				string.append(councillor.getColor().toString() + " \n");
			}

			// KING
			string.append("\nKING \n");
			string.append("These are the councillor on the King's council balcony: \n");
			for (Councillor councillor : this.getGame().getGameMap().getKingBoard().getCouncil().getCouncil()) {
				string.append(councillor.getColor().toString() + " \n");
			}
			string.append(" \n");

		}

		if (question.equals("City")) {
			for (City c : this.getGame().getGameMap().getCities().vertexSet())
				string.append(c.getName() + "\n");
			string.append("Type the name of the city that you want: ");
		}

		if (question.equals("AvailableColors")) {
			string.append("Councillor's colors available to be elected: \n");
			for (CouncillorColors color : CouncillorColors.values())
				for (Councillor c : this.getGame().getGameMap().getCouncillorPool())
					if (c.getColor().equals(color)) {
						string.append(color.toString() + "\n");
						break;
					}
		}

		if (question.equals("CityOnPermitTile")) {
			Player player = this.getGame().getPlayers().get(this.getTokenList().indexOf(request.getToken()));
			BusinessPermitTile tile = player.getUsablePermitTiles()
					.get(Integer.parseInt(((BuildEmporiumWithPermitTileRequestMsg) request).getTile()) - 1);
			for (City c : tile.getCities())
				string.append("- " + c.getName() + "\n");
		}

		if (request instanceof SellRequestMsg && question.equals("NumberOfAssistant")) {
			string.append("Number of assistance: " + this.getGame().getCurrentPlayer().getAssistantNumber());
		}
		if (request instanceof BuyRequestMsg && question.equals("BuyList")) {

			LinkedList<ForSale> forSaleList = this.getGame().getMarket().getSaleList();
			if (forSaleList.isEmpty()) {
				return null;
			} else {
				string.append("BuyList :\n");
				String s = null;
				for (ForSale f : forSaleList) {
					if (f.getObject() instanceof BusinessPermitTile)
						s = " Type: Permit Tile ";
					else if (f.getObject() instanceof PoliticCard)
						s = " Type: Politic Card ";
					else if (f.getObject() instanceof BonusType)
						s = " Type: Assistant ";
					string.append(forSaleList.indexOf(f) + s + " Price:" + f.getPrice() + "coins - "
							+ f.getObject().toString() + "\n");
				}

			}
		}

		return string.toString();
	}

	/**
	 * This method build a string that contains all information of the game.
	 * It's used to return all parameter to client and print a dashboard.
	 * 
	 * @param token
	 * @return
	 */
	public String printGameBoard(Token token) {
		StringBuilder string = new StringBuilder();
		string.append(
				"\n++++++++++++++++++++++++++++++     Game Configuration     +++++++++++++++++++++++++++++++++\n");

		string.append(
				"\n**************************************      Map     ***************************************\n");

		int i = 0;
		for (City c : this.getGame().getGameMap().getCities().vertexSet()) {

			// REGIONS WITH RELATIVES CITIES
			switch (i) {
			case 0:
				string.append(
						"\n                                          COAST                                            \n");
				break;
			case 5:
				string.append(
						"\n                                          HILLS                                            \n");
				break;
			case 10:
				string.append(
						"\n                                         MOUNTAIN                                          \n");
				break;
			}
			string.append("+ " + c.getName() + " connected to ");
			string.append("[ ");
			for (City c2 : this.getGame().getGameMap().getCities().findNeighbours(c))
				string.append(c2.getName() + " ");
			string.append("]");
			string.append(" -- " + c.getToken().toString() + "\n");

			// BUSINESS PERMIT TILES AND BALCONIES
			switch (i) {
			case 4:
				string.append("\nBusiness Permit Tiles: \n");
				string.append("1) "
						+ this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile1().toString()
						+ "\n");
				string.append("2) "
						+ this.getGame().getGameMap().getCoast().getPermitBoard().showUsablePermitTile2().toString()
						+ "\n");
				string.append("\nBonus Tile: \n");
				string.append(this.getGame().getGameMap().getBonusTileByPosition(RegionName.COAST) + "\n");
				string.append("\nCoucil Balcony: \n");
				for (Councillor councillor : this.getGame().getGameMap().getCoast().getCouncil().getCouncil()) {
					string.append(councillor.getColor().toString() + " \n");
				}
				string.append(
						"\n-------------------------------------------------------------------------------------------\n");
				break;
			case 9:
				string.append("\nBusiness Permit Tiles: \n");
				string.append("1) "
						+ this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile1().toString()
						+ "\n");
				string.append("2) "
						+ this.getGame().getGameMap().getHill().getPermitBoard().showUsablePermitTile2().toString()
						+ "\n");
				string.append("\nBonus Tile: \n");
				string.append(this.getGame().getGameMap().getBonusTileByPosition(RegionName.HILLS) + "\n");
				string.append("\nCoucil Balcony: \n");
				for (Councillor councillor : this.getGame().getGameMap().getHill().getCouncil().getCouncil()) {
					string.append(councillor.getColor().toString() + " \n");
				}
				string.append(
						"\n-------------------------------------------------------------------------------------------\n");
				break;
			case 14:
				string.append("\nBusiness Permit Tiles: \n");
				string.append("1) "
						+ this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile1().toString()
						+ "\n");
				string.append("2) "
						+ this.getGame().getGameMap().getMountain().getPermitBoard().showUsablePermitTile2().toString()
						+ "\n");
				string.append("\nBonus Tile: \n");
				string.append(this.getGame().getGameMap().getBonusTileByPosition(RegionName.MOUNTAIN) + "\n");
				string.append("\nCoucil Balcony: \n");
				for (Councillor councillor : this.getGame().getGameMap().getMountain().getCouncil().getCouncil()) {
					string.append(councillor.getColor().toString() + " \n");
				}
				string.append(
						"\n-------------------------------------------------------------------------------------------\n");
				break;
			}

			i++;
		}

		// KIND BALCONY, KING'S DECK AND BONUS TILES
		string.append("\nKing's Council Balcony: \n");
		for (Councillor councillor : this.getGame().getGameMap().getKingBoard().getCouncil().getCouncil()) {
			string.append(councillor.getColor().toString() + " \n");
		}
		string.append("\nFirst King's Reward Tile: \n");
		string.append(
				this.getGame().getGameMap().getKingBoard().getKingRewards().getDrawPile().element().toString() + "\n");
		string.append("\nBonus Tiles: \n");
		string.append(
				"- " + this.getGame().getGameMap().getBonusTileByColor(CityColors.IRON.name()).toString() + ";\n");
		string.append(
				"- " + this.getGame().getGameMap().getBonusTileByColor(CityColors.BRONZE.name()).toString() + ";\n");
		string.append(
				"- " + this.getGame().getGameMap().getBonusTileByColor(CityColors.SILVER.name()).toString() + ";\n");
		string.append(
				"- " + this.getGame().getGameMap().getBonusTileByColor(CityColors.GOLD.name()).toString() + ";\n");

		// KING POSITION
		string.append("\nKing Position: " + this.getGame().getGameMap().getKing().getCity().toString() + "\n");

		// COUNCILLOR POLL
		string.append("\nCouncillor Pool: ");
		string.append(this.getGame().getGameMap().getCouncillorPool().toString() + "\n");

		// NOBILITY TRACK
		string.append("\nNobility Track: \n");
		string.append(this.getGame().getGameMap().getKingBoard().getTrack().toString());

		string.append(
				"\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

		return string.toString();
	}

	public String printPlayersInfo(Token token) {
		StringBuilder string = new StringBuilder();
		string.append(
				"\n*******************************      Players information    *******************************\n");
		for (Player p : this.getGame().getPlayers()) {
			string.append("\n- " + p.getNickname() + ":\n");
			string.append("+ Coins: " + p.getCoinsNumber() + "\n+ Victory Position: " + p.getVictoryPos() + "\n");
			string.append("+ Nobility Position: " + p.getNobilityPos() + "\n+ Assistants: " + p.getAssistantNumber());
			string.append("\n+ Emporiums: ");
			for (City c : p.getEmporiums())
				string.append("[ " + c.getName() + " ]");
			string.append("\n+ Face-Up Permit Tiles: ");
			for (BusinessPermitTile tile : p.getUsablePermitTiles())
				string.append("[ " + tile.toString() + " ]");
			string.append("\n");
		}
		string.append("---------------------------------------------------------------------------------------\n");
		return string.toString();
	}

	public Player getPlayerByToken(Token t) {
		for (Player p : this.game.getPlayers())
			if (p.getId() == t.getPlayerNumber())
				return p;
		return null;
	}

}