package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.List;

import it.polimi.ingsw.lm_bartoli_cof.controller.state.DrawState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.model.Game;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;

public class StartConfigAction extends PlayerAction {

	private boolean executed;
	private final List<Player> players;
	private final Game game;

	/**
	 * Constructor of the StartConfigAction
	 * @param game
	 */
	public StartConfigAction(Game game) {
		super(null);
		this.players = game.getPlayers();
		this.game = game;
		this.executed = false;
	}
	
	/**
	 * this method check if it's possible to perform the action
	 */
	public boolean isValid() {
		if (executed) {
			return false;
		}
		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		game.getPoliticDeck().shuffleDeck();
		for (int i = 0; i < players.size(); i++)
			for (int j = 0; j < 6; j++)
				try {
					players.get(i).addPoliticCard(game.getPoliticDeck().drawCard());
				} catch (NoMorePoliticCardsException e) {
					e.getMessage();
				}
		this.executed = true;
		return DrawState.getState();

	}

}