package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.ChangePermitTile;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.DisconnectAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.ElectCouncillorWithAssistant;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.EngageAssistant;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PerformAddictionallyMainAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

/**
 * 
 */
public class QuickActionState extends TurnState {

	public final static QuickActionState state = new QuickActionState();

	public static TurnState getState() {
		return state;
	}

	/**
	 * These method check if the action can be performed, it must be a Quick
	 * Action and must be valid
	 */
	@Override
	protected boolean isActionValid(PlayerAction action) {
		return (action instanceof EngageAssistant || action instanceof ElectCouncillorWithAssistant
				|| action instanceof PerformAddictionallyMainAction || action instanceof ChangePermitTile
				|| action instanceof DisconnectAction);
	}

}