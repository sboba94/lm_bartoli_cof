package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.DisconnectAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.StartConfigAction;

public class StartConfigState extends TurnState {

	//this allows you to create only one object ad reuse it for all cicles of the FSA
	private final static TurnState state = new StartConfigState();
	
	

	public static TurnState getState() {
		return state;
	}

	/**
	 * questa controlla che l'azione si possa fare in questo determinato stato
	 */
	@Override
	protected boolean isActionValid(PlayerAction action) {
		return (action instanceof StartConfigAction || action instanceof DisconnectAction);
	}

	

}
