package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.FinalControlState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.MainActionState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.BuildEmporiumBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplaySomethingBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.BuildEmporiumResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.GameMap;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

/**
 * This action allows to use build an emporium a permit tile in your hand,
 * player need to choose the permit tile that want to use
 */
public class BuildEmporiumWithPermitTile extends PlayerAction {

	private GameController gameController;
	private GameMap gameMap;
	private int totalCoinsToPay;
	private int totalAssistantsToUse;

	/**
	 * these values were choose by player
	 */
	private BusinessPermitTile tileToUse;
	private City buildEmporiumIn;
	private String invalidMsg;

	/**
	 * this is the constructor of the action
	 * 
	 * @param player
	 * @param game
	 * @param permitTile
	 * @param city
	 */
	public BuildEmporiumWithPermitTile(Player player, GameController game, BusinessPermitTile permitTile, City city) {
		super(player);
		this.gameController = game;
		this.gameMap = gameController.getGame().getGameMap();
		this.tileToUse = permitTile;
		this.buildEmporiumIn = city;
	}

	/**
	 * this method ask if it's possible to perform the action
	 */
	public boolean isValid() {
		if (gameController.getGame().getCurrentPlayer().getNumberOfPrimaryAction() == 0) {
			invalidMsg = "-----> Main action not available!\n\n";
			return false;
		}
		if (!this.getPlayer().getUsablePermitTiles().contains(tileToUse)) {
			invalidMsg = "-----> This tile is not available\n-----> Please repeat the action\n\n";
			return false;
		}
		return true;
	}

	/**
	 * this method allows to perform the action
	 */
	public TurnState execute() {
		if (isValid()) {
			LinkedList<City> cities = tileToUse.getCities();
			for (int i = 0; i < cities.size(); i++) {
				if (this.getPlayer().getEmporiums().contains(cities.get(i))) {
					cities.remove(i);
				}
			}
			totalAssistantsToUse = buildEmporiumIn.getEmporiumNumber();
			if (totalAssistantsToUse > this.getPlayer().getAssistantNumber())
				return MainActionState.getState();
			LinkedList<Bonus> totalBonuses = new LinkedList<Bonus>();
			super.getPlayer().addNumberOfPrimaryAction(-1);
			this.getPlayer().buildEmporium(buildEmporiumIn);
			super.getPlayer().removeAssistants(totalAssistantsToUse);
			super.getPlayer().getUsablePermitTiles().remove(tileToUse);
			super.getPlayer().getUsedPermitTiles().add(tileToUse);
			LinkedList<Bonus> tileBonuses = tileToUse.getBonuses();
			totalBonuses.addAll(tileBonuses);
			for (int i = 0; i < tileBonuses.size(); i++)
				gameController.useBonusByType(tileToUse.getBonuses().get(i), super.getPlayer());
			// controllo se ha tutte le città di quel colore
			if (this.getPlayer().hasAllCitiesByColor(gameMap.getCities(), buildEmporiumIn.getColor())) {
				if (gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()) != null) {
					this.getPlayer()
							.addBonus(gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()).getBonus());
					gameController.useBonusByType(
							gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()).getBonus(),
							super.getPlayer());
					totalBonuses.add(gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()).getBonus());
					this.gameMap.getBonusTiles().remove(gameMap.getBonusTileByColor(buildEmporiumIn.getColor().name()));
				}
			}
			// Check if it has all city in a region depends on RegionBoard
			RegionBoard realRegion = gameMap.getRealRegion(buildEmporiumIn.getRegionName());
			if (this.getPlayer().hasAllCitiesByRegion(realRegion)) {
				if (gameMap.getBonusTileByPosition(realRegion.getRegionName()) != null) {
					this.getPlayer().addBonus(gameMap.getBonusTileByPosition(realRegion.getRegionName()).getBonus());
					gameController.useBonusByType(gameMap.getBonusTileByPosition(realRegion.getRegionName()).getBonus(),
							super.getPlayer());

					totalBonuses.add(gameMap.getBonusTileByPosition(realRegion.getRegionName()).getBonus());

					this.gameMap.getBonusTiles().remove(gameMap.getBonusTileByPosition(realRegion.getRegionName()));
				} else {
					throw new NullPointerException("City color value is NULL");
				}
			}
			gameController.getMessageCouple().setBroadcast(
					new BuildEmporiumBroadcastMsg(this.getPlayer(), this.totalCoinsToPay, buildEmporiumIn));
			gameController.getMessageCouple().setResponse(new BuildEmporiumResponseMsg(buildEmporiumIn,
					this.totalCoinsToPay, totalAssistantsToUse, totalBonuses, getPlayer()));
		} else {
			gameController.getMessageCouple().setBroadcast(new DisplaySomethingBroadcastMsg(null));
			gameController.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					invalidMsg + "\n\n" + ActionQuestion.askForNextState(super.getPlayer())));
		}
		return FinalControlState.getState();
	}

}