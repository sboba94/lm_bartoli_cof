package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

/**
 * This is the superClass of all State
 */
public abstract class TurnState {
	
	/*
	 * Each State in his constructor will make start the Corresponding Action
	 * checking if action is valid
	 */	
	/**
	 * 
	 * 
	 * @return true if the action is valid 
	 */
	protected abstract boolean isActionValid(PlayerAction action);

	/**
	 * questa esegue l'azione nello stato e ritorna il prossimo stato (che viene
	 * comunicato dalla azione che viene effettuata)
	 */
	protected TurnState executeAction(PlayerAction action) {
		return action.execute();
		
	}
}