package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.DisconnectAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.DrawPoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

/**
 * This class represents the DrawState in the TurnMachine
 */
public class DrawState extends TurnState {
	
	private final static DrawState state = new DrawState();
	
	public static TurnState getState() {
		return state;
	}

	/**
	 * check if it's possible to perform the action
	 */
	@Override
	protected boolean isActionValid(PlayerAction action) {
		 return (action instanceof DrawPoliticCard || action instanceof DisconnectAction);
	}

}