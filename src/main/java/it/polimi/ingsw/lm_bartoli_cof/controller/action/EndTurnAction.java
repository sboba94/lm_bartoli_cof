package it.polimi.ingsw.lm_bartoli_cof.controller.action;

import it.polimi.ingsw.lm_bartoli_cof.controller.GameController;

import it.polimi.ingsw.lm_bartoli_cof.controller.state.BuyState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.DrawState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.SellState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.TurnState;
import it.polimi.ingsw.lm_bartoli_cof.controller.state.WinState;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.DisplayToClient;
import it.polimi.ingsw.lm_bartoli_cof.messages.broadcast.InitMarketBroadcastMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.request.action.DrawPoliticCardRequestMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.DisplaySomethingResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.messages.response.InvalidActionResponseMsg;
import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.view.questions.ActionQuestion;

public class EndTurnAction extends PlayerAction {

	private GameController game;

	public EndTurnAction(Player player, GameController game) {
		super(player);
		this.game = game;
	}

	@Override
	public boolean isValid() {
		if (this.getPlayer().getNumberOfPrimaryAction() == 0)
			return true;
		return false;
	}

	@Override
	public TurnState execute() {
		if (isValid()) {
			if (!game.isLastRound()) {
				if (this.getPlayer().getEmporiums().size() == game.getGame().getNumberOfCityToWin()) {
					this.getPlayer().addVictoryPos(3);
					game.setLastRound(true);
					game.setLastPlayer(game.getGame().giveMeLastPlayer(this.getPlayer()));
					game.changePlayer(this.getPlayer());
					game.getTurnMachine().setState(DrawState.getState());
					game.handleRequest(
							new DrawPoliticCardRequestMsg(game.getTokenByPlayer(game.getGame().getCurrentPlayer())));
					return DrawState.getState();
				}

				else if (game.getTokenList().getLast().getPlayerNumber() == this.getPlayer().getId()
						&& !game.getTurnMachine().getState().equals(BuyState.getState())
						&& !game.getTurnMachine().getState().equals(SellState.getState())) {
					game.getMessageCouple().setBroadcast(new InitMarketBroadcastMsg(this.getPlayer()));
					game.getMessageCouple().setResponse(new DisplaySomethingResponseMsg(""));
					return SellState.getState();
				} else if (game.getTurnMachine().getState().equals(BuyState.getState())) {
					game.changePlayer(this.getPlayer());
					game.getTurnMachine().setState(DrawState.getState());
					game.handleRequest(
							new DrawPoliticCardRequestMsg(game.getTokenByPlayer(game.getGame().getCurrentPlayer())));
					game.getPublisherInterface().publish(
							new DisplayToClient(ActionQuestion.askForNextState(game.getGame().getCurrentPlayer()),
									game.getTokenByPlayer(game.getGame().getCurrentPlayer())),
							game.makeTopic());
					game.getMessageCouple().setBroadcast(null);
					game.getMessageCouple().setResponse(new DisplaySomethingResponseMsg(""));
					return DrawState.getState();

				} else if (game.getTurnMachine().getState().equals(SellState.getState())) {
					game.changePlayer(this.getPlayer());
					game.getTurnMachine().setState(DrawState.getState());
					game.handleRequest(
							new DrawPoliticCardRequestMsg(game.getTokenByPlayer(game.getGame().getCurrentPlayer())));
					game.getPublisherInterface().publish(
							new DisplayToClient(ActionQuestion.askForNextState(game.getGame().getCurrentPlayer()),
									game.getTokenByPlayer(game.getGame().getCurrentPlayer())),
							game.makeTopic());
					game.getMessageCouple().setBroadcast(null);
					game.getMessageCouple().setResponse(new DisplaySomethingResponseMsg(""));
					return DrawState.getState();
				} else {
					game.changePlayer(this.getPlayer());
					game.getTurnMachine().setState(DrawState.getState());
					game.handleRequest(
							new DrawPoliticCardRequestMsg(game.getTokenByPlayer(game.getGame().getCurrentPlayer())));
					game.getPublisherInterface().publish(
							new DisplayToClient(ActionQuestion.askForNextState(game.getGame().getCurrentPlayer()),
									game.getTokenByPlayer(game.getGame().getCurrentPlayer())),
							game.makeTopic());
					game.getMessageCouple().setBroadcast(null);
					game.getMessageCouple().setResponse(new DisplaySomethingResponseMsg(
							"Ok, your turn is over. Now wait for the other players..."));
					return DrawState.getState();

				}

			} else if (this.getPlayer().equals(game.getLastPlayer()) && game.isLastRound()) {
				return WinState.getState();
			} else {
				// LAST TURN
				game.changePlayer(this.getPlayer());
				game.handleRequest(
						new DrawPoliticCardRequestMsg(game.getTokenByPlayer(game.getGame().getCurrentPlayer())));
				return DrawState.getState();

			}
		} else {
			game.getMessageCouple().setBroadcast(null);
			game.getMessageCouple().setResponse(new InvalidActionResponseMsg(
					"You have to perform all available primary action!" + ActionQuestion.askForNextState(getPlayer())));
		}
		return DrawState.getState();
	}

}
