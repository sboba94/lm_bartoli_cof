package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.FinalAction;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

/**
 * 
 */
public class WinState extends TurnState {

	private final static WinState state = new WinState();

	public static TurnState getState() {
		return state;
	}

	@Override
	protected boolean isActionValid(PlayerAction action) {
		return (action instanceof FinalAction);
	}

}