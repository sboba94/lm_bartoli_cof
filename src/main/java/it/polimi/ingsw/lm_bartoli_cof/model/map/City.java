package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.io.Serializable;
import java.util.*;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.RewardToken;

public class City implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/////////////////////// Attributes /////////////////////////////

	private String name;
	private CityColors color;
	private RewardToken token;
	private RegionName region;
	private int emporiumNumber;

	/////////////////////// Constructor /////////////////////////////

	/** default **/
	public City(String name) {
		this.name = name;
	}

	/** complete **/
	public City(String name, CityColors color, RegionName region) {
		this.name = name;
		this.color = color;
		this.region = region;
		this.emporiumNumber = 0;
	}

	/////////////////////// Methods /////////////////////////////

	public RewardToken getToken() {
		return token;
	}

	public void setToken(LinkedList<Bonus> toAddBonuses) {
		this.token = new RewardToken(toAddBonuses);
	}

	public String getName() {
		return name;
	}

	public CityColors getColor() {
		return color;
	}

	public void setColor(CityColors color) {
		this.color = color;
	}

	public int getEmporiumNumber() {
		return emporiumNumber;
	}

	public void incrementEmporium() {
		this.emporiumNumber++;
	}

	/**
	 * this is used to initial crate of city. In RegionBoard's constructor
	 * this.region will be a real regionBoard.
	 * 
	 * @param string
	 */
	public void setRegionByEnum(RegionName region) {
		this.region = region;
	}

	public RegionName getRegionName() {

		return (RegionName) this.region;
	}

	@Override
	public String toString() {
		return this.name;
	}

}