package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.io.Serializable;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;

/**
 * This class implements a generic {@link BonusRewardTile}.
 * 
 */
public class BonusRewardTile implements Serializable {
	private static final long serialVersionUID = 1L;
	
	///////////////// Attributes /////////////////

	private final Bonus bonus;
	private final String position;

	///////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link BonusRewardTile}.
	 * 
	 * @param rewardBonus
	 *            the bonus on the {@link BonusRewardTile}
	 * @param position
	 *            where the {@link BonusRewardTile} must be placed in the
	 *            {@link GameMap}
	 */
	public BonusRewardTile(Bonus rewardBonus, String position) {
		this.bonus = rewardBonus;
		this.position = position;
	}

	///////////////// Getters and Setters /////////////////

	/**
	 * Gets the {@link Bonus} that a player gains after he has acquired a
	 * specific {@link BonusRewardTile}.
	 * 
	 * @return the {@link Bonus} that a player gains after he has acquired a
	 *         specific {@link BonusRewardTile}
	 * 
	 */
	public Bonus getBonus() {
		return bonus;
	}

	/**
	 * Gets the position on which the {@link BonusRewardTile} has been placed.
	 * 
	 * @return the position on which the {@link BonusRewardTile} has been placed
	 */
	public String getPosition() {
		return position;
	}

	///////////////// Methods /////////////////

	@Override
	public String toString() {
		return "*BONUS TILE*: Bonus = " + this.bonus + " & Position = " + this.position;
	}

}