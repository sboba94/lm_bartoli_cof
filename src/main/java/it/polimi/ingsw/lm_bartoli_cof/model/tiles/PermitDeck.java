package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.util.*;

/**
 * This class represents the {@link Deck} of {@link BusinessPermitTile}. This
 * class extends the abstract superclass {@link Deck}.
 * 
 * TODO WHAT HAPPENDS WHEN THERE ARE NO MORE TILES IN A BUSINESS PERMIT DECK?
 */
public class PermitDeck extends Deck<BusinessPermitTile> {
	private static final long serialVersionUID = 1L;
	
	///////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link Deck} of {@link BusinessPermitTile} using the
	 * constructor of the abstract superclass {@link Deck}.
	 * 
	 * @param toAddPermitTiles
	 *            the {@link LinkedList} of {@link BusinessPermitTile} to put in
	 *            the {@link BusinessPermitDeck}
	 */
	public PermitDeck(LinkedList<BusinessPermitTile> toAddPermitTiles) {
		super(toAddPermitTiles);
	}

	///////////////// Methods /////////////////

	/**
	 * Enqueues a {@link BusinessPermitTile} in the {@link Deque} of the
	 * {@link BusinessPermitDeck}.
	 *
	 * @param tile
	 *            the {@link BusinessPermitTile} to enqueue in the {@link Deque}
	 *            of the {@link BusinessPermitDeck}
	 */
	public void enqueueTile(BusinessPermitTile toEnqueTile) {
		drawPile.addLast(toEnqueTile);
	}

	/**
	 * Shuffles the the {@link Deque} of the {@link BusinessPermitDeck}.
	 * 
	 */
	public void shuffleDeck() {
		Collections.shuffle((LinkedList<BusinessPermitTile>) drawPile);
	}

	@Override
	public String toString() {
		StringBuilder string = new StringBuilder();
		string.append("Business Permit Deck: \n");
		int i = 1;
		for (BusinessPermitTile thisTile : this.drawPile) {
			string.append(i + ") " + thisTile + ";\n");
			i++;
		}

		return string.toString();

	}

}
