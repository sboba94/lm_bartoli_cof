package it.polimi.ingsw.lm_bartoli_cof.model.market;

import java.io.Serializable;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;


/**
 * This class represents the object in that a player want to sale
 * 
 */
public class ForSale implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Player owner;
	private Object object;
	private int price;

	/**
	 * This is the constructor of the object in sale
	 * 
	 * @param player,
	 *            the owner of the object
	 * @param object,
	 *            the object on sale
	 * @param price,
	 *            the number of coins that need to buy this object
	 */
	public ForSale(Player owner, Object object, int price) {
		this.owner = owner;
		this.object = object;
		this.price = price;
	}

	/**
	 * Gets the price of the object
	 * 
	 * @return
	 */
	public int getPrice() {
		return this.price;
	}

	/**
	 * Gets the object on sale
	 * 
	 * @return
	 */
	public Object getObject() {
		return this.object;
	}

	/**
	 * Gets the owner that wants to sell the object 
	 * 
	 * @return
	 */
	public Player getOwner() {
		return this.owner;
	}

}