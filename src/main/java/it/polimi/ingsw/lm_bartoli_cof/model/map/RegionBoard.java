package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.MyDirectedGraph;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

public class RegionBoard extends Board {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LinkedList<City> cities;
	private PermitBoard permitBoard;
	private RegionName regionName;

	/////////////////// Constructor //////////////////
	public RegionBoard() {
		super();
	}

	public RegionBoard(Setting config, RegionName regionName, MyDirectedGraph graph,
			LinkedList<Councillor> councillorPool) throws PermitDeckNotConfiguredException {
		super(councillorPool);
		this.cities = new LinkedList<City>();
		cities = graph.setRegionCities(regionName);
		this.permitBoard = new PermitBoard(config, regionName, this.cities);
		this.permitBoard.configurePermitBoard();
		this.regionName = regionName;
	}

	///////////////// Getter and Setter //////////////

	public LinkedList<City> getCities() {
		return cities;
	}

	public PermitBoard getPermitBoard() {
		return permitBoard;
	}

	public RegionName getRegionName() {
		return regionName;
	}

	/**
	 * This function allows you to check if a City is in this region
	 * 
	 * @param c:
	 *            the city
	 * @return true if is in, else return false
	 */
	public boolean isCityIn(City c) {
		if (cities.contains(c))
			return true;
		return false;
	}

}