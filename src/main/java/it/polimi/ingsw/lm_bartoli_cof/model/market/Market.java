package it.polimi.ingsw.lm_bartoli_cof.model.market;

import java.io.Serializable;
import java.util.LinkedList;



/**
 * This class represents the set of object on sale.
 */
public class Market implements Serializable{
	private static final long serialVersionUID = 1L;
	
    private LinkedList<ForSale> saleList;

    /**
     * This is the constructor of the class, it create an empty list that will be filled during the game
     */
    public Market(){
    	this.saleList = new LinkedList<ForSale>();
    }
     
    /**
     * Getter of saleList
	 * @return the list of object on sale
	 */
	public LinkedList<ForSale> getSaleList() {
		return saleList;
	}

	/** 
	 * Sets the saleList
	 * 
	 * @param saleList
	 */
	public void setSaleList(LinkedList<ForSale> saleList) {
		this.saleList = saleList;
	}

}