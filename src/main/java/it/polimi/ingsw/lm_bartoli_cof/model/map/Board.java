package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.io.Serializable;
import java.util.LinkedList;


import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncilBalcony;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;

/**
 * This abstract class
 */

public abstract class Board implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/////////////////////// Attributes /////////////////////////////
	
	private CouncilBalcony council;

	/////////////////////// Constructor /////////////////////////////

	/**
	 * constructor
	 */
	public Board(LinkedList<Councillor> councillorPool) {
		this.council = new CouncilBalcony(councillorPool);
	}

	/////////////////////// Getters and Setters /////////////////////////////

	public Board() {

	}

	/**
	 * Getter of the CouncilBalcony
	 * 
	 */
	public CouncilBalcony getCouncil() {
		return council;
	}

	/**
	 * Setter of CouncilBalcony
	 * 
	 */
	public void setCouncil(CouncilBalcony council) {
		this.council = council;
	}

}