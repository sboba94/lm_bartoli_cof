package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.io.Serializable;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;

/**
 * This class implements a generic {@link KingRewardTile}.
 *
 */
public class KingRewardTile implements Serializable {
	private static final long serialVersionUID = 1L;
	
	///////////////// Attributes /////////////////

	private final Bonus bonus;
	private final int position;

	///////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link KingRewardTile}.
	 *
	 * @param kingBonus
	 *            the bonus on the {@link KingRewardTile}
	 * @param position
	 *            the position of the {@link KingRewardTile} in the
	 *            {@link KingRewardsDeck}
	 */
	public KingRewardTile(Bonus kingBonus, int position) {
		this.bonus = kingBonus;
		this.position = position;
	}

	///////////////// Getters and Setters /////////////////

	/**
	 * Gets the {@link Bonus} that a player gains after he has acquired a
	 * specific {@link KingRewardTile}.
	 * 
	 * @return the {@link Bonus} that a player gains after he has acquired a
	 *         specific {@link KingRewardTile}
	 * 
	 */
	public Bonus getBonus() {
		return bonus;
	}

	/**
	 * Gets the position of the {@link KingRewardTile} in the
	 * {@link KingRewardsDeck}.
	 * 
	 * @return the position of the {@link KingRewardTile} in the
	 *         {@link KingRewardsDeck}
	 */
	public int getPosition() {
		return position;
	}

	///////////////// Methods /////////////////

	@Override
	public String toString() {
		return "*KING REWARD TILE*: Bonus = " + this.bonus + " & Position = " + this.position;
	}

}