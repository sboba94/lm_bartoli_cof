package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.util.*;

import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;

/**
 * This class represents the {@link Deck} of {@link PoliticCard}. This class
 * extends the abstract superclass {@link Deck}. When the drawPile (
 * {@link Deque}) of the {@link PoliticDeck} is empty automatically shuffles the
 * {@link ArrayList} of discarded {@link PoliticCard} and add it to the
 * {@link Deque} of drawable {@link PoliticCard}.
 * 
 */
public class PoliticDeck extends Deck<PoliticCard> {
	private static final long serialVersionUID = 1L;
	
	///////////////// Attributes /////////////////

	private final ArrayList<PoliticCard> discardPile;

	///////////////// Constructor /////////////////

	/**
	 * Instantiates the {@link Deque} of drawable {@link PoliticCard} using the
	 * constructor of the abstract superclass {@link Deck} and an
	 * {@link ArrayList} of discarded {@link PoliticCard}.
	 * 
	 * @param toAddPoliticCards
	 *            the {@link LinkedList} of {@link PoliticCard} to put in the
	 *            {@link PoliticDeck}
	 * 
	 */
	public PoliticDeck(LinkedList<PoliticCard> toAddPoliticCards) {
		super(toAddPoliticCards);
		this.discardPile = new ArrayList<>();
	}

	///////////////// Methods /////////////////

	/**
	 * Draws a {@link PoliticCard} by removing it from the {@link Deque} of
	 * drawable {@link PoliticCard}. If the {@link PoliticDeck} is empty it
	 * checks if there are cards in the {@link ArrayList} of discarded
	 * {@link PoliticCard}, if this is empty too then there are no more
	 * {@link PoliticCard} in the game and it will throws a
	 * {@link NoMorePoliticCardsException}, otherwise {@link ArrayList} of
	 * discarded {@link PoliticCard} becomes the new {@link Deque} and the
	 * {@link PoliticDeck} is automatically shuffled.
	 * 
	 * @return the {@link PoliticCard} drawn from the {@link PoliticDeck}
	 * @throws NoMorePoliticCardsException
	 * 
	 */
	@Override
	public PoliticCard drawCard() throws NoMorePoliticCardsException {

		if (this.isAllEmpty()) {
			// No more cards in the deck AND in the discardPile
			throw new NoMorePoliticCardsException("No more politic cards in this game!");
		} else {
			if (this.isDrawPileEmpty()) {
				// Move the cards from the discardPile into the empty deck.
				drawPile.addAll(discardPile);
				discardPile.clear();
				// Shuffle the deck
				this.shuffleDeck();
				// Pop a card and return it
				return drawPile.remove();
			} else {
				return drawPile.remove();
			}
		}

	}

	/**
	 * Adds a {@link PoliticCard} to the {@link ArrayList} of discarded
	 * {@link PoliticCard}.
	 *
	 * @param card
	 *            the {@link PoliticCard} to add to the {@link ArrayList} of
	 *            discarded {@link PoliticCard}
	 */
	public void discardCard(PoliticCard card) {
		discardPile.add(card);
	}

	/**
	 * Checks if the {@link Deque} of drawable {@link PoliticCard} and the
	 * {@link ArrayList} of discarded {@link PoliticCard} are both empty.
	 *
	 * @return true if the {@link Deque} of drawable {@link PoliticCard} and the
	 *         {@link ArrayList} of discarded {@link PoliticCard} are both empty
	 */
	public boolean isAllEmpty() {
		return drawPile.isEmpty() && discardPile.isEmpty();
	}

	/**
	 * Shuffles the the {@link Deque} of the {@link PoliticDeck}.
	 */
	public void shuffleDeck() {
		Collections.shuffle((LinkedList<PoliticCard>) drawPile);
	}

}
