package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.io.Serializable;
import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.model.tiles.*;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

/**
 * This class contains a Business Permit Deck and two Business Permit Tiles,
 * which are usable for a player according to the action that he's performing.
 * 
 */

public class PermitBoard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	///////////////// Attributes /////////////////

	private RegionName region;
	private BusinessPermitTile usablePermitTile1;
	private BusinessPermitTile usablePermitTile2;
	private PermitDeck unusablePermitDeck;

	///////////////// Constructor /////////////////
	/**
	 * The constructor of this class ONLY generates the deck of Business Permit
	 * Tiles from the configuration file. Notice that the deck is not shuffled
	 * after his creation and the two usable Permit Tiles are not set yet.
	 * @throws PermitDeckNotConfiguredException 
	 * 
	 */
	public PermitBoard(Setting config, RegionName name, LinkedList<City> cities) throws PermitDeckNotConfiguredException {
		this.unusablePermitDeck = new PermitDeck(config.generatePermitDeck(name, cities));
		region = name;
	}

	///////////////// Getters and Setters /////////////////
	/**
	 * This method return the Business Permit Tile asked by a player and
	 * replaces it with a new Permit Tile from the deck.
	 * 
	 * @return toReturnPermitTile - the tile asked by a player
	 * 
	 */
	public BusinessPermitTile getUsablePermitTile1() {
		BusinessPermitTile toReturnPermitTile = this.usablePermitTile1;
		try {
			this.usablePermitTile1 = this.unusablePermitDeck.drawCard();
		} catch (Exception e) {
			e.getMessage();
		}
		return toReturnPermitTile;
	}

	/**
	 * This method return the Business Permit Tile asked by a player and
	 * replaces it with a new Permit Tile from the deck.
	 * 
	 * @return toReturnPermitTile - the tile asked by a player
	 * 
	 */
	public BusinessPermitTile getUsablePermitTile2() {
		BusinessPermitTile toReturnPermitTile = this.usablePermitTile2;
		try {
			this.usablePermitTile2 = this.unusablePermitDeck.drawCard();
		} catch (Exception e) {
			e.getMessage();
		}
		return toReturnPermitTile;
	}

	///////////////// Methods /////////////////
	/**
	 * This method shows the Business Permit Tile asked by a player without move
	 * it.
	 * 
	 * @return toReturnPermitTile - the tile asked by a player
	 * 
	 */
	public BusinessPermitTile showUsablePermitTile1() {
		return this.usablePermitTile1;
	}

	/**
	 * This method shows the Business Permit Tile asked by a player without move
	 * it.
	 * 
	 * @return toReturnPermitTile - the tile asked by a player
	 * 
	 */
	public BusinessPermitTile showUsablePermitTile2() {
		return this.usablePermitTile2;
	}

	/**
	 * This method shuffles the Business Permit Deck generated in the
	 * constructor and AFTER that draws the two usable Business Permit Tiles.
	 * 
	 */
	public void configurePermitBoard() {
		this.unusablePermitDeck.shuffleDeck();

		try {
			this.usablePermitTile1 = this.unusablePermitDeck.drawCard();
		} catch (Exception e) {
			e.getMessage();
		}

		try {
			this.usablePermitTile2 = this.unusablePermitDeck.drawCard();
		} catch (Exception e) {
			e.getMessage();
		}

	}

	/**
	 * This method enqueues the two Business Permit Tiles of a Permit Board and
	 * replaces those with new Permit Tiles drawn from the deck.
	 * 
	 */
	public void changeUsableTiles() {
		this.unusablePermitDeck.enqueueTile(this.usablePermitTile1);
		this.unusablePermitDeck.enqueueTile(this.usablePermitTile2);
		try {
			this.usablePermitTile1 = this.unusablePermitDeck.drawCard();
		} catch (Exception e) {
			e.getMessage();
		}

		try {
			this.usablePermitTile2 = this.unusablePermitDeck.drawCard();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public String getRegionName() {
		return this.region.toString();
	}

}
