package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.util.HashMap;
import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.KingRewardTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.KingRewardsDeck;

/**
 * This class needs to instance the king reward deck and the nobility track
 */
public class KingBoard extends Board {

	////////////////////////// Attributes /////////////////////////////

	private static final long serialVersionUID = 1L;
	private KingRewardsDeck kingRewards;
	private NobilityTrack track;

	////////////////////////// Constructor /////////////////////////////

	/**
	 * Constructor of King Board
	 * 
	 * @param config,
	 *            take info from configuration (xml) file
	 * @param councillorPool,
	 *            the pool of the councillor present in game
	 */
	public KingBoard(LinkedList<KingRewardTile> kingRewardCards, LinkedList<Councillor> councillorPool,
			HashMap<Integer, LinkedList<Bonus>> nobilityTrack) {
		super(councillorPool);
		this.kingRewards = new KingRewardsDeck(kingRewardCards);
		this.track = new NobilityTrack();
		this.track.setBonuses(nobilityTrack);
	}

	////////////////////////// Getter and Setter/////////////////////////

	/**
	 * This is the getter of NobilityTrack
	 * 
	 * @return NobilityTrack
	 */
	public NobilityTrack getTrack() {
		return track;
	}

	/**
	 * This is the getter of the king reward deck
	 * 
	 * @return the King reward deck
	 */
	public KingRewardsDeck getKingRewards() {
		return kingRewards;
	}

}