package it.polimi.ingsw.lm_bartoli_cof.model.map;

/**
 * This enumeration contains the different colors that a City or a
 * BonusRewarTile can assume
 */
public enum CityColors {
	PURPLE, BRONZE, GOLD, SILVER, IRON
}