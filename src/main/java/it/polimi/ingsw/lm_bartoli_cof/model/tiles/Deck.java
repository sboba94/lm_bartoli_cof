package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.io.Serializable;
import java.util.*;

import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;

/**
 * This class models and manages a {@link Deck} of linked elements. This class
 * implements a drawPile from which it's possible to draw cards (or tiles) and
 * to enqueue cards (or tiles), depending on the specific type of deck in the
 * game.
 *
 * @param <E>
 *            the type of elements in the {@link Deck}
 */
public abstract class Deck<E> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	///////////////// Attributes /////////////////

	// The Deque Interface is usually used for decks implementation
	protected Deque<E> drawPile;

	///////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link Deck} populated with a {@link LinkedList} of
	 * generic elements generated in {@link Setting} class. The cards (or tiles)
	 * are not shuffled when the deck is created.
	 *
	 * @param cards
	 *            the {@link LinkedList} from {@link Setting} class
	 */
	public Deck(LinkedList<E> cards) {
		drawPile = new LinkedList<E>();
		drawPile.addAll(cards);
	}

	///////////////// Getters and Setters /////////////////

	/**
	 * Gets the {@link Deque} of cards (or tiles) in the deck.
	 * 
	 * @return the {@link Deque} of cards (or tiles) in the deck
	 */

	public Deque<E> getDrawPile() {
		return drawPile;
	}

	///////////////// Methods /////////////////
	/**
	 * Draws a card (or a tile) by removing it from the {@link Deque}. If there
	 * are no more cards (or tiles) this method returns {@link null}.
	 * 
	 * @return the drawn Card (or Tile)
	 * @throws NoMorePoliticCardsException
	 * 
	 */
	public E drawCard() throws NoMorePoliticCardsException {
		if (this.isDrawPileEmpty()) {
			return null;
		} else
			return drawPile.remove();
	}

	/**
	 * Checks if the {@link Deque} of cards (or tiles) of the {@link Deck} is
	 * empty.
	 *
	 * @return true if the {@link Deque} of cards (or tiles) of the {@link Deck}
	 *         is empty
	 */
	public boolean isDrawPileEmpty() {
		return drawPile.isEmpty();
	}

}
