package it.polimi.ingsw.lm_bartoli_cof.model.council;

import java.io.Serializable;
import java.security.InvalidParameterException;

/**
 * This class implements a generic {@link Councillor}.
 * 
 */
public class Councillor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//////////////////// Attributes ////////////////////

	private final CouncillorColors color;

	/////////////////// Constructor ////////////////////////

	/**
	 * Instantiates a new {@link Councillor}.
	 * 
	 * @param color
	 *            the color of the new {@link Councillor}
	 * @throws InvalidParameterException
	 *             if the parameter is MULTICOLOR
	 */
	public Councillor(CouncillorColors color) {
		if (color.equals(CouncillorColors.MULTICOLOR)) {
			throw new InvalidParameterException();
		}
		this.color = color;
	}

	/////////////////// Getters and Setters //////////////////

	/**
	 * Gets the color of a {@link Councillor}.
	 * 
	 * @return the color of a {@link Councillor}
	 */
	public CouncillorColors getColor() {
		return color;
	}

	//////////////////// Other Methods ////////////////////

	@Override
	public String toString() {
		return this.color.toString();
	}

}