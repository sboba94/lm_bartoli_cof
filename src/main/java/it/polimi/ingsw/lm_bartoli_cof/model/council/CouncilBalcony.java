package it.polimi.ingsw.lm_bartoli_cof.model.council;

import java.io.Serializable;
import java.util.LinkedList;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * This class implements a generic {@link CouncilBalcony}. This class implements
 * a {@link LinkedBlockingQueue} of {@link Councillor} because has a fixed
 * dimension.
 * 
 */
public class CouncilBalcony implements Serializable{

	private static final long serialVersionUID = 1L;
	
	//////////////////// Attributes ////////////////////

	public static final int COUNCIL_SIZE = 4;

	// We use the LinkedBlockingQueue because we want a Collection with a
	// fixed dimension, we know that this type of BlockingQueue is thread safe
	// and so this can impact on the performance of our project.
	private LinkedBlockingQueue<Councillor> council;

	//////////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link CouncilBalcony} from a {@link LinkedList} of
	 * {@link Councillor} that represents the "pool" in which can be chosen the
	 * four {@link Councillor} to add.
	 * 
	 * @param councillorPool
	 *            the pool of {@link Councillor}
	 */
	public CouncilBalcony(LinkedList<Councillor> councillorPool) {
		council = new LinkedBlockingQueue<Councillor>(COUNCIL_SIZE);
		this.electCouncil(councillorPool);
	}

	//////////////////// Getters And Setters ////////////////////

	/**
	 * Gets the {@link LinkedBlockingQueue} of {@link Councillor} in the
	 * {@link CouncilBalcony}.
	 *
	 * @return the {@link LinkedBlockingQueue} of {@link Councillor} in the
	 *         {@link CouncilBalcony}
	 */
	public LinkedBlockingQueue<Councillor> getCouncil() {
		return council;
	}

	//////////////////// Methods ////////////////////

	/**
	 * This private method is used by the constructor for elect the
	 * {@link CouncilBalcony} at the beginning a new game. This method
	 * randomically choose four elements from the "pool" of {@link Councillor}
	 * to elect them in the {@link CouncilBalcony}.
	 * 
	 * @param councillorPool
	 *            the pool of {@link Councillor}
	 * @return the pool without the elected {@link Councillor}
	 * 
	 */
	private LinkedList<Councillor> electCouncil(LinkedList<Councillor> councillorPool) {
		for (int j = 0; j < COUNCIL_SIZE; j++) {
			// randomically chosen with Math.random()
			int index = (int) (Math.random() * councillorPool.size());
			council.add(councillorPool.remove(index));
		}
		return councillorPool;
	}

	/**
	 * This public method is used in the Game for elect a new Councillor in the
	 * Council Balcony. We use a {@link LinkedBlockingQueue} so refer to its
	 * documentation for the specification of the methods remove() and add().
	 * 
	 * @return removedCouncillor the Councillors that as been removed from the
	 *         CouncilBalcony
	 */
	public Councillor electCouncillor(Councillor toAddCouncillor) {
		Councillor removedCouncillor = council.remove();
		council.add(toAddCouncillor);
		return removedCouncillor;
	}

	/**
	 * This public method checks if the {@link List} of Politic Cards that a
	 * player want to use for satisfy a Council Balcony is valid or not.
	 * 
	 * @param politicCardColors
	 *            the colors of the Politic Cards that the player wants to use
	 * @return founded - a boolean that is "true" if every Politic Card color
	 *         has a correspondence in the Council Balcony, false otherwise
	 */
	public boolean containColors(LinkedList<CouncillorColors> politicCardColors) {
		LinkedBlockingQueue<Councillor> councilCopy = new LinkedBlockingQueue<Councillor>() ;
		councilCopy.addAll(this.council);
		boolean founded = false;
		for (CouncillorColors color : politicCardColors) {
			founded = false;
			for (Councillor councillor : councilCopy) {
				if (color.equals(councillor.getColor())) {
					councilCopy.remove(councillor);
					founded = true;
					break;
				}
				else if(color.equals(CouncillorColors.MULTICOLOR)){
					founded = true;
				}
			}
			if (!founded)
				return founded;
		}
		return founded;
	}
}