package it.polimi.ingsw.lm_bartoli_cof.model.map;

/**
 * 
 */
public enum RegionName {

	COAST, HILLS, MOUNTAIN;

}
