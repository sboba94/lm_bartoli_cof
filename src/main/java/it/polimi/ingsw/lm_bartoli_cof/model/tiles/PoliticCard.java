package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.io.Serializable;

import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;

/**
 * This class implements a generic {@link PoliticCard}.
 *
 */
public class PoliticCard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	///////////////// Attributes /////////////////

	private CouncillorColors color;

	///////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link PoliticCard}.
	 * 
	 */
	public PoliticCard() {
		super();
	}

	/**
	 * Instantiates a new {@link PoliticCard}.
	 *
	 * @param color
	 *            the color of the new {@link PoliticCard}
	 * 
	 */
	public PoliticCard(CouncillorColors color) {
		this.color = color;
	}

	///////////////// Getters and Setters /////////////////

	/**
	 * Gets the color of a {@link PoliticCard}.
	 * 
	 * @return the color of a {@link PoliticCard}
	 */
	public CouncillorColors getPoliticCardColor() {
		return color;
	}

	///////////////// Methods /////////////////

	@Override
	public String toString() {
		return this.color.name();
	}
}