package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.io.Serializable;

/**
 * This class represents the king in the game, it is use also to set and get the
 * king position
 */
public class King implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * This attribute represents the position of the king
	 */
	private City city;

	/**
	 * Default constructor for King
	 */
	public King() {

	}

	/**
	 * Getter of the position of the king
	 * 
	 * @return the city where the king is
	 */
	public City getCity() {
		return this.city;
	}

	/**
	 * Setter of the position of the king
	 * 
	 * @param city,
	 *            where the king must go
	 */
	public void setCity(City city) {
		this.city = city;
	}

}