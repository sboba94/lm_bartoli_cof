package it.polimi.ingsw.lm_bartoli_cof.model.council;

/**
 * This class enumerates the possible colors of a Councillor or a Politic Card,
 * for each color it's possible to get the name of it by the method getColorName().
 * 
 */
public enum CouncillorColors {
	
	PINK("Pink"), 
	WHITE("White"), 
	ORANGE("Orange"), 
	BLACK("Black"), 
	BLUE("Blue"), 
	PURPLE("Purple"), 
	MULTICOLOR("Multicolor");

	private final String colorName;

	private CouncillorColors(String colorName) {
		this.colorName = colorName;
	}

	/**
     * Gets the name of the color.
     *
     * @return colorName - the name of the color
     */
	@Override
    public String toString() {
        return colorName;
    }

}