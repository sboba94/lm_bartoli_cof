package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.util.LinkedList;

/**
 * This class represents the {@link Deck} of {@link KingRewardTile}. When the
 * {@link Deque} of the {@link KingRewardsDeck} is empty no more players can
 * take this type of tiles. This class extends the abstract superclass
 * {@link Deck}.
 * 
 */
public class KingRewardsDeck extends Deck<KingRewardTile> {
	private static final long serialVersionUID = 1L;
	
	///////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link Deck} of {@link KingRewardTile} using the
	 * constructor of the abstract superclass {@link Deck}.
	 * 
	 * @param toAddKingTiles
	 *            the {@link LinkedList} of {@link KingRewardTile} to put in the
	 *            {@link KingRewardsDeck}
	 */
	public KingRewardsDeck(LinkedList<KingRewardTile> toAddKingTiles) {
		super(toAddKingTiles);
	}

}
