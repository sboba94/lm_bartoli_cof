package it.polimi.ingsw.lm_bartoli_cof.model;

import java.io.Serializable;
import java.util.*;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.CityColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionBoard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.*;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.MyDirectedGraph;

/**
 * This class represent a generic Player.
 *
 */
public class Player implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//////////////////// Attributes ////////////////////
	
	private String nickname;
	private int playerID;
	
	private boolean connect;

	private LinkedList<Bonus> gainedBonuses;
	private LinkedList<PoliticCard> politicCards;
	private LinkedList<BusinessPermitTile> usablePermitTiles;
	private LinkedList<BusinessPermitTile> usedPermitTiles;
	private LinkedList<City> emporiums;

	private int assistant;
	private int victoryPos;
	private int nobilityPos;
	private int coins;
	private int numberOfPrimaryAction;

	private boolean sellExecute;
	private boolean quickActionExecuted;

	//////////////////// Contructor ////////////////////

	/**
	 * This constructor have to get the id, the number of assistant and the
	 * initial coins of each player from controller. It is necessary because
	 * they depends on the player's number!
	 * 
	 * @param identity
	 *            player's identification number
	 * @param defaultAssistants
	 *            the starter number of Assistants
	 * @param defaultCoins
	 *            the starter number of Coins
	 */
	public Player(int identity, int defaultAssistants, int defaultCoins, String nickname) {

		this.nickname = nickname;
		this.playerID = identity;
		
		this.connect=true;

		this.nobilityPos = 0;
		this.victoryPos = 0;
		this.assistant = defaultAssistants;
		this.coins = defaultCoins;

		this.politicCards = new LinkedList<PoliticCard>();
		this.emporiums = new LinkedList<City>();
		this.usablePermitTiles = new LinkedList<BusinessPermitTile>();
		this.usedPermitTiles = new LinkedList<BusinessPermitTile>();
		this.gainedBonuses = new LinkedList<Bonus>();

		this.numberOfPrimaryAction = 1;
		this.quickActionExecuted = false;
		this.sellExecute = false;

	}

	//////////////////// Methods ////////////////////

	public String getNickname() {
		return this.nickname;
	}

	/**
	 * @return the connect
	 */
	public boolean isConnect() {
		return connect;
	}

	/**
	 * @param connect the connect to set
	 */
	public void disconnect() {
		this.connect = false;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getId() {
		return playerID;
	}

	// ********************* BONUSES ********************* //

	public LinkedList<Bonus> getBonus() {
		return gainedBonuses;
	}

	public void addBonus(Bonus bonus) {
		this.gainedBonuses.add(bonus);
	}

	// ********************* POLITIC ********************* //

	public List<PoliticCard> getPoliticCards() {
		return politicCards;
	}

	public void addPoliticCard(PoliticCard card) {
		this.politicCards.add(card);
	}

	// ********************* USABLE BPT ********************* //

	public List<BusinessPermitTile> getUsablePermitTiles() {
		return this.usablePermitTiles;
	}

	public void addPermitTile(BusinessPermitTile card) {
		this.usablePermitTiles.add(card);
	}

	// ********************* USED BPT ********************* //

	public List<BusinessPermitTile> getUsedPermitTiles() {
		return this.usedPermitTiles;
	}

	// ********************* EMPORIUMS ********************* //

	public LinkedList<City> getEmporiums() {
		return emporiums;
	}

	/**
	 * Checks if a player has built an emporium a in a specified city
	 * 
	 */
	public boolean hasEmporiumIn(City c) {
		if (emporiums.contains(c))
			return true;
		return false;
	}

	/**
	 * Allows to build an emporium, controls are already done
	 * 
	 */
	public void buildEmporium(City city) {
		this.emporiums.add(city);
	}

	// ********************* ASSISTANTS ********************* //

	public int getAssistantNumber() {
		return assistant;
	}

	public void addAssistants(int value) {
		this.assistant += value;
	}

	public void removeAssistants(int toRemove) {
		if (this.assistant < toRemove) {
			throw new IllegalArgumentException("Not enough assistants");
		} else {
			this.assistant = this.assistant - toRemove;
		}
	}

	// ********************* VICTORYPOS ********************* //

	public int getVictoryPos() {
		return this.victoryPos;
	}

	public void addVictoryPos(int victoryPos) {
		this.victoryPos += victoryPos;
	}

	// ********************* NOBILITYPOS ********************* //

	public int getNobilityPos() {
		return this.nobilityPos;
	}

	public void setNobilityPos(int newPosition) {
		this.nobilityPos = newPosition;
	}

	// ********************* COINS ********************* //

	public int getCoinsNumber() {
		return coins;
	}

	public void addAssistant() {
		this.assistant++;
	}

	public void addCoins(int value) throws IllegalArgumentException {
		if (value >= 0)
			this.coins += value;
		else
			throw new IllegalArgumentException("Negative value received");
	}

	public void removeCoins(int value) throws IllegalArgumentException {
		if (value < 0) {
			throw new IllegalArgumentException("Negative value received");
		} else if (this.coins < value) {
			throw new IllegalArgumentException("No more coins");
		} else {
			this.coins -= value;
		}
	}

	// ********************* ACTIONS ********************* //

	public int getNumberOfPrimaryAction() {
		return numberOfPrimaryAction;
	}

	public void addNumberOfPrimaryAction(int numberOfPrimaryAction) {
		this.numberOfPrimaryAction += numberOfPrimaryAction;
	}

	public void resetNumberOfPrimaryAction() {
		this.numberOfPrimaryAction = 1;
	}

	public boolean isQuickActionExecuted() {
		return this.quickActionExecuted;
	}

	public void setQuickActionExecuted(boolean quickActionExecuted) {
		this.quickActionExecuted = quickActionExecuted;
	}

	// ********************* UTILS ********************* //

	/**
	 * Checks if a player has built an emporium in every single city of a
	 * specified region
	 * 
	 */
	public boolean hasAllCitiesByRegion(RegionBoard region) {
		Iterator<City> iter = region.getCities().iterator();
		while (iter.hasNext()) {
			if (!this.hasEmporiumIn(iter.next())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if a player has built an emporium in every single city of a
	 * specified color
	 */
	public boolean hasAllCitiesByColor(MyDirectedGraph graph, CityColors color) {
		Iterator<City> iter = graph.vertexSet().iterator();
		while (iter.hasNext()) {
			City aux = iter.next();
			if (!(this.hasEmporiumIn(aux)) && aux.getColor().equals(color)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the sublist of politic cards in the hand of a player
	 * corresponding to the string of colors received
	 * 
	 */
	public synchronized LinkedList<PoliticCard> getPoliticCardsByColors(String[] colors) {
		LinkedList<PoliticCard> copy = this.politicCards;
		LinkedList<PoliticCard> toReturn = new LinkedList<PoliticCard>();
		for (int i = 0; i < colors.length; i++) {
			Iterator<PoliticCard> iter = copy.iterator();
			while (iter.hasNext()) {
				PoliticCard aux = iter.next();
				if (aux.getPoliticCardColor().toString().equals(colors[i]) && !toReturn.contains(aux)) {
					toReturn.add(aux);
					break;
				}

			}
		}
		return toReturn;
	}

	public boolean isSellExecute() {
		return sellExecute;
	}

	public void setSellExecute(boolean sellExecute) {
		this.sellExecute = sellExecute;
	}

	public void setNumberOfPrimaryAction(int numberOfPrimaryAction) {
		this.numberOfPrimaryAction=numberOfPrimaryAction;
		
	}

}