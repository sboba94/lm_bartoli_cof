package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.io.Serializable;
import java.util.*;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;

/**
 * This class implements a generic {@link RewardToken}.
 *
 */
public class RewardToken implements Serializable {
	private static final long serialVersionUID = 1L;

	///////////////// Attributes /////////////////

	private final LinkedList<Bonus> tokenBonuses;

	///////////////// Constructor /////////////////

	/**
	 * Instantiates a {@link RewardToken}.
	 *
	 * @param tokenBonuses
	 *            the {@link LinkedList} of {@link Bonus} on a specific
	 *            {@link RewardToken}
	 */
	public RewardToken(LinkedList<Bonus> tokenBonuses) {
		this.tokenBonuses = tokenBonuses;
	}

	///////////////// Getters and Setters /////////////////

	/**
	 * Gets the {@link LinkedList} of {@link Bonus} that a {@link Player} gains
	 * after he has acquired a specific {@link RewardToken}.
	 * 
	 * @return the {@link LinkedList} of {@link Bonus} that a {@link Player}
	 *         gains after he has acquired a specific {@link RewardToken}
	 * 
	 */
	public LinkedList<Bonus> getBonuses() {
		return (LinkedList<Bonus>) tokenBonuses;
	}

	///////////////// Methods /////////////////

	@Override
	public String toString() {
		return "*REWARD TOKEN*: Bonuses = " + this.tokenBonuses;
	}
}