package it.polimi.ingsw.lm_bartoli_cof.model;

import java.util.*;

import it.polimi.ingsw.lm_bartoli_cof.model.map.GameMap;
import it.polimi.ingsw.lm_bartoli_cof.model.market.Market;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.*;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NobilityTrackNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PoliticDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

/**
 * This class represents the complete status of a single Game, from here it is
 * possible to access the whole model of the project.
 * 
 * @author sboba
 */

public class Game {

	private int id;
	private GameMap map;
	private int numberOfCityToWin;
	private PoliticDeck politicDeck;
	private List<Player> players;
	private Market market;

	private Player currentPlayer;

	public Game(int idGame, Setting config) throws PoliticDeckNotConfiguredException,
			CouncillorPoolNotConfiguredException, PermitDeckNotConfiguredException, KingDeckNotConfiguredException, NobilityTrackNotConfiguredException, BonusTilesNotConfiguredException {
		this.id = idGame;
		this.market = new Market();
		this.map = new GameMap(config);
		this.players = new LinkedList<Player>();
		this.politicDeck = new PoliticDeck(config.generatePoliticDeck());
		this.numberOfCityToWin = 10;
		this.currentPlayer = null;
	}

	public int getNumberOfCityToWin() {
		return this.numberOfCityToWin;
	}

	public int getId() {
		return id;
	}

	public GameMap getGameMap() {
		return map;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public PoliticDeck getPoliticDeck() {
		return politicDeck;
	}

	public void setPoliticDeck(PoliticDeck politicDeck) {
		this.politicDeck = politicDeck;
	}

	public void addNewPlayer(Player player) {
		this.players.add(player);
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public Player giveMeLastPlayer(Player player) {
		int indexOfCurrentPlayer = this.players.indexOf(player);
		int indexOfLastPlayer;
		if (indexOfCurrentPlayer == 0) {
			indexOfLastPlayer = this.players.size() - 1;
		} else {
			indexOfLastPlayer = indexOfCurrentPlayer - 1;
		}
		return this.players.get(indexOfLastPlayer);
	}

	public LinkedList<Player> firstOnNobiltyTrack() {
		// possono esserci più giocatori primi a parimerito
		LinkedList<Player> first = new LinkedList<Player>();
		Iterator<Player> i = this.players.iterator();
		first.add(i.next());
		while (i.hasNext()) {
			Player p = i.next();
			if (p.getVictoryPos() > first.getFirst().getVictoryPos()) {
				first = new LinkedList<Player>();
				first.add(p);
			} else if (p.getVictoryPos() == first.getFirst().getVictoryPos()) {
				first.add(p);
			}
		}
		return first;
	}

	public LinkedList<Player> secondOnNobiltyTrack() {
		// possono esserci più giocatori secondi a parimerito
		LinkedList<Player> second = new LinkedList<Player>();
		Iterator<Player> i = this.players.iterator();
		second.add(i.next());
		while (i.hasNext()) {
			Player p = i.next();
			if (p.getVictoryPos() < firstOnNobiltyTrack().getFirst().getVictoryPos()) {
				if (p.getVictoryPos() > second.getFirst().getVictoryPos()) {
					second = new LinkedList<Player>();
					second.add(p);
				} else if (p.getVictoryPos() == second.getFirst().getVictoryPos()) {
					second.add(p);
				}
			}
		}
		return second;
	}

	public Player ownerWithTheGreaterNumberOfPermitCards() {
		Player greater = null;
		Iterator<Player> i = this.players.iterator();
		greater = i.next();
		while (i.hasNext()) {
			Player p = i.next();
			if (p.getUsablePermitTiles().size() + p.getUsedPermitTiles().size() > greater.getUsablePermitTiles().size()
					+ greater.getUsedPermitTiles().size()) {
				greater = null;
				greater = p;
			} else if (p.getUsablePermitTiles().size()
					+ p.getUsedPermitTiles().size() == greater.getUsablePermitTiles().size()
							+ greater.getUsedPermitTiles().size()) {
				if (p.getAssistantNumber() + p.getPoliticCards().size() > greater.getAssistantNumber()
						+ greater.getPoliticCards().size()) {
					greater = null;
					greater = p;
				}
			}
		}
		return greater;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int playerNumber) {
		for (Player p : this.players) {
			if (p.getId() == playerNumber)
				this.currentPlayer = p;
		}
	}

	public boolean everyoneHasExecuteSellAction() {
		for (Player p : this.players)
			if (!p.isSellExecute())
				return false;
		return true;
	}

	public void resetSellExecute() {
		for (Player p : this.players)
			p.setSellExecute(false);
	}

}