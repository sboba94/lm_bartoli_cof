package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.util.*;
import org.jgrapht.graph.DefaultEdge;

import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.*;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NobilityTrackNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.MyDirectedGraph;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

/**
 * This class define a map in the game. It contains three game's boards, a king
 * and a graph of the cities
 */
public class GameMap {

	private KingBoard kingBoard;
	private RegionBoard coast;
	private RegionBoard hill;
	private RegionBoard mountain;
	private King king;
	public LinkedList<Councillor> councillorPool;
	private List<BonusRewardTile> bonusTiles;

	/**
	 * This graph allows you to keep the connection within all cities
	 */
	private MyDirectedGraph cities;

	/**
	 * Default constructor
	 * 
	 * @throws CouncillorPoolNotConfiguredException
	 * @throws PermitDeckNotConfiguredException
	 * @throws NobilityTrackNotConfiguredException
	 * @throws KingDeckNotConfiguredException
	 * @throws BonusTilesNotConfiguredException
	 */
	public GameMap(Setting config) throws CouncillorPoolNotConfiguredException, PermitDeckNotConfiguredException,
			KingDeckNotConfiguredException, NobilityTrackNotConfiguredException, BonusTilesNotConfiguredException {

		this.cities = new MyDirectedGraph(DefaultEdge.class);
		this.cities.fillGraph(config);
		this.councillorPool = new LinkedList<Councillor>();
		this.councillorPool = config.generateCouncillorPool();
		this.coast = new RegionBoard(config, RegionName.COAST, cities, councillorPool);
		this.hill = new RegionBoard(config, RegionName.HILLS, cities, councillorPool);
		this.mountain = new RegionBoard(config, RegionName.MOUNTAIN, cities, councillorPool);
		this.king = new King();
		this.king.setCity(cities.getPurpleCity());
		this.kingBoard = new KingBoard(config.generateKingRewardsDeck(), councillorPool,
				config.generateNobilityTrack());
		this.bonusTiles = new LinkedList<BonusRewardTile>(config.generateBonusTiles());
		bonusTiles.addAll(config.generateBonusTiles());

	}

	// GETTERS AND SETTERS//

	public LinkedList<Councillor> getCouncillorPool() {
		return councillorPool;
	}

	public KingBoard getKingBoard() {
		return kingBoard;
	}

	public void setKingBoard(KingBoard kingBoard) {
		this.kingBoard = kingBoard;
	}

	public RegionBoard getCoast() {
		return coast;
	}

	public void setCoast(RegionBoard coast) {
		this.coast = coast;
	}

	public RegionBoard getHill() {
		return hill;
	}

	public void setHill(RegionBoard hill) {
		this.hill = hill;
	}

	public RegionBoard getMountain() {
		return mountain;
	}

	public void setMountain(RegionBoard mountain) {
		this.mountain = mountain;
	}

	public King getKing() {
		return king;
	}

	public void setKing(King king) {
		this.king = king;
	}

	public MyDirectedGraph getCities() {
		return cities;
	}

	public void setCities(MyDirectedGraph cities) {
		this.cities = cities;
	}

	public void addConnection(City city1, City city2) {
		cities.addEdge(city1, city2);
	}

	public void addCity(City city) {
		cities.addVertex(city);
	}

	public RegionBoard getRealRegion(RegionName regionName) {
		RegionBoard regionBoard = new RegionBoard();
		if (regionName.equals(RegionName.COAST))
			regionBoard = this.getCoast();
		else if (regionName.equals(RegionName.HILLS))
			regionBoard = this.getCoast();
		else if (regionName.equals(RegionName.MOUNTAIN))
			regionBoard = this.getCoast();
		return regionBoard;
	}

	public BonusRewardTile getBonusTileByColor(String color) {
		for (BonusRewardTile bonus : this.bonusTiles) {
			if (bonus.getPosition().equals(color))
				return bonus;
		}
		return null;
	}

	public BonusRewardTile getBonusTileByPosition(RegionName nameRegion) {
		for (BonusRewardTile bonus : bonusTiles) {
			if (bonus.getPosition().equals(nameRegion.name()))
				return bonus;
		}
		return null;
	}

	public List<BonusRewardTile> getBonusTiles() {
		return bonusTiles;
	}

	public void setCouncillorPool(LinkedList<Councillor> councillorPool) {
		this.councillorPool = councillorPool;
	}

	public Councillor getCouncillorByColor(CouncillorColors color) {
		for (Councillor c : this.councillorPool)
			if (c.getColor().equals(color))
				return c;
		return null;
	}

}
