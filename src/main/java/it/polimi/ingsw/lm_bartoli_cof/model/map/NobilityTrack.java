package it.polimi.ingsw.lm_bartoli_cof.model.map;

import java.io.Serializable;
import java.util.HashMap;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;

public class NobilityTrack implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/////////////////////// Attributes //////////////////////////

	private HashMap<Integer, LinkedList<Bonus>> bonuses;

	/////////////////////// Constructor //////////////////////////

	public NobilityTrack() {
		this.bonuses = new HashMap<Integer, LinkedList<Bonus>>();
	}

	/////////////////////// Getters and Setters //////////////////////////

	/**
	 * Getter of Nobility Track
	 * 
	 * @return Nobility Track
	 */
	public HashMap<Integer, LinkedList<Bonus>> getBonuses() {
		return this.bonuses;
	}

	/**
	 * Setter of NobilityTrack
	 * 
	 * @param bonuses
	 */
	public void setBonuses(HashMap<Integer, LinkedList<Bonus>> bonuses) {
		this.bonuses = bonuses;
	}

	///////////////////////// Other Method//////////////////////////////

	/**
	 * This method add a bonus at the selected key
	 * 
	 * @param bonus
	 * @param key
	 * @return the updated HashMap<Integer, LinkedList<Bonus>>
	 */
	public HashMap<Integer, LinkedList<Bonus>> addBonusAtKey(Bonus bonus, int key) {
		LinkedList<Bonus> list = new LinkedList<Bonus>();
		if (this.bonuses.containsKey(key)) {
			list = this.bonuses.get(key);
			this.bonuses.remove(key);
		} else {
			list = new LinkedList<Bonus>();
		}
		list.add(bonus);
		this.bonuses.put(key, list);
		return this.bonuses;
	}

	/**
	 * This method remove a specific bonus at the selected key, if the bonus
	 * doesn't exist or there is nothing at the corresponding key it doesn't do
	 * anything
	 * 
	 * @param bonus
	 * @param key
	 * @return the updated HashMap<Integer, LinkedList<Bonus>>
	 */
	public HashMap<Integer, LinkedList<Bonus>> removeBonusAtKey(Bonus bonus, int key) {
		if (this.bonuses.containsKey(key)) {
			LinkedList<Bonus> list = this.bonuses.get(key);
			this.bonuses.remove(key);
			list.removeFirstOccurrence(bonus);
			this.bonuses.put(key, list);
		}
		return this.bonuses;
	}

	/**
	 * this Method remove all bonus at the selected key
	 * 
	 * @param key
	 * @return the updated HashMap<Integer, LinkedList<Bonus>>
	 */
	public HashMap<Integer, LinkedList<Bonus>> removeAllBonusAtKey(int key) {
		if (this.bonuses.containsKey(key)) {
			this.bonuses.remove(key);
		}
		return this.bonuses;
	}

	/**
	 * This method allows to get the list of all bonus at a corresponding key
	 * 
	 * @param key
	 * @return the list of bonus at the corresponding key
	 */
	public LinkedList<Bonus> getAllBonusAtKey(int key) {
		if (this.bonuses.containsKey(key)) {
			return this.bonuses.get(key);
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		StringBuilder string = new StringBuilder();
		for (int i = 0; i < 21; i++) {
			string.append("+ Position " + i + " : " + this.getAllBonusAtKey(i) + "\n");
		}
		return string.toString();
	}

}