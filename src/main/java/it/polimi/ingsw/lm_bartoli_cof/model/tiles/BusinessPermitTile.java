package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import java.io.Serializable;
import java.util.*;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;

/**
 * This class implements a generic {@link BusinessPermitTile}.
 *
 */
public class BusinessPermitTile implements Serializable{
	private static final long serialVersionUID = 1L;
	
	///////////////// Attributes /////////////////

	private final LinkedList<Bonus> permitBonuses;
	private final LinkedList<City> permitCities;

	///////////////// Constructor /////////////////

	/**
	 * Instantiates a new {@link BusinessPermitTile}.
	 *
	 * @param permitCities
	 *            the {@link LinkedList} of {@link City} in which a player can
	 *            build an Emporium using a specific {@link BusinessPermitTile}
	 * @param permitBonuses
	 *            the {@link LinkedList} of {@link Bonus} that a player gains
	 *            after he has acquired a specific {@link BusinessPermitTile}
	 */
	public BusinessPermitTile(LinkedList<City> permitCities, LinkedList<Bonus> permitBonuses) {
		this.permitBonuses = permitBonuses;
		this.permitCities = permitCities;
	}

	///////////////// Getters and Setters /////////////////

	/**
	 * Gets the {@link LinkedList} of {@link City} in which a player can build
	 * an Emporium using a specific {@link BusinessPermitTile}.
	 * 
	 * @return the {@link LinkedList} of {@link City} in which a player can
	 *         build an Emporium using a specific {@link BusinessPermitTile}
	 */
	public LinkedList<City> getCities() {
		return (LinkedList<City>) permitCities;
	}

	/**
	 * Gets the {@link LinkedList} of {@link Bonus} that a player gains after he
	 * has acquired a specific {@link BusinessPermitTile}.
	 * 
	 * @return the {@link LinkedList} of {@link Bonus} that a player gains after
	 *         he has acquired a specific {@link BusinessPermitTile}
	 */
	public LinkedList<Bonus> getBonuses() {
		return (LinkedList<Bonus>) permitBonuses;
	}

	///////////////// Methods /////////////////

	@Override
	public String toString() {
		return "*BUSINESS PERMIT TILE*: Bonuses = " + this.permitBonuses + " & Cities = " + this.permitCities;
	}

}