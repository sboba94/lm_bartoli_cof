package it.polimi.ingsw.lm_bartoli_cof.model.bonus;

import java.io.Serializable;

/**
 * This class implements a generic {@link Bonus}. There are various types of
 * bonuses enumerated in the {@link BonusType} enumeration class.
 * 
 */
public class Bonus implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/////////////////////// Attributes /////////////////////////////

	private BonusType type;

	/**
	 * Each {@link Bonus} has a value attribute which allows to understand which
	 * specific field has to be increased when a player gains it.
	 * 
	 */
	private int value;

	/////////////////////// Constructor /////////////////////////////

	/**
	 * This default constructor is used in the {@link Setting} class for the
	 * creation of the various elements of a game starting from the *.xml file
	 * of configuration.
	 * 
	 */
	public Bonus() {
		super();
	}

	/**
	 * This default constructor is used in test classes.
	 * 
	 */
	public Bonus(BonusType type) {
		this.type = type;
	}

	/**
	 * Instantiates a new {@link Bonus}.
	 * 
	 * @param type
	 *            the type of the new {@link Bonus} chosen from
	 *            {@link BonusType} class
	 * @param value
	 *            the value of the new {@link Bonus}
	 */
	public Bonus(BonusType type, int value) {
		this.type = type;
		this.value = value;
	}

	////////////////////// Getters and Setters /////////////////////

	/**
	 * Gets the type of a specific {@link Bonus}.
	 * 
	 * @return the type of a specific {@link Bonus}
	 */
	public BonusType getType() {
		return this.type;
	}

	/**
	 * Sets the type of a new {@link Bonus}. Is used with the default
	 * constructor in the {@link Setting} class.
	 * 
	 * @param type
	 *            the type of a new {@link Bonus}
	 */
	public void setType(BonusType type) {
		this.type = type;
	}

	/**
	 * Gets the value of a specific {@link Bonus}.
	 * 
	 * @return the value of a specific {@link Bonus}
	 * 
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * Sets the value of a new {@link Bonus}. Is used with the default
	 * constructor in the {@link Setting} class.
	 * 
	 * @param type
	 *            the type of a new {@link Bonus}
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/////////////////////// Methods /////////////////////////////

	@Override
	public String toString() {
		return this.type.toString() + this.value;
	}

}