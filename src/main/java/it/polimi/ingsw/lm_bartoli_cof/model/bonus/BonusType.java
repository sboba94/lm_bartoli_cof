package it.polimi.ingsw.lm_bartoli_cof.model.bonus;

/**
 * This class enumerates the possible types of {@link Bonus}, for each type is
 * possible to get the description to show to the player when he gains a
 * particular {@link Bonus}.
 * 
 */
public enum BonusType {

	ASSISTANTS("Receive this number of assistants: "), 
	MONEY("Gain this number of coins: "), 
	POLITIC("Draw this number of politic cards: "), 
	ACTION("Perform this number of addictional Main Action: "), 
	VICTORY("Victory Track position: +"), 
	NOBILITY("Nobility Track position: +"), 
	ACQUIREPERMITCARD("Acquire this number of face-up Business Permit Tiles without paying the cost: "), 
	GETBONUSFROMPERMITCARD("Receive the bonus from this number of the Business Permit Tiles in your hand (also a face-down tile): "), 
	GETBONUSFROMTOKEN("Obtain the bonus of a Reward Token from this number of cities (in which you have an emporium): ");
	
	////////////////////// Attributes /////////////////////
	
	private String string;

	////////////////////// Constructor /////////////////////
	
	BonusType(String name) {
		this.string = name;
	}
	
	////////////////////// Methods /////////////////////
	
	@Override
	public String toString() {
		return string;
	}
	
}