package it.polimi.ingsw.lm_bartoli_cof.controller.state;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.controller.action.DrawPoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.controller.action.PlayerAction;

public class DrawStateTest {

	private DrawState drawState;
	
	@Before
	public void init(){
		this.drawState = new DrawState();
	}
	
	@Test
	public void testGetState() {
		assertNotNull(DrawState.getState());
	}
	
	@Test
	public void testIsActionValid(){
		PlayerAction action = new DrawPoliticCard(null, null);
		if(action.isValid()==true)
			assertEquals(this.drawState.isActionValid(action),true);
		else
			assertEquals(this.drawState.isActionValid(action),false);
	}
	
	@Test
	public void testExecuteAction(){
		PlayerAction action = new DrawPoliticCard(null, null);
		if (!action.isValid())
			assertEquals(drawState.executeAction(action),DrawState.getState());
	}

}
