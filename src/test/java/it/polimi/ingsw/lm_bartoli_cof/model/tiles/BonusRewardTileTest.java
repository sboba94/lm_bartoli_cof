package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BonusRewardTile;

public class BonusRewardTileTest {

	private BonusRewardTile tile;
	private int bonusValue;
	private String position;

	@Before
	public void init() {
		bonusValue = 25;
		position = "COAST";
		tile = new BonusRewardTile(new Bonus(BonusType.VICTORY, bonusValue), position);
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(tile);
	}

	@Test
	public void shouldGetThePosition() {
		assertEquals(tile.getPosition(), position);
	}

	@Test
	public void shouldGetTheBonus() {
		// Abstract Class Tile method .getBonus
		assertEquals(tile.getBonus().getType(), BonusType.VICTORY);
		assertEquals(tile.getBonus().getValue(), bonusValue);
	}

	@Test
	public void toStringTest() {
		System.out.println(tile.toString());
	}

}
