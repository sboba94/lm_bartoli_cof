package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;
import org.junit.Before;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.RewardToken;

public class RewardTokenTest {

	private RewardToken token;
	private LinkedList<Bonus> bonuses;

	@Before
	public void init() {

		bonuses = new LinkedList<Bonus>();
		bonuses.add(new Bonus(BonusType.MONEY, 5));
		bonuses.add(new Bonus(BonusType.NOBILITY, 7));
		bonuses.add(new Bonus(BonusType.POLITIC, 2));
		bonuses.add(new Bonus(BonusType.VICTORY, 1));
		bonuses.add(new Bonus(BonusType.ACQUIREPERMITCARD, 0));
		bonuses.add(new Bonus(BonusType.ACTION, 0));
		bonuses.add(new Bonus(BonusType.ASSISTANTS, 12));

		token = new RewardToken(bonuses);

	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(token);
	}

	@Test
	public void shoulGetBonusesList() {
		assertEquals(bonuses, token.getBonuses());
	}

	@Test
	public void shouldGetRightBonuses() {
		for (int i = 0; i < bonuses.size(); i++) {
			Bonus bonus = bonuses.get(i);
			assertEquals(bonus.getValue(), token.getBonuses().get(i).getValue());
			assertEquals(bonus.getType(), token.getBonuses().get(i).getType());
		}
	}

	@Test
	public void toStringTest() {
		System.out.println(token.toString());
	}

}
