package it.polimi.ingsw.lm_bartoli_cof.model.map;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NobilityTrackNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

public class PermitBoardTest {

	private GameMap gameMap;
	private PermitBoard coast;

	@Before
	public void init() {
		Setting config = new Setting("maps/Map1.xml");
		try {
			gameMap = new GameMap(config);
		} catch (CouncillorPoolNotConfiguredException | PermitDeckNotConfiguredException
				| KingDeckNotConfiguredException | NobilityTrackNotConfiguredException
				| BonusTilesNotConfiguredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		coast = gameMap.getCoast().getPermitBoard();
	}

	@Test
	public void getUsablePermitTile1Test() {
		BusinessPermitTile tile = coast.getUsablePermitTile1();
		assertNotNull(tile);
		assertThat(tile, is(not(coast.showUsablePermitTile1())));
	}

	@Test
	public void getUsablePermitTile2Test() {
		BusinessPermitTile tile = coast.getUsablePermitTile2();
		assertNotNull(tile);
		assertThat(tile, is(not(coast.showUsablePermitTile2())));
	}

	@Test
	public void changeUsableTilesTest() {
		BusinessPermitTile tile1 = coast.showUsablePermitTile1();
		BusinessPermitTile tile2 = coast.showUsablePermitTile1();
		coast.changeUsableTiles();
		assertNotNull(tile1);
		assertThat(tile1, is(not(coast.showUsablePermitTile1())));
		assertNotNull(tile2);
		assertThat(tile2, is(not(coast.showUsablePermitTile2())));
	}

	@Test
	public void getRegionNameTest() {
		assertEquals(coast.getRegionName(), RegionName.COAST.name());
	}
}
