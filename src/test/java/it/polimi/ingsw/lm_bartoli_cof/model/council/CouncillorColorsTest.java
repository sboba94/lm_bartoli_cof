package it.polimi.ingsw.lm_bartoli_cof.model.council;


import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;

public class CouncillorColorsTest {

	@Test
	public void shouldContainElements() {
		assertNotNull(CouncillorColors.values().length > 0);
	}

}
