package it.polimi.ingsw.lm_bartoli_cof.model.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class KingTest {
	
	private King king;

	@Before
	public void init() {
		this.king = new King();
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(this.king);
	}

	@Test
	public void TestSetCity() {
		City city = new City("Dorful");
		this.king.setCity(city);
		assertEquals(this.king.getCity(), city);
	}
	
	@Test
	public void TestGetColor() {
		City city = new City("Dorful");
		this.king.setCity(city);
		assertEquals(this.king.getCity(), city);
	}
}
