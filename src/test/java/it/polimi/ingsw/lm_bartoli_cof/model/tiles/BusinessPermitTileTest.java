package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;

public class BusinessPermitTileTest {

	private BusinessPermitTile tile;
	private LinkedList<Bonus> bonuses;
	private LinkedList<City> cities;

	@Before
	public void init() {

		bonuses = new LinkedList<Bonus>();
		bonuses.add(new Bonus(BonusType.MONEY, 5));
		bonuses.add(new Bonus(BonusType.NOBILITY, 7));
		bonuses.add(new Bonus(BonusType.POLITIC, 2));
		bonuses.add(new Bonus(BonusType.VICTORY, 1));
		bonuses.add(new Bonus(BonusType.ASSISTANTS, 12));

		cities = new LinkedList<City>();
		cities.add(new City("Dorful"));
		cities.add(new City("Arkon"));

		tile = new BusinessPermitTile(cities, bonuses);
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(tile);
	}

	@Test
	public void shouldGetCities() {
		for (int i = 0; i < cities.size(); i++) {
			assertEquals(cities.get(i), tile.getCities().get(i));
		}
	}

	@Test
	public void shouldGetBonuses() {
		for (int i = 0; i < bonuses.size(); i++) {
			assertEquals(bonuses.get(i), tile.getBonuses().get(i));
		}
	}

	@Test
	public void toStringTest() {
		System.out.println(tile.toString());
	}

}
