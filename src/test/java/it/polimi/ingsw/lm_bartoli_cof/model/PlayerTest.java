package it.polimi.ingsw.lm_bartoli_cof.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;

public class PlayerTest {

	private Player player;

	@Before
	public void init() {
		player = new Player(1, 2, 10, "ricky");
	}

//	private void showPlayerStatus() {
//		System.out.println(
//				"****************************************************************************************************************************"
//						+ "\n");
//
//		System.out.println("PLAYER STATUS:" + "\n");
//		System.out.println("assistants: " + player.getAssistantNumber() + "\n");
//		System.out.println("money: " + player.getCoinsNumber() + "\n");
//		System.out.println("politic: " + player.getPoliticCards().toString() + "\n");
//		System.out.println("victory: " + player.getVictoryPos() + "\n");
//		System.out.println("nobility: " + player.getNobilityPos() + "\n");
//		System.out.println("actions: " + player.getNumberOfPrimaryAction() + "\n");
//		System.out.println("permitTiles[face-up]: " + player.getUsablePermitTiles().toString() + "\n");
//		System.out.println("permitTiles[face-down]: " + player.getUsedPermitTiles().toString() + "\n");
//		System.out.println("emporiums: " + player.getEmporiums().toString() + "\n");
//
//		System.out.println(
//				"****************************************************************************************************************************"
//						+ "\n");
//	}

	@Test
	public void testGetPoliticCardsByColors() {
		// player hand
		player.addPoliticCard(new PoliticCard(CouncillorColors.BLACK));
		player.addPoliticCard(new PoliticCard(CouncillorColors.ORANGE));
		player.addPoliticCard(new PoliticCard(CouncillorColors.PINK));
		player.addPoliticCard(new PoliticCard(CouncillorColors.PINK));
		player.addPoliticCard(new PoliticCard(CouncillorColors.PURPLE));
		player.addPoliticCard(new PoliticCard(CouncillorColors.MULTICOLOR));
		// OK test
		String[] colorsOK;
		colorsOK = new String[3];
		colorsOK[0] = "Black";
		colorsOK[1] = "Pink";
		colorsOK[2] = "White";
		LinkedList<PoliticCard> toReturn = player.getPoliticCardsByColors(colorsOK);
		for (int i = 0; i < toReturn.size(); i++) {
			assertEquals(colorsOK[i], toReturn.get(i).getPoliticCardColor().toString());
		}
		// FAIL test
		String[] colorsNOT;
		colorsNOT = new String[3];
		colorsNOT[0] = "White";
		colorsNOT[1] = "Blue";
		colorsNOT[2] = "White";
		toReturn = player.getPoliticCardsByColors(colorsNOT);
		for (int i = 0; i < toReturn.size(); i++) {
			assertEquals(toReturn.size(), 0);
		}

	}

	@Test
	public void testHasEmporiumIn() {
		City city1 = new City("Dorful");
		City city2 = new City("Lyram");
		assertFalse(player.hasEmporiumIn(city1));
		assertFalse(player.hasEmporiumIn(city2));
		this.player.buildEmporium(city1);
		this.player.buildEmporium(city2);
		assertTrue(player.hasEmporiumIn(city1));
		assertTrue(player.hasEmporiumIn(city2));
	}
	
	@Test
	public void testGetNickname(){
		assertTrue(player.getNickname().equals("ricky"));
	}
	
	@Test
	public void testSetNickname(){
		this.player.setNickname("carmi");
		assertTrue(player.getNickname().equals("carmi"));
	}
	
	@Test
	public void testGetId(){
		assertEquals(player.getId(),1);
	}
	
	@Test
	public void testGetBonus(){
		Bonus bonus1 = new Bonus(BonusType.ASSISTANTS,1);
		Bonus bonus2 = new Bonus(BonusType.MONEY,2);
		Bonus bonus3 = new Bonus(BonusType.NOBILITY,1);
		this.player.addBonus(bonus1);
		this.player.addBonus(bonus2);
		this.player.addBonus(bonus3);
		assertEquals(this.player.getBonus().get(0),bonus1);
		assertEquals(this.player.getBonus().get(1),bonus2);
		assertEquals(this.player.getBonus().get(2),bonus3);
	}
	
	@Test
	public void testAddBonus(){
		Bonus bonus1 = new Bonus(BonusType.VICTORY,3);
		int dim = this.player.getBonus().size();
		this.player.addBonus(bonus1);
		assertEquals(this.player.getBonus().get(dim),bonus1);

	}

}
