package it.polimi.ingsw.lm_bartoli_cof.model.map;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;

public class NobilityTrackTest {

	NobilityTrack nobilityTrack;
	HashMap<Integer, LinkedList<Bonus>> trackForTest = new HashMap<Integer, LinkedList<Bonus>>();

	@Before
	public void init() {

		nobilityTrack = new NobilityTrack();

		LinkedList<Bonus> toAddBonuses0 = new LinkedList<Bonus>();

		toAddBonuses0.add(new Bonus(BonusType.MONEY, 3));
		toAddBonuses0.add(new Bonus(BonusType.VICTORY, 2));
		toAddBonuses0.add(new Bonus(BonusType.ASSISTANTS, 1));
		trackForTest.put(0, toAddBonuses0);

		LinkedList<Bonus> toAddBonuses2 = new LinkedList<Bonus>();
		toAddBonuses2.add(new Bonus(BonusType.GETBONUSFROMTOKEN, 1));
		toAddBonuses2.add(new Bonus(BonusType.GETBONUSFROMPERMITCARD, 1));
		toAddBonuses2.add(new Bonus(BonusType.ACQUIREPERMITCARD, 1));
		trackForTest.put(2, toAddBonuses2);

		LinkedList<Bonus> toAddBonuses5 = new LinkedList<Bonus>();
		toAddBonuses2.add(new Bonus(BonusType.ACQUIREPERMITCARD, 1));
		trackForTest.put(3, toAddBonuses5);

		LinkedList<Bonus> toAddBonuses9 = new LinkedList<Bonus>();
		toAddBonuses9.add(new Bonus(BonusType.NOBILITY, 8));
		toAddBonuses9.add(new Bonus(BonusType.POLITIC, 4));
		trackForTest.put(1, toAddBonuses9);

		nobilityTrack.setBonuses(trackForTest);

	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(nobilityTrack);
	}

	@Test
	public void shouldBeRightSize() {
		assertEquals(nobilityTrack.getBonuses().keySet().size(), trackForTest.keySet().size());
	}

	/*
	@Test
	public void shouldHaveRightKeys() {
		Integer[] keys;
		nobilityTrack.getBonuses().keySet().toArray(keys);
		
		assertEquals(keys[0], 0);
		assertEquals(keys[2], 2);
		
	}

	@Test
	public void shouldReturnRightBonuses() {
		for (int i = 0; i < nobilityTrack.getBonuses().size(); i++) {
			System.out.println("position :" + nobilityTrack.getBonuses().);
			assertEquals(nobilityTrack.getAllBonusAtKey(i), trackForTest.get(i));
			for (int j = 0; j < nobilityTrack.getAllBonusAtKey(i).size(); j++) {
				assertEquals(nobilityTrack.getAllBonusAtKey(i).get(j).getType(), trackForTest.get(i).get(j).getType());
				assertEquals(nobilityTrack.getAllBonusAtKey(i).get(j).getValue(),
						trackForTest.get(i).get(j).getValue());
			}
		}
	}
	*/

}
