package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;

public class PoliticCardTest {

	PoliticCard card;

	@Before
	public void init() {
		card = new PoliticCard(CouncillorColors.BLACK);
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(card);
	}

	@Test
	public void shouldGetTheColor() {
		assertEquals(card.getPoliticCardColor(), CouncillorColors.BLACK);
	}

	@Test
	public void toStringTest() {
		System.out.println(card.toString());
	}

}
