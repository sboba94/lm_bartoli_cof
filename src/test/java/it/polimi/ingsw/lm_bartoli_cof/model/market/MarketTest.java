package it.polimi.ingsw.lm_bartoli_cof.model.market;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;

public class MarketTest {

	private Market market;

	@Before
	public void init() {
		market = new Market();
	}

	@Test
	public void getSaleListTest() {

		ForSale item;
		Player owner;
		PoliticCard card;

		owner = new Player(1, 1, 1, "nickname");
		card = new PoliticCard(CouncillorColors.BLACK);
		item = new ForSale(owner, card, 10);

		LinkedList<ForSale> list = new LinkedList<ForSale>();
		list.add(item);
		market.setSaleList(list);

		assertNotNull(market.getSaleList());
		assertEquals(market.getSaleList().element(), item);

	}

}
