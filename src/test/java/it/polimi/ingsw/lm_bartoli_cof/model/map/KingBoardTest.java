package it.polimi.ingsw.lm_bartoli_cof.model.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NobilityTrackNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

public class KingBoardTest {

	private GameMap gameMap;
	private KingBoard board;

	@Before
	public void init() {
		Setting config = new Setting("maps/Map1.xml");
		try {
			gameMap = new GameMap(config);
		} catch (CouncillorPoolNotConfiguredException | PermitDeckNotConfiguredException
				| KingDeckNotConfiguredException | NobilityTrackNotConfiguredException
				| BonusTilesNotConfiguredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		board = gameMap.getKingBoard();
	}
	
	@Test
	public void getTrackTest() {
		assertNotNull(board.getTrack());
	}
	
	@Test
	public void getKingRewardsTest() {
		assertNotNull(board.getKingRewards());
	}

}
