package it.polimi.ingsw.lm_bartoli_cof.model.market;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.Player;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;

public class ForSaleTest {

	private ForSale item;
	private Player owner;
	private PoliticCard card;

	@Before
	public void init() {
		owner = new Player(1, 1, 1, "nickname");
		card = new PoliticCard(CouncillorColors.BLACK);
		item = new ForSale(owner, card, 10);
	}

	@Test
	public void getPriceTest() {
		assertEquals(item.getPrice(), 10);
	}

	@Test
	public void getObjectTest() {
		assertEquals(item.getObject(), card);
	}

	@Test
	public void getOwnerTest() {
		assertEquals(item.getOwner(), owner);
	}

}
