package it.polimi.ingsw.lm_bartoli_cof.model.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NobilityTrackNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

public class RegionBoardTest {

	private GameMap gameMap;
	private RegionBoard coast;

	@Before
	public void init() {
		Setting config = new Setting("maps/Map1.xml");
		try {
			gameMap = new GameMap(config);
		} catch (CouncillorPoolNotConfiguredException | PermitDeckNotConfiguredException
				| KingDeckNotConfiguredException | NobilityTrackNotConfiguredException
				| BonusTilesNotConfiguredException e) {
			e.printStackTrace();
		}
		coast = gameMap.getCoast();
	}

	@Test
	public void getCitiesTest() {
		assertNotNull(coast.getCities());
	}

	@Test
	public void getPermitBoardTest() {
		assertNotNull(coast.getPermitBoard());
	}

	@Test
	public void getRegionNameTest() {
		assertNotNull(coast.getRegionName());
		assertEquals(coast.getRegionName(), RegionName.COAST);
	}

	@Test
	public void isCityInTest() {
		City test1 = gameMap.getCities().getVertexFromName("Dorful");
		City test2 = gameMap.getCities().getVertexFromName("Juvelar");
		assertTrue(coast.isCityIn(test1));
		assertFalse(coast.isCityIn(test2));
	}

}
