package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.KingRewardTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.KingRewardsDeck;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;

public class KingRewardsDeckTest {

	private KingRewardsDeck kingDeck;
	private int deckSize;

	private LinkedList<KingRewardTile> tiles;

	@Before
	public void init() {

		tiles = new LinkedList<KingRewardTile>();
		tiles.add(new KingRewardTile(new Bonus(BonusType.VICTORY, 25), 1));
		tiles.add(new KingRewardTile(new Bonus(BonusType.VICTORY, 18), 2));
		tiles.add(new KingRewardTile(new Bonus(BonusType.VICTORY, 12), 3));
		tiles.add(new KingRewardTile(new Bonus(BonusType.VICTORY, 7), 4));
		tiles.add(new KingRewardTile(new Bonus(BonusType.VICTORY, 3), 5));

		kingDeck = new KingRewardsDeck(tiles);
		deckSize = kingDeck.drawPile.size();
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(kingDeck);
	}

	@Test
	public void shouldDrawCard() {
		for (KingRewardTile tile : tiles) {
			try {
				assertEquals(tile, kingDeck.drawCard());
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void shouldReturnTrueIfDeckIsEmpty() {
		assertFalse(kingDeck.isDrawPileEmpty());
		// Empty the deck
		for (int i = 0; i < deckSize; i++) {
			try {
				kingDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
		// No cards in the drawPile
		assertTrue(kingDeck.isDrawPileEmpty());
	}

	@Test
	public void shouldDrawNullIfDeckIsEmpty() {
		assertFalse(kingDeck.isDrawPileEmpty());
		// Empty the deck
		for (int i = 0; i < deckSize; i++) {
			try {
				kingDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
		// There are no cards in the drawPile
		// The drawCard() method must return null
		try {
			assertNull(kingDeck.drawCard());
		} catch (NoMorePoliticCardsException e) {
			e.printStackTrace();
		}

	}

}
