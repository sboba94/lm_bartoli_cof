package it.polimi.ingsw.lm_bartoli_cof.model.map;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NobilityTrackNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

public class GameMapTest {

	private GameMap gameMap;

	@Before
	public void init() {
		Setting config = new Setting("maps/Map1.xml");
		try {
			gameMap = new GameMap(config);
		} catch (CouncillorPoolNotConfiguredException | PermitDeckNotConfiguredException
				| KingDeckNotConfiguredException | NobilityTrackNotConfiguredException
				| BonusTilesNotConfiguredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void getCouncillorPoolTest() {
		LinkedList<Councillor> pool = new LinkedList<Councillor>();
		pool.add(new Councillor(CouncillorColors.BLACK));
		pool.add(new Councillor(CouncillorColors.ORANGE));
		gameMap.setCouncillorPool(pool);
		assertNotNull(gameMap.getCouncillorPool());
		assertEquals(gameMap.getCouncillorByColor(CouncillorColors.BLACK).getColor(), pool.getFirst().getColor());
		assertEquals(gameMap.getCouncillorByColor(CouncillorColors.ORANGE).getColor(), pool.getLast().getColor());
	}

	@Test
	public void getKingBoardTest() {
		assertNotNull(gameMap.getKingBoard());
	}

	@Test
	public void getCoastTest() {
		assertNotNull(gameMap.getCoast());
	}

	@Test
	public void getHillTest() {
		assertNotNull(gameMap.getHill());
	}

	@Test
	public void getMountainTest() {
		assertNotNull(gameMap.getMountain());
	}

	@Test
	public void getKingTest() {
		King king = new King();
		king.setCity(new City("Juvelar"));
		assertNotNull(gameMap.getKing());
		assertEquals(king.getCity().getName(), gameMap.getKing().getCity().getName());
	}

	@Test
	public void getCitiesTest() {
		assertNotNull(gameMap.getCities());
	}

	@Test
	public void addCityTest() {
		City test = new City("Test");
		gameMap.addCity(test);
		assertTrue(gameMap.getCities().containsVertex(test));
	}

	@Test
	public void addConnectionTest() {
		City test1 = new City("Test1");
		gameMap.addCity(test1);
		City test2 = new City("Test2");
		gameMap.addCity(test2);
		gameMap.addConnection(test1, test2);
		assertTrue(gameMap.getCities().findNeighbours(test1).contains(test2));
	}

	@Test
	public void getRealRegionTest() {
		assertNotNull(gameMap.getRealRegion(RegionName.COAST));
		assertNotNull(gameMap.getRealRegion(RegionName.HILLS));
		assertNotNull(gameMap.getRealRegion(RegionName.MOUNTAIN));
	}

	@Test
	public void getBonusTileByColor() {
		assertNotNull(gameMap.getBonusTileByColor(CityColors.BRONZE.name()));
		assertNotNull(gameMap.getBonusTileByColor(CityColors.GOLD.name()));
		assertNotNull(gameMap.getBonusTileByColor(CityColors.IRON.name()));
		assertNotNull(gameMap.getBonusTileByColor(CityColors.SILVER.name()));
	}

	@Test
	public void getBonusTileByPosition() {
		assertNotNull(gameMap.getBonusTileByPosition(RegionName.COAST));
		assertNotNull(gameMap.getBonusTileByPosition(RegionName.HILLS));
		assertNotNull(gameMap.getBonusTileByPosition(RegionName.MOUNTAIN));
	}

	@Test
	public void getBonusTilesTest() {
		assertNotNull(gameMap.getBonusTiles());
	}

}
