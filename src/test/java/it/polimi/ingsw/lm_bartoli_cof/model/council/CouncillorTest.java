package it.polimi.ingsw.lm_bartoli_cof.model.council;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;

public class CouncillorTest {

	private Councillor councillor;

	@Before
	public void init() {
		this.councillor = new Councillor(CouncillorColors.BLACK);
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(this.councillor);
	}

	@Test
	public void shouldGetColor() {
		assertEquals(this.councillor.getColor(), CouncillorColors.BLACK);
	}

}
