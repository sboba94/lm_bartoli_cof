package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import static org.junit.Assert.*;

import java.util.LinkedList;

import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticDeck;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;

import org.junit.Before;
import org.junit.Test;

public class PoliticDeckTest {

	private PoliticDeck politicDeck;
	private int deckSize;

	private LinkedList<PoliticCard> cards;

	@Before
	public void init() {

		cards = new LinkedList<PoliticCard>();

		cards.add(new PoliticCard(CouncillorColors.BLACK));
		cards.add(new PoliticCard(CouncillorColors.BLUE));
		cards.add(new PoliticCard(CouncillorColors.MULTICOLOR));
		cards.add(new PoliticCard(CouncillorColors.ORANGE));
		cards.add(new PoliticCard(CouncillorColors.PINK));
		cards.add(new PoliticCard(CouncillorColors.PURPLE));
		cards.add(new PoliticCard(CouncillorColors.WHITE));

		politicDeck = new PoliticDeck(cards);
		deckSize = politicDeck.drawPile.size();
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(politicDeck);
	}

	@Test
	public void shouldDrawCard() {
		for (PoliticCard card : cards) {
			try {
				assertEquals(card, politicDeck.drawCard());
			} catch (NoMorePoliticCardsException e) {
				e.getMessage();
			}
		}
	}

	@Test
	public void shouldReturnTrueIfIsAllEmpty() {
		assertFalse(politicDeck.isDrawPileEmpty());
		// Empty the deck
		for (int i = 0; i < deckSize; i++) {
			try {
				politicDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.getMessage();
			}
		}
		// No cards in the discardPile neither
		assertTrue(politicDeck.isAllEmpty());

	}

	@Test
	public void shouldReturnTrueIfIsDrawPileEmpty() {
		assertFalse(politicDeck.isDrawPileEmpty());
		// Empty the deck
		for (int i = 0; i < deckSize; i++) {
			try {
				politicDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.getMessage();
			}
		}
		// No cards in the drawPile
		assertTrue(politicDeck.isDrawPileEmpty());

	}

	@Test
	public void shouldDrawNullIfIsAllEmpty() {
		assertFalse(politicDeck.isDrawPileEmpty());
		// Empty the deck
		for (int i = 0; i < deckSize; i++) {
			try {
				politicDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.getMessage();
			}
		}
		// There are no cards in the drawPile and in the discardPile
		// The drawCard() method must return null
		try {
			assertNull(politicDeck.drawCard());
		} catch (NoMorePoliticCardsException e) {
			e.getMessage();
		}

	}

	@Test
	public void shouldUseDiscardPileIfDrawPileIsEmpty() {
		assertFalse(politicDeck.isDrawPileEmpty());
		// Empty the deck except for one card
		for (int i = 0; i < (deckSize - 1); i++) {
			try {
				politicDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.getMessage();
			}
		}
		try {
			// Empty the deck but save a reference to the last card
			PoliticCard expectedCard = politicDeck.drawCard();
			// Put that card in the discardPile of the politicDeck
			// Test the discardCard() method too
			politicDeck.discardCard(expectedCard);
			// Check if we get the same card from the method drawCard()
			assertEquals(expectedCard, politicDeck.drawCard());
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
