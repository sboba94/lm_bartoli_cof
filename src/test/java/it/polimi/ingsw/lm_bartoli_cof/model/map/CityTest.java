package it.polimi.ingsw.lm_bartoli_cof.model.map;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;

public class CityTest {

	City city;

	@Before
	public void init() {
		city = new City("Dorful", CityColors.GOLD, RegionName.COAST);
	}

	@Test
	public void getTokenTest() {
		LinkedList<Bonus> toAddBonuses = new LinkedList<Bonus>();
		toAddBonuses.add(new Bonus(BonusType.ACQUIREPERMITCARD, 10));
		city.setToken(toAddBonuses);
		assertEquals(city.getToken().getBonuses().getFirst().getType(), toAddBonuses.getFirst().getType());
		assertEquals(city.getToken().getBonuses().getFirst().getValue(), toAddBonuses.getFirst().getValue());
	}

	@Test
	public void getNameTest() {
		assertEquals(city.getName(), "Dorful");
	}

	@Test
	public void getColorTest() {
		assertEquals(city.getColor(), CityColors.GOLD);
	}

	@Test
	public void getEmporiumNumberTest() {
		city.incrementEmporium();
		city.incrementEmporium();
		assertEquals(city.getEmporiumNumber(), 2);
	}

	@Test
	public void getRegionNameTest() {
		assertEquals(city.getRegionName(), RegionName.COAST);
	}

}
