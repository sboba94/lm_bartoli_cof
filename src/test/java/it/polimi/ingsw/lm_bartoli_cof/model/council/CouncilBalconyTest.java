package it.polimi.ingsw.lm_bartoli_cof.model.council;


import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncilBalcony;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.council.CouncillorColors;
public class CouncilBalconyTest {

	private CouncilBalcony council;
	private LinkedList<Councillor> pool;
	private LinkedList<CouncillorColors> politicCardsTrue;
	private LinkedList<CouncillorColors> politicCardsFalse;
	private LinkedList<CouncillorColors> politicCardsDouble;
	private LinkedList<CouncillorColors> politicCardsNot;

	@Before
	public void init() {

		// add only four Councillors for now
		pool = new LinkedList<Councillor>();
		pool.add(new Councillor(CouncillorColors.BLACK));
		pool.add(new Councillor(CouncillorColors.BLUE));
		pool.add(new Councillor(CouncillorColors.ORANGE));
		pool.add(new Councillor(CouncillorColors.PINK));

		politicCardsTrue = new LinkedList<CouncillorColors>();
		politicCardsTrue.add(CouncillorColors.BLACK);
		politicCardsTrue.add(CouncillorColors.ORANGE);
		politicCardsTrue.add(CouncillorColors.BLUE);

		politicCardsFalse = new LinkedList<CouncillorColors>();
		politicCardsFalse.add(CouncillorColors.BLACK);
		politicCardsFalse.add(CouncillorColors.WHITE);

		politicCardsDouble = new LinkedList<CouncillorColors>();
		politicCardsDouble.add(CouncillorColors.BLUE);
		politicCardsDouble.add(CouncillorColors.BLUE);

		politicCardsNot = new LinkedList<CouncillorColors>();
		politicCardsNot.add(CouncillorColors.PURPLE);

		council = new CouncilBalcony(pool);
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(council);
	}

	@Test
	public void shouldBeRightSize() {
		int balconySize = council.getCouncil().size();
		int poolSize = pool.size();
		assertEquals(balconySize, 4);
		/*
		 * for (Councillor councillor : council.getCouncil()) {
		 * System.out.println(councillor.getColors()); }
		 */
		assertEquals(poolSize, 0);
		/*
		 * for (Councillor councillor : pool) {
		 * System.out.println(councillor.getColors()); }
		 */
	}

	@Test
	public void shouldElectACouncillor() {
		Councillor councillor = new Councillor(CouncillorColors.BLACK);
		council.electCouncillor(councillor);
		int balconySize = council.getCouncil().size();
		assertEquals(balconySize, 4);
		council.getCouncil().remove();
		council.getCouncil().remove();
		council.getCouncil().remove();
		assertEquals(council.getCouncil().size(), 1);
		assertEquals(council.getCouncil().remove(), councillor);
	}

	@Test
	public void shouldReturnTrueIfContainRightColors() {
		assertTrue(council.containColors(politicCardsTrue));
		assertFalse(council.containColors(politicCardsFalse));
		assertFalse(council.containColors(politicCardsDouble));
		assertFalse(council.containColors(politicCardsNot));
	}

}
