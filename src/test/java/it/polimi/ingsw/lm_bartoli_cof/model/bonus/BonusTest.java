package it.polimi.ingsw.lm_bartoli_cof.model.bonus;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BonusTest {

	private Bonus bonus;

	@Before
	public void init() {
		bonus = new Bonus();
	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(bonus);
	}

	@Test
	public void getTypeTest() {
		bonus.setType(BonusType.ACQUIREPERMITCARD);
		assertEquals(bonus.getType(), BonusType.ACQUIREPERMITCARD);
	}

	@Test
	public void getValueTest() {
		bonus.setValue(10);
		assertEquals(bonus.getValue(), 10);
	}

}
