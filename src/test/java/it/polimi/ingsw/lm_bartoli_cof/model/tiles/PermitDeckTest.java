package it.polimi.ingsw.lm_bartoli_cof.model.tiles;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.bonus.BonusType;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PermitDeck;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;

import org.junit.Before;

public class PermitDeckTest {

	private PermitDeck permitDeck;
	private int deckSize;

	private LinkedList<BusinessPermitTile> tiles;
	private LinkedList<Bonus> bonuses1;
	private LinkedList<Bonus> bonuses2;
	private LinkedList<Bonus> bonuses3;
	private LinkedList<City> cities1;
	private LinkedList<City> cities2;
	private LinkedList<City> cities3;

	@Before
	public void init() {

		bonuses1 = new LinkedList<Bonus>();
		bonuses2 = new LinkedList<Bonus>();
		bonuses3 = new LinkedList<Bonus>();
		cities1 = new LinkedList<City>();
		cities2 = new LinkedList<City>();
		cities3 = new LinkedList<City>();
		tiles = new LinkedList<BusinessPermitTile>();

		bonuses1.add(new Bonus(BonusType.MONEY, 5));
		bonuses1.add(new Bonus(BonusType.NOBILITY, 7));
		bonuses1.add(new Bonus(BonusType.POLITIC, 2));
		bonuses1.add(new Bonus(BonusType.VICTORY, 1));
		bonuses1.add(new Bonus(BonusType.ASSISTANTS, 12));
		cities1.add(new City("Dorful"));
		cities1.add(new City("Arkon"));

		bonuses2.add(new Bonus(BonusType.MONEY, 0));
		bonuses2.add(new Bonus(BonusType.NOBILITY, 9));
		bonuses2.add(new Bonus(BonusType.POLITIC, 4));
		bonuses2.add(new Bonus(BonusType.VICTORY, 23));
		bonuses2.add(new Bonus(BonusType.ASSISTANTS, 1));
		cities2.add(new City("Indur"));
		cities2.add(new City("Juvelar"));
		cities2.add(new City("Graden"));

		bonuses3.add(new Bonus(BonusType.MONEY, 45));
		bonuses3.add(new Bonus(BonusType.NOBILITY, 2));
		bonuses3.add(new Bonus(BonusType.POLITIC, 54));
		bonuses3.add(new Bonus(BonusType.VICTORY, 6));
		bonuses3.add(new Bonus(BonusType.ASSISTANTS, 11));
		cities3.add(new City("Naris"));

		tiles.add(new BusinessPermitTile(cities1, bonuses1));
		tiles.add(new BusinessPermitTile(cities2, bonuses2));
		tiles.add(new BusinessPermitTile(cities3, bonuses3));

		permitDeck = new PermitDeck(tiles);
		deckSize = permitDeck.drawPile.size();

	}

	@Test
	public void shouldBeCreated() {
		assertNotNull(permitDeck);
	}

	@Test
	public void shouldDrawCard() {
		for (BusinessPermitTile tile : tiles) {
			try {
				assertEquals(tile, permitDeck.drawCard());
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void shouldReturnTrueIfIsDrawPileEmpty() {
		assertFalse(permitDeck.isDrawPileEmpty());
		// Empty the deck
		for (int i = 0; i < deckSize; i++) {
			try {
				permitDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
		// No cards in the drawPile
		assertTrue(permitDeck.isDrawPileEmpty());
	}

	@Test
	public void shouldDrawNullIfDeckIsEmpty() {
		assertFalse(permitDeck.isDrawPileEmpty());
		// Empty the deck
		for (int i = 0; i < deckSize; i++) {
			try {
				permitDeck.drawCard();
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
		// There are no cards in the drawPile
		// The drawCard() method must return null
		try {
			assertNull(permitDeck.drawCard());
		} catch (NoMorePoliticCardsException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void shouldEnqueueTiles() {
		try {
			BusinessPermitTile expectedTile = permitDeck.drawCard();
			permitDeck.enqueueTile(expectedTile);
			for (int i = 0; i < (deckSize - 1); i++) {
				permitDeck.drawCard();
			}
			assertEquals(expectedTile, permitDeck.drawCard());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
