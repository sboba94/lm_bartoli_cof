package it.polimi.ingsw.lm_bartoli_cof.utils;

import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_bartoli_cof.model.bonus.Bonus;
import it.polimi.ingsw.lm_bartoli_cof.model.council.Councillor;
import it.polimi.ingsw.lm_bartoli_cof.model.map.City;
import it.polimi.ingsw.lm_bartoli_cof.model.map.RegionName;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BonusRewardTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.KingRewardTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.KingRewardsDeck;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PermitDeck;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.BusinessPermitTile;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticCard;
import it.polimi.ingsw.lm_bartoli_cof.model.tiles.PoliticDeck;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.BonusTilesNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.CouncillorPoolNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.KingDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.NoMorePoliticCardsException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PermitDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.exception.PoliticDeckNotConfiguredException;
import it.polimi.ingsw.lm_bartoli_cof.utils.map.Setting;

public class SettingTest {

	Setting config = new Setting("maps/Map1.xml");

	LinkedList<PoliticCard> politicCards;
	PoliticDeck politicDeck;

	LinkedList<BusinessPermitTile> coastTiles;
	LinkedList<BusinessPermitTile> hillsTiles;
	LinkedList<BusinessPermitTile> mountainTiles;
	PermitDeck coastPermitDeck;
	PermitDeck hillsPermitDeck;
	PermitDeck mountainPermitDeck;

	LinkedList<KingRewardTile> kingTiles;
	KingRewardsDeck kingDeck;

	LinkedList<Councillor> poolTest;
	LinkedList<Councillor> councillorPool;

	LinkedList<BonusRewardTile> bonusTest;
	LinkedList<BonusRewardTile> bonusTiles;

	LinkedList<City> citiesTest;
	LinkedList<City> cities;

	LinkedHashMap<String, LinkedList<String>> edgesTest;
	LinkedHashMap<String, LinkedList<String>> edges;

	LinkedHashMap<Integer, LinkedList<Bonus>> nobilityTest;
	LinkedHashMap<Integer, LinkedList<Bonus>> nobilityTrack;

	// cities for every Region
	LinkedList<City> coastCities;
	LinkedList<City> hillsCities;
	LinkedList<City> mountainCities;

	@Before
	public void init() {

		coastCities = new LinkedList<City>();
		coastCities.add(new City("Arkon"));
		coastCities.add(new City("Burgen"));
		coastCities.add(new City("Castrum"));
		coastCities.add(new City("Dorful"));
		coastCities.add(new City("Esti"));

		hillsCities = new LinkedList<City>();
		hillsCities.add(new City("Framek"));
		hillsCities.add(new City("Graden"));
		hillsCities.add(new City("Hellar"));
		hillsCities.add(new City("Indur"));
		hillsCities.add(new City("Juvelar"));

		mountainCities = new LinkedList<City>();
		mountainCities.add(new City("Kultos"));
		mountainCities.add(new City("Lyram"));
		mountainCities.add(new City("Merkatim"));
		mountainCities.add(new City("Naris"));
		mountainCities.add(new City("Osium"));

		try {

			politicCards = config.generatePoliticDeck();
			politicDeck = new PoliticDeck(politicCards);

			coastTiles = config.generatePermitDeck(RegionName.COAST, coastCities);
			coastPermitDeck = new PermitDeck(coastTiles);
			hillsTiles = config.generatePermitDeck(RegionName.HILLS, hillsCities);
			hillsPermitDeck = new PermitDeck(hillsTiles);
			mountainTiles = config.generatePermitDeck(RegionName.MOUNTAIN, mountainCities);
			mountainPermitDeck = new PermitDeck(mountainTiles);

			kingTiles = config.generateKingRewardsDeck();
			kingDeck = new KingRewardsDeck(kingTiles);

			poolTest = config.generateCouncillorPool();
			councillorPool = new LinkedList<Councillor>();
			councillorPool.addAll(poolTest);

			bonusTest = config.generateBonusTiles();
			bonusTiles = new LinkedList<BonusRewardTile>();
			bonusTiles.addAll(bonusTest);

			citiesTest = config.fillGraphWithCities();
			cities = new LinkedList<City>();
			cities.addAll(citiesTest);

			edgesTest = config.fillGraphWithEdges();
			edges = new LinkedHashMap<String, LinkedList<String>>();
			edges = edgesTest;

			nobilityTest = config.generateNobilityTrack();
			nobilityTrack = new LinkedHashMap<Integer, LinkedList<Bonus>>();
			nobilityTrack = nobilityTest;

		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test
	public void shouldGenerateAllElements() {
		assertNotNull(politicCards);
		assertNotNull(coastPermitDeck);
		assertNotNull(hillsPermitDeck);
		assertNotNull(mountainPermitDeck);
		assertNotNull(kingDeck);
		assertNotNull(councillorPool);
		assertNotNull(bonusTiles);
		assertNotNull(cities);
		assertNotNull(edges);
		assertNotNull(nobilityTrack);
	}

	@Test
	public void shouldCreateRightPoliticDeck() throws PoliticDeckNotConfiguredException {
		int size = politicDeck.getDrawPile().size();
		for (int i = 0; i < size; i++) {
			try {
				assertEquals(politicDeck.drawCard().getPoliticCardColor(), politicCards.get(i).getPoliticCardColor());
			} catch (NoMorePoliticCardsException e) {
				e.getMessage();
			}
		}
	}

	@Test
	public void shouldCreateRightPermitDecks() throws PermitDeckNotConfiguredException {
		int coastSize = coastPermitDeck.getDrawPile().size();
		for (int i = 0; i < coastSize; i++) {
			BusinessPermitTile thisPermitTile;
			try {
				thisPermitTile = coastPermitDeck.drawCard();
				assertEquals(thisPermitTile.getBonuses(), coastTiles.get(i).getBonuses());
				assertEquals(thisPermitTile.getCities(), coastTiles.get(i).getCities());
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}

		int hillsSize = hillsPermitDeck.getDrawPile().size();
		for (int i = 0; i < hillsSize; i++) {
			BusinessPermitTile thisPermitTile;
			try {
				thisPermitTile = hillsPermitDeck.drawCard();
				assertEquals(thisPermitTile.getBonuses(), hillsTiles.get(i).getBonuses());
				assertEquals(thisPermitTile.getCities(), hillsTiles.get(i).getCities());
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}

		int mountainSize = mountainPermitDeck.getDrawPile().size();
		for (int i = 0; i < mountainSize; i++) {
			BusinessPermitTile thisPermitTile;
			try {
				thisPermitTile = mountainPermitDeck.drawCard();
				assertEquals(thisPermitTile.getBonuses(), mountainTiles.get(i).getBonuses());
				assertEquals(thisPermitTile.getCities(), mountainTiles.get(i).getCities());
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void shouldCreateRightKingDeck() throws KingDeckNotConfiguredException {
		int size = kingDeck.getDrawPile().size();
		for (int i = 0; i < size; i++) {
			KingRewardTile thisKingTile;
			try {
				thisKingTile = kingDeck.drawCard();
				assertEquals(thisKingTile.getBonus().getType(), kingTiles.get(i).getBonus().getType());
				assertEquals(thisKingTile.getBonus().getValue(), kingTiles.get(i).getBonus().getValue());
				assertEquals(thisKingTile.getPosition(), kingTiles.get(i).getPosition());
			} catch (NoMorePoliticCardsException e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void shouldCreateRightCouncillorPool() throws CouncillorPoolNotConfiguredException {
		int size = councillorPool.size();
		for (int i = 0; i < size; i++) {
			assertEquals(councillorPool.get(i).getColor(), poolTest.get(i).getColor());
		}
	}

	@Test
	public void shouldCreateRightBonusTiles() throws BonusTilesNotConfiguredException {
		int size = bonusTiles.size();
		for (int i = 0; i < size; i++) {
			BonusRewardTile thisBonusTile = bonusTiles.get(i);
			System.out.println(bonusTiles.get(i));
			assertEquals(thisBonusTile.getBonus().getType(), bonusTest.get(i).getBonus().getType());
			assertEquals(thisBonusTile.getBonus().getValue(), bonusTest.get(i).getBonus().getValue());
			assertEquals(thisBonusTile.getPosition(), bonusTest.get(i).getPosition());
		}
	}

	@Test
	public void shouldCreateRightCitiesList() {
		int size = cities.size();
		for (int i = 0; i < size; i++) {
			City thisCity = cities.get(i);
			assertEquals(thisCity.getColor(), citiesTest.get(i).getColor());
			assertEquals(thisCity.getName(), citiesTest.get(i).getName());
			assertEquals(thisCity.getRegionName(), citiesTest.get(i).getRegionName());
			assertEquals(thisCity.getToken(), citiesTest.get(i).getToken());
		}
	}

	@Test
	public void shouldCreateRightEdges() {
		assertEquals(edges.size(), edgesTest.size());
		int size = edges.size();
		for (int i = 0; i < size; i++) {

			LinkedList<String> keySet = new LinkedList<String>();
			keySet.addAll(edges.keySet());
			LinkedList<String> keySetTest = new LinkedList<String>();
			keySetTest.addAll(edgesTest.keySet());

			assertEquals(keySet.get(i), keySetTest.get(i));

			int numberOfEdges = edges.get((keySet).get(i)).size();
			for (int j = 0; j < numberOfEdges; j++) {
				assertEquals(edges.get(keySet.get(i)).get(j), edgesTest.get(keySetTest.get(i)).get(j));
			}
		}
	}

	@Test
	public void shouldCreateRightNobilityTrack() {
		assertEquals(nobilityTrack.size(), nobilityTest.size());
		int size = nobilityTrack.size();
		for (int i = 0; i < size; i++) {
			int numberOfBonuses = nobilityTrack.get(i).size();
			for (int j = 0; j < numberOfBonuses; j++) {
				assertEquals(nobilityTrack.get(i).get(j).getType(), nobilityTest.get(i).get(j).getType());
				assertEquals(nobilityTrack.get(i).get(j).getValue(), nobilityTest.get(i).get(j).getValue());
			}
		}
	}
}
